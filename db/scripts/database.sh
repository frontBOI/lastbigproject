set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE event_db;
    CREATE DATABASE user_db;
	  CREATE DATABASE user_suggestion_db;
    CREATE DATABASE user_event_db;
    CREATE DATABASE auth_db;
    CREATE DATABASE event_comment_db;
    # CREATE DATABASE event_inviting_db;
    # CREATE DATABASE datamining_db;
    # CREATE DATABASE user_inviting_db;
    # CREATE DATABASE ticket_office_db;
    # CREATE DATABASE ticket_transaction_db;
    
    

EOSQL

