import json

from flask import Flask
from flask import request
from static.model.dao import DAO
from static.model.mining_model import MiningModel
from static.model.prediction import Prediction
app = Flask(__name__)
dao = DAO()
model = MiningModel(dao)
model.updateModel()
predicter = Prediction()

def bad_request():
    return '<html>' \
           '<meta charset="UTF-8">' \
           '<head>' \
           '<h1>400 : Bad Request</h1>' \
           '</head>' \
           '<body>' \
           '</body>' \
           '</html>'

@app.route('/')
def hello_world():
    return '<html>' \
           '<meta charset="UTF-8">' \
           '<div>Web Server is up !</div><br/>' \
           '<div><p>Available URLs on this server</p>' \
           '<dl>' \
            '<dt>/predict</dt>' \
            '<dd>Method : POST</dd>'\
            '<dd>Parameters : JSON with keys "listEvtMeetUpID" : list of string, "listTags" : list of string (e.g.: {"listEvtMeetUpID": ["idexample1"],"listTags": ["tag1","tag2"]})</dd>' \
            '<dd>Return type : list of Events ID (e.g.: "[1,2,3,4,5]")</dd>' \
            '<dt>/predictHTML</dt>' \
            '<dd>Method : GET</dd>'\
            '<dd>Parameters : None (autoset in code, URL for test purposes only)</dd>' \
            '<dd>Return type : HTML content (table of events cluster + table of suggested events)</dd>' \
            '<dt>/updateModel</dt>' \
            '<dd>Parameters : None</dd>' \
            '<dd>Return type : String : "Model updated" </dd>' \
           '</dl></div>' \
           '</html>'

@app.route('/predict', methods=['POST'])
def predict():
    #strToPredict = "Acquérir des clients grâce à internet n'est pas aussi facile que certain.e.s souhaitent vous faire croire. D'autant plus que cette fois-ci, c'est bien plus important que toutes les précédentes : c'est renouer le contact, coupé par les évènements, avec vos prospects.C'est s'adapter aux changements.C'est passer les obstacles et les imprévus.Mais êtes-vous réellement prêt.e.s pour lancer une nouvelle campagne de prospection ?La publicité en ligne c'est au moins 40% de chiffre d'affaire en plus.D'ailleurs, pendant cette période, ça peut être plus ... Et puis cela pourra aussi, rentabiliser votre site eCommerce ou futur site eCommerce si vous avez pris le virage de la vente en ligne !Rien n'est perdu.Tout se transforme.Car tant que nous n'abandonnons pas, nous nous renfonçons.Envie de tester sans risques une campagne de communication grâce à nos coachs en Webmarketing ? Genosy vous propose un coaching live d'une heure avec nos deux experts en webmarketing, Ydriss Diagne et Camille Le Guilloux, afin de vous préparer à l'après. Pendant une heure, échangez avec nos deux coachs et construisez votre retour en force sur la scène économique et sociale ! Prêt.e.s à lancer votre campagne de publicité en ligne ? Rejoignez notre formation 100% financée. Réservez votre place et demandez votre financement avec notre formulaire en ligne "
    if request.method == 'POST' :
        try:
            json_obj = request.json
        except Exception:
            print("Parameter isn't a JSON string")
            return bad_request()

        if "listTags" in json_obj and "listEvtMeetUpID" in json_obj :
            predicter.updatePredictionModel(model)
            listTags = json_obj['listTags']
            listEvtMeetUpID = json_obj['listEvtMeetUpID']
            return (str(predicter.requestPrediction(listTags, listEvtMeetUpID)))
        else :
            return bad_request()
    else :
        return bad_request()
    #return 'Not implemented yet'

@app.route('/predictHTML')
def predictHTML():
    strToPredict = request.args.get('strToPredict')

    if request.method == 'GET' and strToPredict != None:
        htmlcontent = predicter.updatePredictionModel(model)
        return (htmlcontent + predicter.requestPredictionHTML(strToPredict))
    else :
        return bad_request()

    #return 'Not implemented yet'

@app.route('/updateModel')
def updateModel():
    model.updateModel()
    return ("Model updated")

if __name__ == '__main__':
    app.run()

