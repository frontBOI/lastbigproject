import json,os
from pymongo import MongoClient

class DAO:
    def __init__(self):
        self.urlconnection = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@mongo:27017'
        #self.urlconnection = 'mongodb://mongo:password@127.0.0.1:27017' #A décommenter pour tester en local
        self.client = MongoClient(self.urlconnection)
        self.db_name = 'events_database'
        self.db = self.client[self.db_name]
        self.db_events = self.db["events"]

    def getAllEventsFromDB(self):
        listOfDesc = []
        listOfEvtsNames = []
        listOfEvtsIds = []
        for currentEvent in self.db_events.find():
            if "mining_ready" in currentEvent and currentEvent["mining_ready"] != 'null' and "name" in currentEvent and "meetupId" in currentEvent and currentEvent["meetupId"] not in listOfEvtsIds:
                listOfDesc.append(str(currentEvent["name"]) + " " + " ".join(currentEvent["mining_ready"]))
                listOfEvtsNames.append(currentEvent["name"])
                listOfEvtsIds.append(currentEvent["meetupId"])
        result = {
            "descriptions": listOfDesc,
            "names": listOfEvtsNames,
            "ids": listOfEvtsIds
        }
        print(result)
        return result

    def testTempGetDataFromFile(self):
        with open('static/local/exploitable_dataset.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
        listOfDesc = []
        listOfEvtsNames = []
        listOfEvtsIds = []
        for currentEvent in data:
            if currentEvent["mining_ready"] != 'null' and currentEvent["name"] not in listOfEvtsNames:
                listOfDesc.append(str(currentEvent["name"]) + " " + " ".join(currentEvent["mining_ready"]))
                listOfEvtsNames.append(currentEvent["name"])
                listOfEvtsIds.append(currentEvent["id"])
        f.close()
        result = {
            "descriptions" : listOfDesc,
            "names" : listOfEvtsNames,
            "ids" : listOfEvtsIds
        }
        return result

