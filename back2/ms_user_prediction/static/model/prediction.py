from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import pandas as pd
import operator

class Prediction:
    def __init__(self):
        self.__predictionModel = None
        self.__trainingDataset = None
        self.__dataVectorizer = None
        self.__dataframeClusterizedData = None

    def updatePredictionModel(self, model):
        if model.getModel() != None and model.getModelData() != None and model.getDataVectorizer() != None:
            self.__predictionModel = model.getModel()
            self.__trainingDataset = model.getModelData()
            self.__dataVectorizer = model.getDataVectorizer()
            return self.updateDataframeClusterizedData()
        else :
            self.__predictionModel = None
            return ("[]")

    def updateDataframeClusterizedData(self):
        if self.__predictionModel != None:
            labels = self.__predictionModel.labels_
            self.__dataframeClusterizedData = pd.DataFrame(list(zip(self.__trainingDataset["ids"],self.__trainingDataset["names"],self.__trainingDataset["descriptions"],labels)), columns=['id', 'title','mining_ready','cluster'])
            return (self.__dataframeClusterizedData.sort_values(by=['cluster']).to_html())
        else :
            self.__predictionModel = None
            return ("[]")


    def requestPrediction(self, listTags,listEvtMeetUpID):
        if isinstance(self.__dataframeClusterizedData, pd.DataFrame) :
            listEventsParticipated = self.__dataframeClusterizedData.loc[self.__dataframeClusterizedData['id'].isin(listEvtMeetUpID)]
        else :
            listEventsParticipated = []
        tagStr = " ".join(listTags)

        listOfPredictionResults = {}

        if self.__predictionModel != None :#and isinstance(self.__dataframeClusterizedData, pd.DataFrame):
            '''
            Prédiction basée sur les tags
            '''
            if tagStr != None and len(tagStr) > 0:
                Y = self.__dataVectorizer.transform([tagStr])
                prediction = self.__predictionModel.predict(Y)

                if prediction[0] in listOfPredictionResults:
                    listOfPredictionResults[prediction[0]] += 1
                else :
                    listOfPredictionResults[prediction[0]] = 1
            '''
            Prédictions basées sur les événements auxquels l'utilisateur a déjà participé
            '''
            for index, row in listEventsParticipated.iterrows():
                if row["mining_ready"] != None:
                    strToPredict = str(row["mining_ready"])
                    Y = self.__dataVectorizer.transform([strToPredict])
                    prediction = self.__predictionModel.predict(Y)

                    if prediction[0] in listOfPredictionResults:
                        listOfPredictionResults[prediction[0]] += 1
                    else:
                        listOfPredictionResults[prediction[0]] = 1
            '''
            On vérifie qu'on a pu faire des prédictions
            len = 0 : cas où utilisateur a 0 tags et a participé à aucun événement
            '''
            if(len(listOfPredictionResults.keys())>0):
                matchingCluster = max(listOfPredictionResults.items(), key=operator.itemgetter(1))[0]
                print("Closest cluster id : " + str(matchingCluster))
                return (self.__dataframeClusterizedData.loc[self.__dataframeClusterizedData['cluster'] == matchingCluster]["id"].values.tolist())
            else :
                return "[]"
        else:
            return "[]"
        return "[]"


    def requestPredictionHTML(self,strOfInterests):
        if self.__predictionModel != None and isinstance(self.__dataframeClusterizedData, pd.DataFrame) :
            Y = self.__dataVectorizer.transform([strOfInterests])
            prediction = self.__predictionModel.predict(Y)
            return (self.__dataframeClusterizedData.loc[self.__dataframeClusterizedData['cluster'] == prediction[0]].to_html())
        else :
            return("[]")
