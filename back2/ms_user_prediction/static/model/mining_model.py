from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
from kneed import KneeLocator as kneel
import math
import pandas as pd

class MiningModel:

    def __init__(self, dao):
        self.__model = None
        self.__modelData = None
        self.__modelNbClusters = 2
        self.__modelDataClusterLimit = 50
        self.__dataVectorizer = TfidfVectorizer()
        #self.__dataframeClusterizedData = None
        self.__dao = dao

    def loadModelData(self):
        self.__modelData = self.__dao.getAllEventsFromDB()

    def determineElbowTip(self):
        if len(self.__modelData["descriptions"])<=0:
            print("No events found in database, model not initialised")
            return
        X = self.__dataVectorizer.fit_transform(self.__modelData["descriptions"])
        nb_cat_max = math.ceil(len(self.__modelData["descriptions"])/3)

        if(nb_cat_max>self.__modelDataClusterLimit):
            nb_cat_max = self.__modelDataClusterLimit

        if(nb_cat_max<2):
            self.__modelNbClusters = 1
            return

        K = range(2, nb_cat_max)
        Sum_of_squared_distances = []

        for k in K:
            km = KMeans(n_clusters=k, max_iter=200, n_init=10)
            km = km.fit(X)
            Sum_of_squared_distances.append(km.inertia_)

        rangeOfClusers = range (1, len(Sum_of_squared_distances)+1)
        kn = kneel(rangeOfClusers,Sum_of_squared_distances,curve='convex',direction='decreasing')
        if(isinstance(kn.knee,np.int64)):
            self.__modelNbClusters = kn.knee
        else:
            self.__modelNbClusters = math.ceil(nb_cat_max/2)
            print ("WARNING: Default cluster number set: "+str(math.ceil(nb_cat_max/2)))

        print("nb cluster recommandé= "+str(self.__modelNbClusters))
        plt.plot(rangeOfClusers, Sum_of_squared_distances, 'bx-')
        plt.vlines(self.__modelNbClusters, plt.ylim()[0], plt.ylim()[1], linestyles='dashed')
        plt.xlabel('k')
        plt.ylabel('Sum_of_squared_distances')
        plt.title('Elbow Method For Optimal k')
        plt.savefig('static/local/elbow.png')

    def setModel(self):
        if len(self.__modelData["descriptions"])<=0:
            return
        X = self.__dataVectorizer.fit_transform(self.__modelData["descriptions"])
        self.__model = KMeans(n_clusters=self.__modelNbClusters,init='k-means++',max_iter=200,n_init=10)
        self.__model.fit(X)

    def updateModel(self):
        self.loadModelData()
        self.determineElbowTip()
        self.setModel()

    def getModel(self):
        return self.__model

    def getModelData(self):
        return self.__modelData

    def getDataVectorizer(self):
        return self.__dataVectorizer
