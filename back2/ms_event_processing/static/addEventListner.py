import traceback
import json
from static.dao import DAO
from static.dataProcessing import DataProcesser
from static.serviceNotifier import ServiceNotifier
import paho.mqtt.client as mqtt
from static.dao import DAO

class AddEventListener():
    def __init__(self):
        self.client = mqtt.Client(client_id='PYTHONCONSUMER')
        #self.client.ws_set_options(path='tcp://activemq:1883',headers=None)
        self.client.on_connect = on_connect
        self.client.on_message = on_message

    def connectToESB(self):
        self.client.connect('activemq',1883,60)
        self.client.loop_forever()

def on_connect(client, userdata, flags, rc):
    print("Connected on ActiveMQ with result code " + str(rc) +". Waiting for data")
    client.subscribe('/queue/addEvent',2)

def on_message(client, userdata, message):
    str_msg = str(message.payload,'utf-8')
    #print("Received message '" + str_msg + "' on topic '" + message.topic + "' with QoS " + str(message.qos))
    process_message_event(str_msg)

def process_message_event(eventString):
    db = DAO()
    dataProcesser = DataProcesser()
    serviceNotifier = ServiceNotifier()
    try:
        json_obj = json.loads(eventString)
    except Exception:
        traceback.print_exc()
        json_obj = None

    if json_obj != None :
        formated_json = dataProcesser.process_event_data(json_obj)
        db.insertNewEvent(formated_json)
        serviceNotifier.resetTimer()
    else :
        print("ERROR processing an event")


