import traceback
import xml.dom.minidom
import stomp
from static.dao import DAO

class UpdateEventListener(stomp.ConnectionListener, DAO):
    def __init__(self, conn, db, notifier):
        conn.connect('admin', 'admin', wait=True)
        self.message = None
        self.db = db
        self.notifier = notifier

    def on_before_message(self, headers, body):
        if 'transformation' in headers:
            trans_type = headers['transformation']
            if trans_type != 'jms-map-xml':
                return body

            try:
                entries = {}
                doc = xml.dom.minidom.parseString(body)
                rootElem = doc.documentElement
                for entryElem in rootElem.getElementsByTagName("entry"):
                    pair = []
                    for node in entryElem.childNodes:
                        print(node.firstChild)
                        if not isinstance(node, xml.dom.minidom.Element):
                            continue
                        pair.append(node.firstChild.nodeValue)
                    assert len(pair) == 2
                    entries[pair[0]] = pair[1]
                return (headers, entries)
            except Exception:
                #
                # unable to parse message. return original
                #
                traceback.print_exc()
                return (headers, body)

    def on_message(self, headers, body):
        self.message = body
        # self.db.insertNewEvent(self.message)
        print('Demande de MàJ reçue mais pas effectuée TODO "%s"' % self.message)