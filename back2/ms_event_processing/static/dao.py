'''
from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db_name = 'events_database'
db = client[db_name]
db_events = db["events"]

for x in db_events.find():
    print(x)
'''
from pymongo import MongoClient
from threading import Lock
import os
class SingletonDAO(type):
    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        """
        Singleton pour l'accès à la base de données
        """
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


class DAO(metaclass=SingletonDAO):
    urlconnection: str = None
    client: MongoClient = None
    db_name: str = None
    db: MongoClient = None
    db_events: MongoClient= None

    def __init__(self):
        self.urlconnection = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@mongo:27017'
        self.client = MongoClient(self.urlconnection)
        self.db_name = 'events_database'
        self.db = self.client[self.db_name]
        self.db_events = self.db["events"]

    def insertNewEvent(self, evt):
        self.db_events.insert_one(evt)

    def insertNewEvents(self, evt):
        self.db_events.insert_many(evt)

    def updateEvent(self, evtId, evt):
        row_to_update = { "event_id" : evtId}
        new_values = { "$set" : evt}
        self.db_events.update_one(row_to_update,new_values)

    def removeEvent(self, evtId):
        row_to_delete = { "event_id" : evtId}
        self.db_events.delete_one(row_to_delete)