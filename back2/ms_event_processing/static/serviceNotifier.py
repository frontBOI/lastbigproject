import time
from threading import Lock
import requests

class SingletonServiceNotifier(type):
    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        """
        Singleton pour le notificateur de service
        """
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]

class ServiceNotifier(metaclass=SingletonServiceNotifier):
    serverURL : str = None
    lastMessageDequedTimestamp : time = None
    timer : int = None

    def __init__(self):
        self.serverURL = "http://user-prediction-service:5000/updateModel"
        self.lastMessageDequedTimestamp = None
        self.timer = 5

    def resetTimer(self):
        self.lastMessageDequedTimestamp = time.time()

    def toggleNotification(self):
        current_time = time.time()
        if self.lastMessageDequedTimestamp != None :
            if((current_time-self.lastMessageDequedTimestamp) > self.timer):
                self.sendNotificationRequest()
                self.lastMessageDequedTimestamp = None

    def sendNotificationRequest(self):
        requests.get(self.serverURL)
        print("[HTTP] updateModel request sent")

    def evaluateUpdateRequest(self):
        while True:
            self.toggleNotification()
            time.sleep(1)