import re
import json
import string
from threading import Lock
import nltk
import spacy
from nltk.corpus import stopwords

class SingletonDataProcesser(type):
    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        """
        Singleton pour le formatteur de données
        """
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]

class DataProcesser(metaclass=SingletonDataProcesser) :

    def __init__(self):
        nltk.download('stopwords')
        pass

    def cleanHTML(self,raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        cleantext.translate(str.maketrans('', '', string.punctuation))
        return str(cleantext)

    def cleanEmojis(self,raw_text):
        cleanr = re.compile("["
                            u"\U0001F600-\U0001F64F"  # emoticons
                            u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                            u"\U0001F680-\U0001F6FF"  # transport & map symbols
                            u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                            u"\U00002702-\U000027B0"
                            u"\U000024C2-\U0001F251"
                            "]+", flags=re.UNICODE)
        return cleanr.sub(r'', raw_text)

    def cleanTextStr(self,raw_html_str):
        return self.cleanHTML(self.cleanEmojis(raw_html_str))


    def process_event_data(self,json_obj):
        current_event = json_obj
        valid_basic_structure  = False
        if "name" in current_event and "id" in current_event :
            valid_basic_structure = True
        if valid_basic_structure :
            if "description" in current_event and current_event["description"] != None and isinstance(current_event["description"],str):
                current_event["description"] = self.cleanTextStr(current_event["description"])
            else:
                current_event["description"] = "null"
            if (current_event["description"] != "null"):
                sample = current_event["description"]
                cleanr = re.compile(',')
                cleantext = re.sub(cleanr, ' ', sample)

                listOfDescWords = cleantext.split(" ")
                stop = set(stopwords.words('french'))
                listOfDescWordsCleaned = [x for x in listOfDescWords if x not in stop]

                lemmatizedDesc = []

                nlp = spacy.load('fr_core_news_md')
                processedNLPtext = nlp(" ".join(listOfDescWordsCleaned))

                for word in processedNLPtext:
                    lemmatizedDesc.append(word.lemma_)
                current_event["mining_ready"] = lemmatizedDesc
            else:
                current_event["mining_ready"] = "null"
            return current_event
        else:
            return None
