import sys

from static.addEventListner import AddEventListener
from static.serviceNotifier import ServiceNotifier
from threading import Thread



serviceNotifier = ServiceNotifier()
addEventListner = AddEventListener()

threadNotifier = Thread(target=serviceNotifier.evaluateUpdateRequest,args=[])
threadActiveMQ = Thread(target=addEventListner.connectToESB,args=[])
threadNotifier.start()
threadActiveMQ.start()
threadActiveMQ.join()
sys.exit(1) #Si on un pb pour se connecter à l'ESB, on finit le programme en erreur, Docker le redémarrera