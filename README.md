# LastBigProject

![](img/quandRienNeVaPlus.jpg)

> Un projet à la hauteur du talent de ces jeunes ingénieurs Lyonnais
> - Jacques Chirac


## Histoire

Le dernier projet de cette formation à l'école d'ingénieur CPE Lyon. Tout cela est parti d'une idée qui a su captiver l'attention de tout le monde. A partir de ce moment, le désir de créer quelque chose de grand à plusieurs est apparue. Alors, chacun a proposé son point de vue et a su user de son talent réthorique pour aboutir à un consensus: de là, les rôles ont été naturellement répartis et nous sommes partis à l'aventure...

## Sujet

Une map qui permet de visualiser les évènements les plus proches de soi, à visualiser lorsqu'on ne sait pas quoi faire de sa soirée. Une belle opportunité de passer des moments conviviaux avec ses amis, mais aussi des inconnus que l'on rencontrera au gré de ses envies.
Il est possible, grâce à un clustering et une analyse de données rigoureux, de filtrer les évènements selon leur type: désirez-vous vous défouler sur la piste de danse au rythmé d'un tango endiablé ? Ou peut-être êtes-vous plutôt friant de soirées mondaines autour d'un petit verre de Bordeaux... Le choix vous appartient, notre application saura répondre à toutes vos exigences.
Un design épuré et efficace grâce à des technologies modernes développé par deux ingénieurs full stack compétent vous apportera une navigation déliceusement addictive.

## Technologies

### Front
React native pour application mobile iOS et Android<br/>
[Documentation React Native](https://reactnative.dev/docs/getting-started)<br/>
[Expo tutorial](https://www.youtube.com/watch?v=0-S5a0eXPoc&feature=emb_title)<br/>
[Documentation React Maps](https://github.com/react-native-maps/react-native-maps)<br/>
[Trouver un material icon](https://materialdesignicons.com/)<br/>
[Uploader une image depuis le mobile en React Native](https://heartbeat.fritz.ai/how-to-upload-images-in-a-react-native-app-4cca03ded855)<br/>

### Back
SpringBoot MS

## Rôles

* PO: Amaury QUERON
* SCRUM master: Jérémy MOHARIRY
* Développeur fullstack: Tom BLANCHET
* Développeur fullstack: Nicolas OSPINA
* Développeur back: Bilge EKINCI
* Développeur back: Alexis CANEVALI

## Ressources

[Trello](https://trello.com/b/j6LqW1BC/agile-sprint-board)

## Fonctionnement sans CICD

Mettre à jour un microservices sur le cloud :
* Build le .jar : mvn clean package -DskipTests
* créer l'image (exemple avec eventComment, penser à changer le nom du service et son chemin) : docker build -t index.docker.io/amaurycpe/lastbigproject:event-comment-service back/eventComment
* pousser l'image sur docker hub : docker push index.docker.io/amaurycpe/lastbigproject:event-comment-service
* aller sur la vm gcp et aller au bon repertoire : /home/lastbigproject/lastbigproject
* récupérer le nom de container du service à update, l'arrêter et le supprimer : 
sudo docker container ls
sudo docker stop id-container
sudo docker rm id-container
* récupérer l'image du service concerné et la supprimer : sudo docker image ls
sudo docker image rm id-image

## Fonctionnement CICD

Il y a 3 parties : build, host deploy.
Build correspond à la création des fichiers jar pour chacuns des microservices Java.
Host consiste à la création de l'image docker en fonction du fichier jar généré précedemment et l'ajout de ce dernier dans le dockerhub du projet.
Deploy, pour finir, s'occupe de copier les fichiers de configuration ainsi que el docker-compose.yml dans la VM GCP et de lancer les conteneurs.

Pour ajouter votre microservices, il faut décommenter dans le fichier gitlab-ci.yml les lignes correspondantes à votre MS dans les parties :
    - variables
    - build
    - host

L'url externe statique pour accèder à la VM et aux microservices est 35.208.236.230.


Demarrer la VM :

tu te connecte à ce compte google (LastBigProject), tu vas sur gcp, tu choisis le projet lastbigproject en haut à gauche (à droite du menu dépliyant 3barre), tu devrais voir "compute engine" sur la gauche, tu vas dans instance de vm je crois puis tu selectionne la vm et t'appuie sur play en haut