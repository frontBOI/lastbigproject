import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// view elements
import Header from '../components/Header';
import Title from '../components/Title';
import Search from '../components/Search';
import LineSeparator from '../components/LineSeparator';
import Event from '../model/Event';

// store related
import { connect } from 'react-redux';

function MyEvents (props) 
{
    const navigation = useNavigation();

    const [userLikedEvents, setUserLikedEvents] = useState(props.user.events.liked)
    const [userParticipatingEvents, setUserParticipatingEvents] = useState(props.user.events.participating)

    const initialEvents = props.events.filter(event => {
        const isUserLiking = userLikedEvents.includes(event.id)
        const isUserParticipating = userParticipatingEvents.includes(event.id)
        return isUserLiking || isUserParticipating
    })

    const [filteredEvents, setFilteredEvents] = useState(initialEvents)

    // on s'abonne aux modifs de userReducer afin de mettre à jour le state lorsqu'un nouvel
    // évènement est liké ou participé
    useEffect(() => {
        if (props.user.events && props.user.events.liked && props.user.events.participating) {
            setFilteredEvents(props.events.filter(event => {
                const isUserLiking = props.user.events.liked.includes(event.id)
                const isUserParticipating = props.user.events.participating.includes(event.id)
                return isUserLiking || isUserParticipating
            }))
        }
    }, [ props.user ])
    
    return (
        <View>
            <Header/>
            <View style={style.wrapper}>
                <Title value={'Mes évènements'} />

                <TouchableHighlight 
                    style={style.additionalFeature}
                    onPress={() => navigation.navigate('CreateEvent', { name: 'Créer un évènement' })}
                >
                    <View style={style.iconAndText}>
                        <MaterialCommunityIcons name="plus" color={'#2c3e50'} size={25} />
                        <Text style={style.buttonText}>Créer un évènement</Text>
                    </View>
                </TouchableHighlight>

                <Search 
                    passedStyle={style.searchBar}
                    searchCallback=
                    {
                        (foundEvents) => {
                            setFilteredEvents(foundEvents)
                        }
                    }
                    events={initialEvents}
                />

                <LineSeparator />

                {
                    filteredEvents.map((event, index) => {
                        return (
                            <Event 
                                displayType='MY_EVENT' 
                                event={event} 
                                key={index}
                            />
                        )
                    })
                }
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    wrapper: {
        padding: 20
    },
    buttonText: {
        fontSize: 20,
        color: '#2c3e50',
        fontWeight: 'bold'
    },
    iconAndText: {
        flexDirection: 'row'
    },
    searchBar: {
        position: 'relative',
        width: '100%',
        top: 0,
        marginTop: 20,
        marginBottom: 20
    }
});

const mapStateToProps = state => {
    return {
        events: state.eventReducer.events,
        user: state.userReducer.user
    }
}
export default connect(mapStateToProps)(MyEvents);