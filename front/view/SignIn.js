import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Button, TextInput, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { validateInteger } from '../store/actions/helper'

// fonts
import { useFonts } from 'expo-font'

// store related
import store from '../store/store';
import { connect } from 'react-redux';
import { createUser } from '../store/actions/actions';

function validateMail (mail) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(mail).toLowerCase());
}

function SignIn (props) 
{
    const navigation = useNavigation();

    // form data
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [pseudo, setPseudo] = useState('')
    const [password, setPassword] = useState('')
    const [mail, setMail] = useState('')
    const [address, setAddress] = useState('')
    const [age, setAge] = useState('')
    const [hobbies, setHobbies] = useState('')
    const [areInputsValid, setAreInputsValid] = useState(true)
    const [errorMessage, setErrorMessage] = useState('')

    let [fontsLoaded] = useFonts({
        'Vogue': require('../assets/fonts/Vogue.ttf')
    })

    // Similaire à componentDidMount et componentDidUpdate
    // ici, s'active lorsque hasSuccessfullyBeenCreated du userReducer change d'état
    useEffect(() => {
        if (props.hasSuccessfullyBeenCreated) {
            navigation.navigate('Login')
        }
    });

    return (
        <ScrollView style={style.container}>

            {
                fontsLoaded ?  
                    <Text style={{...style.label, ...style.labelFonted}}>AroundUs</Text>
                    :
                    <Text style={style.label}>AroundUs</Text>
            }

            <View style={style.centerMe}>

                {
                    // message d'erreur lorsque le formulaire n'est pas correct
                    !areInputsValid ?
                        <View style={style.warning}>
                            <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'alert'} color={'white'} size={25} />
                            <Text style={style.warningMessage}>{errorMessage}</Text>
                        </View>
                        :
                        <></>
                }

                <View style={style.inputContainerHalfContainer}>
                    <View style={{...style.inputContainerHalf, marginRight: 20}}>
                        <TextInput
                            style={style.input}
                            onChangeText={value => { setAreInputsValid(true); setFirstName(value) }}
                            placeholder={'Prénom'}
                            value={firstName}
                        />
                    </View>

                    <View style={style.inputContainerHalf}>
                        <TextInput
                            style={style.input}
                            onChangeText={value => { setAreInputsValid(true); setLastName(value) }}
                            placeholder={'Nom'}
                            value={lastName}
                        />
                    </View>
                </View>

                <View style={style.inputContainer}>
                    <TextInput
                        style={style.input}
                        onChangeText={value => { setAreInputsValid(true); setPseudo(value) }}
                        placeholder={'Pseudo'}
                        value={pseudo}
                    />
                </View>

                <View style={style.inputContainer}>
                    <TextInput
                        style={style.input}
                        onChangeText={value => { setAreInputsValid(true); setPassword(value) }}
                        placeholder={'Mot de passe'}
                        value={password}
                        secureTextEntry={true}
                    />
                </View>

                <View style={style.inputContainer}>
                    <TextInput
                        style={style.input}
                        onChangeText={value => { setAreInputsValid(true); setMail(value) }}
                        placeholder={'Mail'}
                        value={mail}
                    />
                </View>

                <View style={style.inputContainer}>
                    <TextInput
                        style={style.input}
                        onChangeText={value => { setAreInputsValid(true); setAddress(value) }}
                        placeholder={'Adresse'}
                        value={address}
                    />
                </View>

                <View style={style.inputContainer}>
                    <TextInput
                        style={style.input}
                        onChangeText={value => {
                            setAreInputsValid(true)
                            if (validateInteger(value)) {
                                setAge(value)
                            }
                        }}
                        keyboardType={'numeric'}
                        placeholder={'Âge'}
                        value={age}
                    />
                </View>

                <View style={style.inputContainer}>
                    <TextInput
                        style={[style.input, {height: 70}]}
                        onChangeText={value => { setAreInputsValid(true); setHobbies(value) }}
                        placeholder={'Centres d\'intérêt'}
                        multiline={true}
                        value={hobbies}
                    />
                </View>

                <View style={style.button}>
                    <Button 
                        title={'\nCREER MON COMPTE\n'} 
                        color="black"
                        onPress={
                            () => 
                            {
                                let failedInputs = []
                                if (firstName.length == 0) {
                                    failedInputs.push('prénom')
                                }
                                if (lastName.length == 0) {
                                    failedInputs.push('nom')
                                }
                                if (pseudo.length == 0) {
                                    failedInputs.push('pseudo')
                                }
                                if (!validateMail(mail)) {
                                    failedInputs.push('mail')
                                }
                                if (address.length == 0) {
                                    failedInputs.push('adresse')
                                }
                                if (hobbies.length == 0) {
                                    failedInputs.push('hobbies')
                                }
                                if (failedInputs.length > 0) {
                                    setAreInputsValid(false)
                                    setErrorMessage(`Les champs suivants ne sont pas bons: ${failedInputs.join(', ').substring(-2)}`)
                                } else 
                                {
                                    const parsedHobbies = hobbies.replace(/\s/g, '').split(',')
                                    store.dispatch(createUser({
                                        name: lastName,
                                        firstName: firstName,
                                        pseudo: pseudo,
                                        password: password,
                                        mail: mail,
                                        address: address,
                                        age: age,
                                        sex: 'Troglodyte',
                                        hobbies: parsedHobbies,
                                    }))
                                }
                            }
                        }
                    />
                </View>    
            </View>

            {/* Bon lui il est nécessaire car autrement on ne peut pas scroller
                tout en bas de la page !... */}
            {/* <Text style={{marginTop: 20}}></Text> */}
        </ScrollView>
    )
}

const style = StyleSheet.create({
    container: {
        backgroundColor: '#2c3e50',
        height: '100%'
    },
    centerMe: {
        alignItems: 'center',
    },
    inputContainerHalfContainer: {
        flexDirection: 'row'
    },
    inputContainerHalf: {
        marginTop: 20,
        width: '32%',
        height: 35
    },  
    inputContainer: {
        marginTop: 20,
        width: '70%',
        height: 35
    },
    input: {
        borderColor: 'gray', 
        borderWidth: 1, 
        width: '100%',
        height: '100%',
        marginTop: 5,
        backgroundColor: 'white',
        padding: 5
    },
    label: {
        color: 'white',
        fontSize: 40,
        marginBottom: 10,
        textAlign: 'center',
        marginTop: 20
    },
    labelFonted: {
        fontFamily: 'Vogue'
    },
    button: {
        marginTop: 80,
        marginBottom: 50,
        width: '60%'
    },
    warning: {
        flexDirection: 'column',
        width: '80%',
        backgroundColor: '#c0392b',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        alignItems: "center",
        paddingLeft: 15
    },
    warningMessage: {
        color: 'white'
    }
});

const mapStateToProps = state => {
    return {
        hasSuccessfullyBeenCreated: state.userReducer.hasSuccessfullyBeenCreated,
    }
}
export default connect(mapStateToProps)(SignIn);