import React from 'react';
import { StyleSheet } from 'react-native';
import Event from '../model/Event';

export default function EventDetails ({ route }) 
{
    const { event } = route.params;

    return (
        <Event displayType='DETAILS' event={event} />
    )
}

const style = StyleSheet.create({
    
});