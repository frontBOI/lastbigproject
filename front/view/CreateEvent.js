import React, { Component } from 'react';
import { StyleSheet, TextInput, Button, Text, ScrollView, View, CheckBox, TouchableHighlight } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { validateInteger } from '../store/actions/helper'
import ImageUpload from '../components/Upload'

// view related
import Title from '../components/Title';
import DateTimePicker from '@react-native-community/datetimepicker';

// store related
import { connect } from 'react-redux';
import store from '../store/store';
import { createNewEvent, reinitSuccessMessage } from '../store/actions/actions';

class CreateEvent extends Component
{
    constructor (props) 
    {
        super(props);

        this.state = {
            event: {
                name: '',
                address: '',
                city: '',
                date: new Date(),
                startHour: new Date(),
                duration: '',
                description: '',
                price: '',
                websiteLink: '',
                coverPicture: '',
                online: false
            },
            areInputsValid: true,
            errorMessage: '',
            mounted: false,
            showDatePicker: false,
            showTimePicker: false,
            datePickerButtonLabel: 'Saisir la date',
            timePickerButtonLabel: 'Saisir l\'heure',
            showAddressAndCityInputs: true
        }

        store.dispatch(reinitSuccessMessage())
        this.componentDidUpdate = this.componentDidUpdate.bind(this);
    }

    setDateButtonLabel (selectedDate) {
        const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
        const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
        const year = selectedDate.getFullYear()
        const dayName = days[selectedDate.getDay()]
        const dayNumber = selectedDate.getDate()
        const month = months[selectedDate.getMonth()]
        const fancyDate = `${dayName} ${dayNumber} ${month.toLowerCase()} ${year}`

        if (this.state.mounted) {
            this.setState({datePickerButtonLabel: fancyDate})
        }
    }

    setTimeButtonLabel (selectedTime) {
        const hour = selectedTime.getHours()
        const minutes = selectedTime.getUTCMinutes()
        const fancyTime = `${hour < 10 ? '0' + hour : hour}:${minutes < 10 ? '0' + minutes : minutes}`

        if (this.state.mounted) {
            this.setState({timePickerButtonLabel: fancyTime})
        }
    }

    createEvent () 
    {
        if (this.state.mounted && this.checkInputsValidity()) {
            store.dispatch(createNewEvent({
                'date': this.state.event.date,
                'name': this.state.event.name,
                'duration': this.state.event.duration,
                'hour': this.state.event.startHour,
                'link': this.state.event.websiteLink,
                'ticketing': this.state.event.price > 0 ? '1': '0',
                'description': this.state.event.description,
                'address': this.state.event.address,
                'coverPicture': this.state.event.coverPicture,
                'city': this.state.event.city,
                'organisateurLocalId': this.props.user.id
            }))
        }
    }

    checkInputsValidity () 
    {
        if (this.state.mounted) {
            let invalidFields = []
            if (this.state.event.name.length == 0)
                invalidFields.push('nom')

            if (!this.state.event.online) {
                if (this.state.event.address.length == 0)
                invalidFields.push('adresse')

                if (this.state.event.city.length == 0)
                    invalidFields.push('ville')
            }

            if (this.state.event.startHour.length == 0)
                invalidFields.push('heure de début')

            if (this.state.event.duration.length == 0)
                invalidFields.push('durée')

            if (this.state.event.description.length == 0)
                invalidFields.push('description')

            let message = ''
            if (invalidFields.length == 1) {
                message = `Le champ "${invalidFields[0]}" est invalide`
            } else if (invalidFields.length > 1) {
                message = `Les champs `
                for (const f of invalidFields) {
                    message += `${f}, `
                }
                message = message.substring(0, message.length - 2)
                message += ' sont invalides'
            }

            const areInputsFinallyValidBoi = invalidFields.length == 0

            if (this.state.mounted) {
                this.setState({errorMessage: message})
                this.setState({areInputsValid: areInputsFinallyValidBoi})
            }
            return areInputsFinallyValidBoi
        }
    }

    resetFields () {
        if (this.state.mounted) {
            this.setState({
                ...this.state,
                event: {
                    name: '',
                    address: '',
                    city: '',
                    startHour: '',
                    duration: '',
                    description: '',
                    price: '',
                    websiteLink: '',
                }
            })
        }
    }

    componentDidMount () {
        this.setState({mounted: true})
    }

    componentWillUnmount () {
        this.setState({mounted: false})
    }

    componentDidUpdate(prevProps) 
    {
        if (this.state.mounted) 
        {
            // lorsque "showSuccess" change de valeur, on attend 6s et on le reset
            if (this.props.showSuccessMessage !== prevProps.showSuccessMessage) {
                this.resetFields()
                setTimeout(() => {
                    store.dispatch(reinitSuccessMessage())
                }, 6000)
            }
        }
    }

    render () {
        return (
            <ScrollView style={style.wrapper}>
                <Title value={'Saisir un nouvel évènement'} />

                {
                    // Message d'erreur lorsqu'un ou plusieurs <input> ne sont pas valides
                    this.state.areInputsValid ?
                    <></>
                    :
                    <View style={style.warning}>
                        <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'alert'} color={'white'} size={25} />
                        <Text style={style.warningMessage}>{this.state.errorMessage}</Text>
                    </View>
                }

                {
                    // Message de success
                    !this.props.showSuccessMessage ?
                    <></>
                    :
                    <View style={style.success}>
                        <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'hand-okay'} color={'white'} size={25} />
                        <Text style={style.warningMessage}>{this.props.successMessage}</Text>
                    </View>
                }

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            if (this.state.mounted) {
                                this.setState({event: {...this.state.event, name: text}})
                                this.setState({areInputsValid: true})
                            }
                        }
                    }
                    placeholder={'Nom de l\'évènement'}
                    value={this.state.event.name}
                />

                <View style={{flexDirection:'row', marginTop: 10, marginBottom: 10}}>
                    <CheckBox
                        value={this.state.event.online}
                        onValueChange={value => {
                            if (this.state.mounted) {
                                this.setState({event: {...this.state.event, online: value}})
                            }
                        }}
                        style={style.checkbox}
                    />

                    <Text style={{fontSize:15, paddingTop: 2}}>Evenement en ligne</Text>
                </View>

                {
                    !this.state.event.online ?
                        <View>
                            <TextInput
                                style={style.input}
                                onChangeText={
                                    text => {
                                        if (this.state.mounted) {
                                            this.setState({event: {...this.state.event, address: text}})
                                            this.setState({areInputsValid: true})
                                        }
                                    }
                                }
                                placeholder={'Adresse complète'}
                                value={this.state.event.address}
                            />

                            <TextInput
                                style={style.input}
                                onChangeText={
                                    text => {
                                        if (this.state.mounted) {
                                            this.setState({event: {...this.state.event, city: text}})
                                            this.setState({areInputsValid: true})
                                        }
                                    }
                                }
                                placeholder={'Ville'}
                                value={this.state.event.city}
                            />
                        </View>
                        :
                        <></>
                }

                <View style={style.pickerWrapper}>
                    <TouchableHighlight
                        onPress={() => { 
                            if (this.state.mounted) {
                                this.setState({showDatePicker: true}) 
                            }
                        }} 
                    >
                        <Text style={style.pickerButton}>{this.state.datePickerButtonLabel}</Text>
                    </TouchableHighlight>
                    {
                        this.state.showDatePicker && (
                            <DateTimePicker
                                testID="datePicker"
                                value={this.state.event.date}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                minimumDate={new Date()}
                                onChange={(event, selectedDate) => 
                                {
                                    if (this.state.mounted) {
                                        if (!event.type === 'neutralButtonPressed') {
                                            this.setState({event: {...this.state.event, date: selectedDate}})
                                        }
                                        this.setState({showDatePicker: false})
                                        this.setDateButtonLabel(selectedDate)
                                    }
                                }}
                            />
                        )
                    }

                    <TouchableHighlight
                        onPress={() => { 
                            if (this.state.mounted) {
                                this.setState({showTimePicker: true}) 
                            }
                        }} 
                    >
                        <Text style={style.pickerButton}>{this.state.timePickerButtonLabel}</Text>
                    </TouchableHighlight>
                    {
                        this.state.showTimePicker && (
                            <DateTimePicker
                                testID="timePicker"
                                value={this.state.event.startHour}
                                mode={'time'}
                                is24Hour={true}
                                display="default"
                                onChange={(event, selectedTime) => 
                                {
                                    if (this.state.mounted) {
                                        if (!event.type === 'neutralButtonPressed') {
                                            this.setState({event: {...this.state.event, startHour: selectedTime}})
                                        }
                                        this.setState({showTimePicker: false})
                                        this.setTimeButtonLabel(selectedTime)
                                    }
                                }}
                            />
                        )
                    }
                </View>

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            if (this.state.mounted) {
                                if (validateInteger(text)) {
                                    this.setState({event: {...this.state.event, duration: text}})
                                    this.setState({areInputsValid: true})
                                }
                            }
                        }
                    }
                    placeholder={'Durée (en nb heures)'}
                    keyboardType={'numeric'}
                    value={this.state.event.duration}
                />

                <TextInput
                    style={[style.input, {height: 70}]}
                    onChangeText={
                        text => {
                            if (this.state.mounted) {
                                this.setState({event: {...this.state.event, description: text}})
                                this.setState({areInputsValid: true})
                            }
                        }
                    }
                    placeholder={'Description'}
                    multiline={true}
                    value={this.state.event.description}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            if (this.state.mounted) {
                                if (validateInteger(text)) {
                                    this.setState({event: {...this.state.event, price: text}})
                                    this.setState({areInputsValid: true})
                                }
                            }
                        }
                    }
                    placeholder={'Tarif'}
                    keyboardType={'numeric'}
                    value={this.state.event.price}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            if (this.state.mounted) {
                                this.setState({event: {...this.state.event, websiteLink: text}})
                                this.setState({areInputsValid: true})
                            }
                        }
                    }
                    placeholder={'Lien vers le site de l\'évènement'}
                    value={this.state.event.websiteLink}
                />

                <ImageUpload 
                    uploadCallback={newAvatarBase64 => {
                        const newCover = 'data:image/png;base64,' + newAvatarBase64

                        if (this.state.mounted) {
                            this.setState({event: {...this.state.event, coverPicture: newCover}})
                        }
                    }}
                    text={'Télécharger une image de couverture'}
                    display={'LINE'}
                />

                {/* <TouchableHighlight 
                    style={style.additionalFeature}
                    onPress={() => console.log('à coder...')}
                >
                    <View style={style.iconAndText}>
                        <MaterialCommunityIcons name="plus" color={'#2c3e50'} size={25} />
                        <Text style={style.buttonText}>Inviter des amis</Text>
                    </View>
                </TouchableHighlight> */}

                <View style={style.validateButton}>
                    <Button 
                        title={'Valider'} 
                        color="#34495e"
                        onPress={() => this.createEvent()}
                    />
                </View>

                {/* Bon lui il est nécessaire car autrement on ne peut pas scroller
                tout en bas de la page !... */}
                <Text style={style.spacer}></Text>
            </ScrollView>
        )
    }
}

const style = StyleSheet.create({
    wrapper: {
        padding: 20
    },
    pickerWrapper: {
        
    },
    pickerButton: {
        color: '#3498db',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 20,
        textAlign: 'center'
    },
    input: { 
        height: 35, 
        borderColor: 'gray', 
        borderWidth: 1, 
        width: '90%',
        marginTop: 5,
        backgroundColor: 'white',
        padding: 5
    },
    buttonText: {
        fontSize: 20,
        color: '#2c3e50',
        fontWeight: 'bold'
    },
    iconAndText: {
        flexDirection: 'row'
    },
    additionalFeature: {
        marginTop: 20
    },
    validateButton: {
        marginTop: 20
    },
    spacer: {
        marginTop: 20
    },
    warning: {
        flexDirection: 'column',
        backgroundColor: '#c0392b',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        alignItems: "center",
        paddingLeft: 15
    },
    success: {
        flexDirection: 'column',
        backgroundColor: '#27ae60',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        alignItems: "center",
        paddingLeft: 15
    },
    warningMessage: {
        color: 'white'
    },
    checkbox: {
        width: 20, 
        height: 20,
        marginRight: 10,
        borderColor: 'black'
    }
});

const mapStateToProps = state => {
    return {
        user: state.userReducer.user,
        showSuccessMessage: state.eventReducer.showSuccessMessage,
        successMessage: state.eventReducer.successMessage,
    }
}
export default connect(mapStateToProps)(CreateEvent);