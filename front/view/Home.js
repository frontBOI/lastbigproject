import React, { Component } from 'react';
import { StyleSheet, View, Switch, StatusBar } from 'react-native';

// view elements
import Header from '../components/Header';
import Map from '../components/Map';
import Search from '../components/Search';
import Event from '../model/Event';

// store related
import { connect } from 'react-redux';

class HomeScreen extends Component 
{
    constructor (props) {
        super(props);

        this.state = {
            displayMap: true,
            filteredEvents: undefined
        }

        this.searchChild = undefined

        this.onSwitchToggle = this.onSwitchToggle.bind(this);
        this.searchCallback = this.searchCallback.bind(this);
    }

    onSwitchToggle () {
        const currentValue = this.state.displayMap

        // on set la nouvelle valeur de displayMap. La fonction qui est passée en second
        // paramètre sera exécutée lorsque le setState() sera effectif
        this.setState(
            {displayMap: !currentValue},
            () => { this.refs.searchChild.forceCallback() }
        )
        this.setState({filteredEvents: undefined})
    }

    searchCallback (foundEvents) {
        this.setState({filteredEvents: foundEvents})
    }

    componentDidUpdate(prevProps) 
    {
        if (this.props.allEvents !== prevProps.allEvents) {
            this.refs.searchChild.forceCallback()
        }
    }

    render () {
        return (
            <View>
                <Header/>
                <Switch
                    style={style.switch}
                    onValueChange={this.onSwitchToggle}
                    value={this.state.displayMap}
                />

                {
                    this.state.displayMap ? 
                        <Map
                            events={this.state.filteredEvents ? this.state.filteredEvents : this.props.locatedEvents}
                        /> 
                        : 
                        <View style={style.listContainer}>
                            <Event 
                                displayType='LIST' 
                                events={this.state.filteredEvents ? this.state.filteredEvents : this.props.allEvents}
                            />
                        </View>
                }

                <Search
                    searchCallback={this.searchCallback}
                    events={this.state.displayMap ? this.props.locatedEvents : this.props.allEvents}
                    ref={'searchChild'}
                />
            </View>
        )
    }
}

const style = StyleSheet.create({
    switch: {
        position: 'absolute', 
        top: StatusBar.currentHeight + 110, 
        right: 10, 
        zIndex: 10,
        elevation: 20
    },
    listContainer: {
        paddingBottom: 69
    }
});

const mapStateToProps = state => {
    return {
        locatedEvents: state.eventReducer.locatedEvents,
        allEvents: state.eventReducer.events,
    }
}
export default connect(mapStateToProps)(HomeScreen);