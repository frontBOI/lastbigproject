import React, { useEffect } from 'react';
import { StyleSheet, Button, Text, View, Image, ScrollView } from 'react-native';
import Title from '../components/Title';
import { useNavigation } from '@react-navigation/native';
import SoundPlayer from '../components/SoundPlayer'

// store related
import store from '../store/store';
import { connect } from 'react-redux';
import { logOut, deleteUser } from '../store/actions/actions';

function Compte_detailDisplay (props)
{
    const navigation = useNavigation();
    const fancyInterests = props.user.hobbies ? props.user.hobbies.map(e => e + '\n') : []

    let formatAvatar
    if (props.user.avatar) {
        formatAvatar = props.user.avatar.charAt(0) == '"' ?
            props.user.avatar.substring(1, props.user.avatar.length - 1)
            :
            props.user.avatar
    }

    // Similaire à componentDidMount et componentDidUpdate
    // ici, s'active lorsque isLogged du userReducer change d'état
    useEffect(() => {
        if (props.isLoggedOutSuccessful) {
            navigation.navigate('Login')
        }
    }, [ props.isLoggedOutSuccessful]);

    return (
        <ScrollView style={{paddingTop: 20}}>
            <View style={style.titleAndImage}>
                <Title value={'Détails du compte'}/>
                <View style={{paddingLeft: 100}}>
                    <Image 
                        style={style.avatar}
                        source={
                            props.user.avatar && props.user.avatar.length > 0 ?
                                { uri: formatAvatar }
                                :
                                require('../assets/user.png')
                        }
                    />
                </View>
            </View>

            <View style={style.propertyWrapper}>
                <Text style={style.propertyName}>Nom</Text>
                <Text style={style.propertyValue}>{props.user.name}</Text>
            </View>

            <View style={style.propertyWrapper}>
                <Text style={style.propertyName}>Prénom</Text>
                <Text style={style.propertyValue}>{props.user.firstName}</Text>
            </View>

            <View style={style.propertyWrapper}>
                <Text style={style.propertyName}>Email</Text>
                <Text style={style.propertyValue}>{props.user.mail}</Text>
            </View>

            <View style={style.propertyWrapper}>
                <Text style={style.propertyName}>Date de naissance</Text>
                <Text style={style.propertyValue}>{props.user.birthdate}</Text>
            </View>

            <View style={style.propertyWrapper}>
                <Text style={style.propertyName}>Ville préférée</Text>
                <Text style={style.propertyValue}>{props.user.favoriteCity}</Text>
            </View>

            <View style={style.propertyWrapper}>
                <Text style={style.propertyName}>Sexe</Text>
                <Text style={style.propertyValue}>{props.user.sex}</Text>
            </View>

            <View style={[style.propertyWrapper, {borderBottomWidth: 0, marginBottom: 10}]}>
                <Text style={style.propertyName}>Centres d'intérêt</Text>
                <Text style={style.propertyValue}>{fancyInterests}</Text>
            </View>

            <Title value={'Paramètres'}/>

            <View style={style.preferenceButton}>
                <Button 
                    title={'Changer mon mot de passe'} 
                    color="#34495e"
                    onPress={
                        () => navigation.navigate(
                            'ParameterChanger', 
                            { 
                                name: 'Mot de passe', 
                                parameterType: 'PASSWORD', 
                                user: props.user 
                            })
                    }
                />
            </View>

            <View style={style.preferenceButton}>
                <Button 
                    title={'Modifier mon avatar'} 
                    color="#34495e"
                    onPress={
                        () => navigation.navigate(
                            'ParameterChanger', 
                            { 
                                name: 'Avatar', 
                                parameterType: 'AVATAR', 
                                user: props.user 
                            })
                    }
                />
            </View>

            <View style={style.preferenceButton}>
                <Button 
                    title={'Modifier mes centres d\'intérêt'} 
                    color="#34495e"
                    buttonStyle={{marginTop: 100}}
                    onPress={
                        () => navigation.navigate(
                            'ParameterChanger', 
                            { 
                                name: 'Centres d\'intéret', 
                                parameterType: 'HOBBIES', 
                                user: props.user 
                            })
                    }
                />
            </View>

            <View style={style.preferenceButton}>
                <Button 
                    title={'Me déconnecter'} 
                    color="#c0392b"
                    buttonStyle={{marginTop: 100}}
                    onPress={() => { 
                        const sonDeconnexion = require(`../assets/sounds/deconnexion.mp3`)
                        SoundPlayer(sonDeconnexion)
                        store.dispatch(logOut(props.user.id)) 
                    }}
                />
            </View>

            <View style={style.preferenceButton}>
                <Button 
                    title={'Supprimer mon compte'} 
                    color="#c0392b"
                    buttonStyle={{marginTop: 100}}
                    onPress={() => { store.dispatch(deleteUser(props.user.id)) }}
                />
            </View>

            {/* Bon lui il est nécessaire car autrement on ne peut pas scroller
            tout en bas de la page !... */}
            <Text style={style.spacer}></Text>
        </ScrollView>
    )
}

const style = StyleSheet.create({
    titleAndImage: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    avatar: { 
        width: 100, 
        height: 100
    },
    propertyWrapper: {
        flexDirection: 'row',
        padding: 7,
        borderBottomWidth: 1,
        margin: 1
    },
    propertyName: {
        textAlign: 'left',
        width: '50%'
    },
    propertyValue: {
        textAlign: 'left',
        width: '50%'
    },
    spacer: {
        marginTop: 20
    },
    preferenceButton: {
        marginTop: 10
    }
});

const mapStateToProps = state => {
    return {
        user: state.userReducer.user,
        isLoggedOutSuccessful: state.userReducer.isLoggedOutSuccessful
    }
}
export default connect(mapStateToProps)(Compte_detailDisplay);