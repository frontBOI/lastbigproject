import React from 'react'
import { ImageBackground, StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'

function Event_listDisplay (props) 
{
    const navigation = useNavigation()

    const bigEnoughEventName = props.event.name.length > 35 ? 
        props.event.name
        :
        props.event.name + '\n'

    let shortenedDescription = props.event.description
    const maxLength = 150
    if (shortenedDescription && shortenedDescription.length > maxLength) {
        shortenedDescription = shortenedDescription.substring(0, maxLength) + '...'
    }

    return (
        <TouchableOpacity
            onPress={() => navigation.navigate('EventDetails', { name: props.event.name, event: props.event })}
        >
            <View style={style.wrapper}>
            
                <ImageBackground 
                    source={{ uri: props.event.coverPicture }} 
                    style={{
                        height: 200
                    }}
                />
                
                <View style={style.eventBox}>
                    <Text style={style.title}>{bigEnoughEventName}</Text>
                    <Text style={style.description}>{shortenedDescription}</Text>
                </View>

            </View>
        </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    wrapper: {
        padding: 10,
        paddingTop: 50 // la place pour le switch !
    },
    eventBox: {
        backgroundColor: 'white',
        shadowColor: "#000",

        // shadow
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10
    },
    title: {
        color: 'white',
        backgroundColor: 'rgba(44, 62, 80, .7)',
        fontSize: 20,
        marginBottom: 20,
        padding: 10,
        marginTop: -70,

        borderBottomWidth: 1,
        borderBottomColor: "rgb(44, 62, 80)"
    },
    description: {
        color: 'rgb(80, 112, 145)',
        padding: 10,
        marginTop: -20
    }
});

export default Event_listDisplay;