import React, { Component } from 'react';
import { StyleSheet, TextInput, Button, Text, ScrollView, View, TouchableHighlight } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { validateInteger } from '../store/actions/helper'
import ImageUpload from '../components/Upload'

// view related
import Title from '../components/Title';
import DateTimePicker from '@react-native-community/datetimepicker';

// store related
import { connect } from 'react-redux';
import store from '../store/store';
import { modifyEvent, reinitSuccessMessage } from '../store/actions/actions';

class ModifyEvent extends Component
{
    constructor (props) 
    {
        super(props);

        this.state = {
            event: {
                ...this.props.route.params.event,
                date: new Date(),
                startHour: new Date(),
            },
            areInputsValid: true,
            errorMessage: '',
            mounted: false,
            showDatePicker: false,
            showTimePicker: false,
            datePickerButtonLabel: 'Saisir la date',
            timePickerButtonLabel: 'Saisir l\'heure',
        }

        this.modifyEvent = this.modifyEvent.bind(this);
        this.checkInputsValidity = this.checkInputsValidity.bind(this);
    }

    setDateButtonLabel (selectedDate) {
        const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
        const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
        const year = selectedDate.getFullYear()
        const dayName = days[selectedDate.getDay()]
        const dayNumber = selectedDate.getDate()
        const month = months[selectedDate.getMonth()]
        const fancyDate = `${dayName} ${dayNumber} ${month.toLowerCase()} ${year}`
        this.setState({datePickerButtonLabel: fancyDate})
    }

    setTimeButtonLabel (selectedTime) {
        const hour = selectedTime.getHours()
        const minutes = selectedTime.getUTCMinutes()
        const fancyTime = `${hour < 10 ? '0' + hour : hour}:${minutes < 10 ? '0' + minutes : minutes}`
        this.setState({timePickerButtonLabel: fancyTime})
    }

    modifyEvent () 
    {
        if (this.state.mounted && this.checkInputsValidity()) {
            store.dispatch(modifyEvent({
                'id': this.state.event.id,
                'date': this.state.event.date,
                'name': this.state.event.name,
                'duration': this.state.event.duration,
                'hour': this.state.event.startHour,
                'link': this.state.event.websiteLink,
                'ticketing': this.state.event.price > 0 ? '1': '0',
                'description': this.state.event.description,
                'address': this.state.event.address,
                'public': this.state.event.public,
                'coverPicture': this.state.event.coverPicture,
                'comments': this.state.event.comments,
            }))
        }
    }

    checkInputsValidity () 
    {
        if (this.state.mounted) {
            let invalidFields = []
            const areInputsFinallyValidBoi = invalidFields.length == 0

            let message = ''
            this.setState({errorMessage: message})
            this.setState({areInputsValid: areInputsFinallyValidBoi})
            return areInputsFinallyValidBoi
        }
    }

    componentDidMount () {
        this.setState({mounted: true})
    }

    componentWillUnmount () {
        this.setState({mounted: false})
    }

    componentDidUpdate(prevProps) 
    {
        if (this.state.mounted) 
        {
            // lorsque "showSuccess" change de valeur, on attend 6s et on le reset
            if (this.props.showSuccessMessage !== prevProps.showSuccessMessage) {
                setTimeout(() => {
                    store.dispatch(reinitSuccessMessage())
                }, 6000)
            }
        }
    }

    render () {
        return (
            <ScrollView style={style.wrapper}>
                <Title value={'Modifier mon évènement'} />

                {
                    // Message d'erreur lorsqu'un ou plusieurs <input> ne sont pas valides
                    this.state.areInputsValid ?
                    <></>
                    :
                    <View style={style.warning}>
                        <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'alert'} color={'white'} size={25} />
                        <Text style={style.warningMessage}>{this.state.errorMessage}</Text>
                    </View>
                }

                {
                    // Message de success
                    !this.props.showSuccessMessage ?
                    <></>
                    :
                    <View style={style.success}>
                        <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'hand-okay'} color={'white'} size={25} />
                        <Text style={style.warningMessage}>{this.props.successMessage}</Text>
                    </View>
                }

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, name: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Nom de l\'évènement'}
                    value={this.state.event.name}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, address: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Adresse complète'}
                    value={this.state.event.address}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, city: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Ville'}
                    value={this.state.event.city}
                />

                <View style={style.pickerWrapper}>
                    <TouchableHighlight
                        onPress={() => { this.setState({showDatePicker: true}) }} 
                    >
                        <Text style={style.pickerButton}>{this.state.datePickerButtonLabel}</Text>
                    </TouchableHighlight>
                    {
                        this.state.showDatePicker && (
                            <DateTimePicker
                                testID="datePicker"
                                value={this.state.event.date}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={(event, selectedDate) => 
                                {
                                    if (!event.type === 'neutralButtonPressed') {
                                        this.setState({event: {...this.state.event, date: selectedDate}})
                                    }
                                    this.setState({showDatePicker: false})
                                    this.setDateButtonLabel(selectedDate)
                                }}
                            />
                        )
                    }

                    <TouchableHighlight
                        onPress={() => { this.setState({showTimePicker: true}) }} 
                    >
                        <Text style={style.pickerButton}>{this.state.timePickerButtonLabel}</Text>
                    </TouchableHighlight>
                    {
                        this.state.showTimePicker && (
                            <DateTimePicker
                                testID="timePicker"
                                value={this.state.event.startHour}
                                mode={'time'}
                                is24Hour={true}
                                display="default"
                                onChange={(event, selectedTime) => 
                                {
                                    if (!event.type === 'neutralButtonPressed') {
                                        this.setState({event: {...this.state.event, startHour: selectedTime}})
                                    }
                                    this.setState({showTimePicker: false})
                                    this.setTimeButtonLabel(selectedTime)
                                }}
                            />
                        )
                    }
                </View>

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, duration: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Durée'}
                    value={this.state.event.duration}
                />

                <TextInput
                    style={[style.input, {height: 70}]}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, description: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Description'}
                    multiline={true}
                    value={this.state.event.description}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, public: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Public visé'}
                    value={this.state.event.public}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, price: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Tarif'}
                    value={this.state.event.price}
                />

                <TextInput
                    style={style.input}
                    onChangeText={
                        text => {
                            this.setState({event: {...this.state.event, link: text}})
                            this.setState({areInputsValid: true})
                        }
                    }
                    placeholder={'Lien vers le site de l\'évènement'}
                    value={this.state.event.link}
                />
                
                <ImageUpload 
                    uploadCallback={newAvatarBase64 => {
                        const newCover = 'data:image/png;base64,' + newAvatarBase64
                        this.setState({event: {...this.state.event, coverPicture: newCover}})
                    }}
                    text={'Changer l\'image de couverture'}
                    display={'LINE'}
                />

                <View style={style.validateButton}>
                    <Button 
                        title={'Modifier'} 
                        color="#34495e"
                        onPress={() => this.modifyEvent()}
                    />
                </View>

                {/* Bon lui il est nécessaire car autrement on ne peut pas scroller
                tout en bas de la page !... */}
                <Text style={style.spacer}></Text>
            </ScrollView>
        )
    }
}

const style = StyleSheet.create({
    wrapper: {
        padding: 20
    },
    pickerButton: {
        color: '#3498db',
        marginTop: 10,
        marginBottom: 10,
        fontSize: 20,
        textAlign: 'center'
    },
    input: { 
        height: 35, 
        borderColor: 'gray', 
        borderWidth: 1, 
        width: '90%',
        marginTop: 5,
        backgroundColor: 'white',
        padding: 5
    },
    buttonText: {
        fontSize: 20,
        color: '#2c3e50',
        fontWeight: 'bold'
    },
    iconAndText: {
        flexDirection: 'row'
    },
    additionalFeature: {
        marginTop: 20
    },
    validateButton: {
        marginTop: 20
    },
    spacer: {
        marginTop: 20
    },
    warning: {
        flexDirection: 'column',
        backgroundColor: '#c0392b',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        alignItems: "center",
        paddingLeft: 15
    },
    success: {
        flexDirection: 'column',
        backgroundColor: '#27ae60',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        alignItems: "center",
        paddingLeft: 15
    },
    warningMessage: {
        color: 'white'
    }
});

const mapStateToProps = state => {
    return {
        showSuccessMessage: state.eventReducer.showSuccessMessage,
        successMessage: state.eventReducer.successMessage,
    }
}
export default connect(mapStateToProps)(ModifyEvent);