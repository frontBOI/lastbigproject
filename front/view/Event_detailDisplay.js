import React, { useEffect, useState } from 'react';
import { StyleSheet,Image, Linking,  ScrollView, Text, Button, View, TouchableHighlight, TextInput, Clipboard } from 'react-native';
import Title from '../components/Title';
import { useNavigation } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';

// store related
import { connect } from 'react-redux';
import store from '../store/store';
import { likeEvent, participateEvent, loadComments, postComment } from '../store/actions/actions';
import { setHasCommentBeenPosted_action } from '../store/actions/actions_pure';

function Event_detailDisplay (props) 
{
    const navigation = useNavigation()
    const socialButtonsSize = 50

    // fancying event's start date
    const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
    const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
    let eventDateJS = new Date(props.event.date)
    const year = eventDateJS.getFullYear()
    const dayName = days[eventDateJS.getDay()]
    const dayNumber = eventDateJS.getDate()
    const month = months[eventDateJS.getMonth()]
    const fancyDate = `${dayName} ${dayNumber} ${month.toLowerCase()} ${year} ${props.event.hour ? 'à ' + props.event.hour : ''}`

    // fancying description
    let firstLetterIndex = 0, i
    const rawDescription = props.event.description
    if (rawDescription == 'null') {
        rawDescription = 'Aucune description fournie'
    }
    for (i = 0 ; i < rawDescription.length ; i++) {
        if (rawDescription[i].match(/[a-zA-Z]/)) {
            break
        }
        firstLetterIndex++
    }
    const descriptionFirstLetter = rawDescription[firstLetterIndex]
    const descriptionRestOfLetters = rawDescription.substring(firstLetterIndex + 1, rawDescription.length)

    const [comment, onChangeComment] = useState()
    const [hasCommentsBeenLoaded, setHasCommentsBeenLoaded] = useState(false)
    const [isUserLiking, onChangeIsUserLiking] = useState(props.user.events.liked.includes(props.event.id))
    const [isUserParticipating, onChangeIsUserParticipating] = useState(props.user.events.participating.includes(props.event.id))
    const [currentEventComments, setCurrentEventComment] = useState(props.comments.filter(c => c.eventId == props.event.id))
    const [hasCommentBeenPosted, setHasCommentBeenPosted] = useState(false)

    // on s'abonne aux modifs de userReducer afin de mettre à jour le state lorsque c'est nécessaire
    useEffect(() => {
        const newProps_isLiking = props.user.events.liked.includes(props.event.id)
        const oldProps_isLiking = isUserLiking
        if (newProps_isLiking !== oldProps_isLiking) {
            onChangeIsUserLiking(newProps_isLiking)
        }

        const newProps_isParticipating = props.user.events.participating.includes(props.event.id)
        const oldProps_isParticipating = isUserParticipating
        if (newProps_isParticipating !== oldProps_isParticipating) {
            onChangeIsUserParticipating(newProps_isParticipating)
        }

        const newComments = props.comments.filter(c => c.eventId == props.event.id)
        const oldComments = currentEventComments
        if (newComments.length > oldComments.length) {
            setCurrentEventComment(newComments)
        }

        const newProps_hasCommentBeenPosted = props.hasCommentBeenPosted
        const oldProps_hasCommentBeenPosted = hasCommentBeenPosted
        if (
            newProps_hasCommentBeenPosted !== oldProps_hasCommentBeenPosted && 
            newProps_hasCommentBeenPosted == true
        ) {
            handlePostedComment()
        }
    });

    // chargement des commentaires de l'event
    if (!hasCommentsBeenLoaded) {
        store.dispatch(loadComments(props.event.id))
        setHasCommentsBeenLoaded(true)
    }

    const onPressLike = () => {
        store.dispatch(likeEvent(props.event.id, props.user.id, !isUserLiking))
    }
    
    const onPressParticipate = () => {
        store.dispatch(participateEvent(props.event.id, props.user.id, !isUserParticipating))
    }

    const handlePostedComment = () => {
        store.dispatch(loadComments(props.event.id))
        store.dispatch(setHasCommentBeenPosted_action(false))
    }
    
    const onPressShare = (value) => {
        switch (value) {
            case 1:
                const eventLink = props.event.link
                if (eventLink)
                    Clipboard.setString(eventLink);
                break;

            case 2:
                console.log('TODO: partager avec mes amis')
                break;

            default:
                console.log('> Erreur: option de <Menu> inconnue')
                break;
        }
    }

    const allerOnPostLeCommentaire = () => {
        if (comment.length > 0) {
            store.dispatch(postComment(props.event.id, props.user.pseudo, props.user.id, comment))
            onChangeComment('')
        }
    }

    return (
        <ScrollView>

            <Image 
                style={style.image}
                source={{uri: props.event.coverPicture}}
            />

            <View style={style.wrapper}>

                <View style={style.socialButtons}>
                    <TouchableHighlight 
                        style={style.socialButton}
                        onPress={onPressLike}
                    >
                        <MaterialCommunityIcons 
                            name={isUserLiking ? 'heart' : 'heart-outline'} 
                            color={isUserLiking ? '#e74c3c' : '#7f8c8d'} 
                            size={socialButtonsSize} 
                        />
                    </TouchableHighlight>

                    <TouchableHighlight 
                        style={style.socialButton}
                        onPress={onPressParticipate}
                    >
                        <MaterialCommunityIcons 
                            name={'checkbox-marked-circle-outline'} 
                            color={isUserParticipating ? '#27ae60' : '#7f8c8d'} 
                            size={socialButtonsSize} 
                        />
                    </TouchableHighlight>

                    <Menu 
                        style={style.contextMenu}
                        onSelect={value => onPressShare(value)}
                    >
                        <MenuTrigger>
                            <MaterialCommunityIcons 
                                name={'share-outline'} 
                                color={'#2c3e50'} 
                                size={socialButtonsSize} 
                            />
                        </MenuTrigger>

                        <MenuOptions>
                            <MenuOption style={{color: '#2c3e50'}} value={1} text='Copier le lien' />
                            <MenuOption style={{color: '#2c3e50'}} value={2} text='Partager avec mes amis'/>
                        </MenuOptions>
                    </Menu>
                </View>

                <View style={style.dateHourAndAdress}>
                    <Text style={style.dateAndHour}>{fancyDate}</Text>

                    {
                        // Afficher l'adresse du lieu si elle a été renseignée
                        props.event.address && props.event.address.includes('Événement en ligne') ? 
                            <></> 
                            : 
                            <Text style={style.address}>Adresse: {props.event.address}</Text>
                    }
                </View>

                <View style={style.descriptionWrapper}>
                    <Text style={style.descriptionFirstLetter}>{descriptionFirstLetter}</Text>
                    <Text style={style.description}>{descriptionRestOfLetters}</Text>
                </View>

                {
                    // Afficher si il est nécessaire d'acheter un billet pour participer
                    props.event.ticketing ? 
                        <View style={style.warning}>
                            <MaterialCommunityIcons style={{marginRight: 10}} name={'alert'} color={'white'} size={25} />
                            <Text style={style.ticketing}>Il est nécessaire d'avoir un billet pour participer !</Text>
                        </View>
                        :
                        <></> 
                }

                <Button 
                    title={'Lien vers le site'} 
                    style={style.link} 
                    onPress={() => { Linking.openURL(props.event.link)}} 
                />

                {
                    // Si l'évènement a été créé par le user, alors il peut le modifier
                    props.isUserEvent ?
                        <View style={{marginTop: 20}}>
                            <Button 
                                title={'Modifier l\'évènement'} 
                                style={style.link} 
                                onPress={() => navigation.navigate('ModifyEvent', { name: 'Modifier mon évènement', event: props.event })}
                            />
                        </View>
                        :
                        <></>
                }

                <View style={style.commentSection}>
                    <Title value='Commentaires        ' />

                    {
                        currentEventComments.length > 0 ?
                            currentEventComments.map((c, index) => 
                            {
                                return (
                                    <View 
                                        key={index}
                                        style={style.comment}
                                    >
                                        <Text style={style.commentMeta}>{c.author}, {c.date}</Text>
                                        <Text style={style.commentBody}>{c.text}</Text>
                                    </View>
                                )
                            })
                            :
                            <Text style={style.commentInfoText}>Aucun commentaire</Text>
                    }

                    {/* LINE SEPARATOR */}
                    <View
                        style={{
                            borderBottomColor: '#2c3e50',
                            borderBottomWidth: 1,
                            marginTop: 20,
                            marginBottom: 30
                        }}
                    />

                    <View style={style.commentForm}>
                        <TextInput
                            style={style.input}
                            onChangeText={value => onChangeComment(value)}
                            placeholder={'Laisser un commentaire'}
                            multiline={true}
                            value={comment}
                        />

                        <View style={style.validateButton}>
                            <Button 
                                title={'Envoyer'} 
                                color="#34495e"
                                onPress={allerOnPostLeCommentaire}
                            />
                        </View>
                    </View>
                </View>

                {/* Bon lui il est nécessaire car autrement on ne peut pas scroller
                    tout en bas de la page !... */}
                <Text style={style.spacer}></Text>
            </View>
        </ScrollView>
    )
}

const style = StyleSheet.create({
    wrapper: {
        padding: 20
    },
    header: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        padding: 10,
        backgroundColor: '#2c3e50',

        // cette propriété émule un "width: auto"
        alignSelf:"flex-start"
    },
    image: {
        width: '100%',
        height: 200
    },
    dateAndHour: {
        fontStyle: 'italic',
        alignItems: 'center'
    },
    address: {
        marginTop: 10
    },
    dateHourAndAdress: {
        marginBottom: 20
    },
    warning: {
        backgroundColor: '#c0392b',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 15
    },
    ticketing: {
        color: 'white'
    },
    descriptionWrapper: {
        flexDirection: 'column'
    },
    description: {
        marginBottom: 50,
    },
    descriptionFirstLetter: {
        color: '#2c3e50',
        fontSize: 40
    },
    link: {
        fontStyle: 'italic',
        alignItems: 'center',
        marginTop: 20
    },
    spacer: {
        marginTop: 20
    },
    socialButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 30,
    },
    commentSection: {
        marginTop: 40
    },
    commentForm: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    input: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#2c3e50',
        padding: 10,
        width: '65%',
        height: 80
    },
    validateButton: {
        marginTop: 20,
        width: '30%'
    },
    comment: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#2c3e50',
        padding: 10,
        marginBottom: 10
    },
    commentBody: {
        fontSize: 20
    },
    commentMeta: {
        flexDirection: 'row',
        marginBottom: 10,
        fontStyle: 'italic',
        fontSize: 12
    },
    commentInfoText: {
        fontStyle: 'italic',
        color: '#7f8c8d',
        fontSize: 25,
        textAlign: 'center'
    }
});

const mapStateToProps = state => {
    return {
        user: state.userReducer.user,
        comments: state.eventReducer.comments,
        hasCommentBeenPosted: state.eventReducer.hasCommentBeenPosted
    }
}
export default connect(mapStateToProps)(Event_detailDisplay);