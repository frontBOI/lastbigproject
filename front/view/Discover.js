import React from 'react';
import { StyleSheet, Text } from 'react-native';

// view elements
import Header from '../components/Header';
import Title from '../components/Title';
import Event from '../model/Event';

// store related
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';

function DiscoverScreen (props) 
{
    return (
        <ScrollView>
            <Header/>

            <Title 
                spacing={10} 
                value={'My discoveries           '} 
            />

            <Text style={style.subTitle}>Suggested for you</Text>
            
            {
                props.discoverEvents.map((event, index) => {
                    return (
                        <Event 
                            displayType='MY_EVENT' 
                            event={event} 
                            key={index}
                        />
                    )
                })
            }
        </ScrollView>
    )
}

const style = StyleSheet.create({
    subTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        paddingLeft: 15
    }
});

const mapStateToProps = state => {
    return {
        discoverEvents: state.eventReducer.discoverEvents,
    }
}
export default connect(mapStateToProps)(DiscoverScreen);