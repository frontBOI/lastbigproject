import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Button, Text } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

import Password from '../components/ParameterChange/Password'
import Avatar from '../components/ParameterChange/Avatar'
import Hobbies from '../components/ParameterChange/Hobbies'

// store related
import store from '../store/store'
import { connect } from 'react-redux';
import { updateUser as __updateUser__ } from '../store/actions/actions'
import { setParameterModificationSuccessful } from '../store/actions/actions_pure'

function ParameterChanger (props)
{
    const navigation = useNavigation()

    let [currentParameterChanger, setCurrentParameterChanger] = useState(null)
    let [user, setUser] = useState(props.__user__)
    let [showSuccess, setShowSuccess] = useState(false)
    let [successMessage, setSuccessMessage] = useState(false)

    const showSuccessMessage = (message) => {
        setShowSuccess(true)
        setSuccessMessage(message)

        setTimeout(() => { setShowSuccess(false)}, 3000)
    }

    const showErrorMessage = (message) => {
        console.log('showing error message "' + message + '"')
    }

    const validateAndDispatch = () => {
        const validationResult = currentParameterChanger.validate()
        if (validationResult.success === true) {
            const newUser = validationResult.data
            setUser(newUser)
            store.dispatch(__updateUser__(newUser))
        } else {
            showErrorMessage(validationResult.errorMessage)
        }
    }

    // définition des comportements à adopter lors d'un update du state
    useEffect(() => {
        if (props.paramaterModificationSuccessful) {
            showSuccessMessage('Votre profil a été mis à jour')
            store.dispatch(setParameterModificationSuccessful(false))
        }


    }, [ props.paramaterModificationSuccessful ]);

    let updaterComponent
    switch (props.route.params.parameterType) 
    {
        case 'PASSWORD':
            updaterComponent = (
                <Password 
                    childRef={ref => setCurrentParameterChanger(ref)}
                    user={user}
                />
            )
            break;

        case 'AVATAR':
            updaterComponent = (
                <Avatar 
                    childRef={ref => setCurrentParameterChanger(ref)}
                    user={user}
                />
            )
            break;

        case 'HOBBIES':
            updaterComponent = (
                <Hobbies 
                    childRef={ref => setCurrentParameterChanger(ref)}
                    user={user}
                />
            )
            break;

        default:
            return <Text>Type de paramètre inconnu</Text>
    }

    return (
        <View>

            { updaterComponent }

            <View style={{marginTop: 20}}>
                <Button 
                    title={'Valider'} 
                    color="#34495e"
                    onPress={() => validateAndDispatch()}
                />
            </View>

            {
                // Message de success
                showSuccess ?
                    <View style={style.wrapper}>
                        <View style={style.success}>
                            <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'hand-okay'} color={'white'} size={25} />
                            <Text style={style.warningMessage}>{successMessage}</Text>
                        </View>
                    </View>
                    :
                    <></>
            }
        </View>
    )
}

const style = StyleSheet.create({
    wrapper: {
        marginTop: 30,
        justifyContent: 'center',
        zIndex: 1000
    },
    success: {
        flexDirection: 'column',
        backgroundColor: '#27ae60',
        padding: 5,
        textAlign: 'center',
        alignItems: "center",
        paddingLeft: 15,
        height: 100
    },
    warningMessage: {
        color: 'white'
    }
});

const mapStateToProps = state => {
    return {
        __user__: state.userReducer.user,
        paramaterModificationSuccessful: state.userReducer.paramaterModificationSuccessful
    }
}
export default connect(mapStateToProps)(ParameterChanger)