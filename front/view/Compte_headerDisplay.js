import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, TouchableHighlight } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function Compte_detailDisplay () 
{
    const navigation = useNavigation();

    return (
        <TouchableHighlight 
            style={style.wrapper}
            onPress={() => navigation.navigate('Compte')}
        >
            <MaterialCommunityIcons name="account-circle-outline" color={'white'} size={50} />
        </TouchableHighlight>
    )
}

const style = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        right: 15,
        top: 15
    },
    image: {
        width: 50,
        height: 50,
        resizeMode: 'stretch',
    }
});