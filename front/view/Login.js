import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { SafeAreaView } from 'react-native-safe-area-context';
import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableHighlight, View, Text, Button, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import SoundPlayer from '../components/SoundPlayer'

// fonts
import { useFonts } from 'expo-font'

// store related
import store from '../store/store';
import { connect } from 'react-redux';
import { logUser, loadEvents, loadDiscoverEvents } from '../store/actions/actions';
import { setLoginErrorMessage } from '../store/actions/actions_pure'

function Login (props) 
{
    const navigation = useNavigation();

    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [hasTriedOnce, setHasTriedOnce] = useState(false)
    const [showError, setShowError] = useState(false)
    const [errorMessage, setErrorMessage] = useState(false)

    let [fontsLoaded] = useFonts({
        'Vogue': require('../assets/fonts/Vogue.ttf')
    })

    // Similaire à componentDidMount et componentDidUpdate
    useEffect(() => {
        if (hasTriedOnce) 
        {
            if (props.isLogged) {
                setShowError(false)
                setHasTriedOnce(false)
                setLogin('')
                setPassword('')

                // on charge un premier set d'events à découvrir
                store.dispatch(loadDiscoverEvents(props.userId))

                // fetch premier set d'events du cycle de vie de l'appli
                store.dispatch(loadEvents(props.originalLocation.latitude, props.originalLocation.longitude))

                navigation.navigate('BottomTabs')
            } 
        }

        if (props.loginErrorMessage.length > 0) {
            showErrorMessage(props.loginErrorMessage)
        }
    }, [ props.isLogged, props.loginErrorMessage ]);

    const showErrorMessage = errorMessage => {
        setShowError(true)
        setErrorMessage(errorMessage)

        setTimeout(() => {
            setShowError(false)
            store.dispatch(setLoginErrorMessage(''))
        }, 3000)
    }

    return (
        <SafeAreaView style={style.container}>
            {
                fontsLoaded ?  
                    <Text style={{...style.label, ...style.labelFonted}}>AroundUs</Text>
                    :
                    <Text style={style.label}>AroundUs</Text>
            }

            <View style={style.inputContainer}>
                <TextInput
                    style={style.input}
                    onChangeText={value => setLogin(value)}
                    placeholder={'Login'}
                    value={login}
                />
            </View>

            <View style={style.inputContainer}>
                <TextInput
                    style={style.input}
                    onChangeText={value => setPassword(value)}
                    placeholder={'Password'}
                    value={password}
                    secureTextEntry={true}
                />
            </View>

            <View style={style.button}>
                <Button 
                    title={'\nCONNEXION\n'} 
                    color="black"
                    onPress={
                        () => 
                        {
                            const sonConnexion = require(`../assets/sounds/connexion.mp3`)
                            SoundPlayer(sonConnexion)

                            if (login.length === 0 && password.length === 0) {
                                showErrorMessage('On a besoin d\'un login et d\'un mot de passe')
                            } else if (login.length === 0) {
                                showErrorMessage('Il faut renseigner le login tout de même')
                            } else if (password.length === 0){
                                showErrorMessage('Il faut renseigner le mot de passe voyons')
                            } else 
                            {
                                setHasTriedOnce(true)
                                store.dispatch(logUser(login, password))
                            }
                        }
                    }
                />
            </View>

            <TouchableHighlight
                onPress={() => {navigation.navigate('SignIn')}}
            >
                <Text style={style.createAccount}>Créer un compte</Text>
            </TouchableHighlight>

            {
                showError ?
                    <View style={style.warning}>
                        <MaterialCommunityIcons style={{marginTop: 10, marginRight: 10, marginBottom: 10}} name={'alert'} color={'white'} size={25} />
                        <Text style={style.warningMessage}>{errorMessage}</Text>
                    </View>
                    :
                    <></>
            }
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    container: {
        paddingTop: 150,
        alignItems: 'center',
        backgroundColor: '#2c3e50',
        height: '100%'
    },
    inputContainer: {
        marginTop: 20,
        width: '70%',
        height: 35
    },
    input: {
        borderColor: 'gray', 
        borderWidth: 1, 
        width: '100%',
        height: '100%',
        marginTop: 5,
        backgroundColor: 'white',
        padding: 5
    },
    label: {
        color: 'white',
        fontSize: 65,
        marginBottom: 10
    },
    labelFonted: {
        fontFamily: 'Vogue'
    },
    warning: {
        flexDirection: 'column',
        backgroundColor: '#c0392b',
        padding: 5,
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
        alignItems: "center",
    },
    warningMessage: {
        color: 'white'
    },
    button: {
        marginTop: 50,
        marginBottom: 50,
        width: '60%'
    },
    createAccount: {
        color: 'white',
        fontStyle: 'italic',
        fontSize: 15
    }
});

const mapStateToProps = state => {
    return {
        userId: state.userReducer.user.id,
        isLogged: state.userReducer.isLogged,
        loginErrorMessage: state.userReducer.loginErrorMessage,
        originalLocation: state.userReducer.originalLocation
    }
}
export default connect(mapStateToProps)(Login);