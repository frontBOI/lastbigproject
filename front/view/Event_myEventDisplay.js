import React from 'react';
import { StyleSheet, Text, Button, TouchableHighlight, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

function Event_myEventDisplay (props) 
{
    const navigation = useNavigation();

    // troncage du nom de l'event si il est trop long
    let fancyName = props.event.name;
    if (fancyName.length > 45) {
        fancyName = fancyName.substring(0, 45) + '...'
    }

    return (
        <TouchableHighlight 
            style={style.wrapper}
            onPress={() => navigation.navigate('EventDetails', { name: props.event.name, event: props.event })}
        >
            <View style={style.subWrapper}>
                {/* {
                    props.isLiked ? 
                        <MaterialCommunityIcons 
                            style={style.heartIcon}
                            name="heart-outline" 
                            color={'#2c3e50'} 
                            size={25} 
                        />
                        :
                        <MaterialCommunityIcons 
                            style={style.heartIcon}
                            name="heart" 
                            color={'#ff6b6b'} 
                            size={25} 
                        />
                } */}
                <Text>{fancyName}</Text>
            </View>
        </TouchableHighlight>
    )
}

const style = StyleSheet.create({
    wrapper: {
        marginTop: 10
    },
    subWrapper: {
        flexDirection: 'row',
        padding: 5,
        alignItems: 'center'
    },
    heartIcon: {
        paddingLeft: 10,
        marginRight: 10
    }
});

export default Event_myEventDisplay;