import React from 'react';
import { View, Text, StyleSheet  } from 'react-native';

// view elements
import Header from '../components/Header';

export default function MyFriendsScreen () {
    return (
        <View>
            <Header/>
            <Text>Mes amis !</Text>
        </View>
    )
}

const style = StyleSheet.create({
    
});