import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

// views
import Compte_headerDisplay from '../view/Compte_headerDisplay';
import Compte_detailDisplay from '../view/Compte_detailDisplay';

export default function Compte (props) 
{
    let display;
    if (props.displayType == 'HEADER') {
        display = (
            <Compte_headerDisplay />
        )
    } else {
        display = (
            <Compte_detailDisplay />
        )
    }

    return display
}