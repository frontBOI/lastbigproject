import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';

// view elements
import Event_listDisplay from '../view/Event_listDisplay';
import Event_detailDisplay from '../view/Event_detailDisplay';
import Event_myEventDisplay from '../view/Event_myEventDisplay';

class Event extends Component 
{
    constructor (props) {
        super(props);

        this.state = {
        }
    }

    render () {
        let view
        switch (this.props.displayType) 
        {
            case 'LIST':
                view = (
                    <ScrollView style={style.safeview}>
                        {
                            this.props.events.map((event, index) => 
                            {
                                return (
                                    <Event_listDisplay 
                                        key={index}
                                        isLastChild={index == this.props.events.length ? 'true' : 'false'}
                                        event={event}
                                    />
                                )
                            })
                        }
                    </ScrollView>
                )
                break;

            case 'DETAILS':
                view = (
                    <Event_detailDisplay 
                        event={this.props.event} 
                        isUserEvent={true}
                    />
                )
                break;
            
            case 'MY_EVENT':
                view = (
                    <Event_myEventDisplay event={this.props.event} />
                )
                break;

            default:
                view = (
                    <Text>Type d'affichage de l'event inconnu ("{this.props.displayType}")</Text>
                )
                break;
        }

        return view
    }
}

const style = StyleSheet.create({
    safeview: {
        height: '84%'
    }
});

export default Event;