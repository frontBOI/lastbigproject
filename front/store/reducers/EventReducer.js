
const initialState = {
    events: [],
    comments: [],
    locatedEvents: [],
    discoverEvents: [],
    showSuccessMessage: false,
    successMessage: '',
    canFetchNextEventSet: true,
    hasCommentBeenPosted: false
}

// REDUCER
const eventReducer = (state = initialState, action) => {
    switch (action.type) 
    {
        case 'LOAD_EVENTS':
          console.log('[ EventReducer ] Loaded new event set')
          return { 
            ...state, 
            events: action.events, 
            locatedEvents: action.locatedEvents,
            canFetchNextEventSet: true
          };

        case 'CREATE_EVENT':
          console.log('[ EventReducer ] Event has been created')
          let newEvents = state.events;
          newEvents.push(action.event)

          // sorting selon la date de début, de la plus récente à la plus ancienne
          newEvents = newEvents.sort((a, b) => {
            return Date.parse(a.date) < Date.parse(b.date)
          })  

          return {
            ...state,
            events: newEvents,
            showSuccessMessage: true,
            successMessage: `Création de l'évènement réussie`
          }

        case 'MODIFY_EVENT':
          console.log('[ EventReducer ] Event has been modified')
          let newEvents2 = []
          for (let e of state.events) {
            newEvents2.push(e.id == action.event.id ? action.event : e)
          }
          
          return {
            ...state,
            events: newEvents2,
            showSuccessMessage: true,
            successMessage: `Modification de l'évènement réussie`
          }

        case 'REINIT_SUCCESS_MESSAGE':
          return {
            ...state,
            showSuccessMessage: false,
            successMessage: ''
          }

        case 'LOAD_COMMENTS':
          console.log('[ EventReducer ] Comments of current event have been loaded')
          const __comments__ = action.comments.map(com => {
            return {
              eventId: com.eventId,
              author: com.userName,
              text: com.content,
              date: `${com.localTime.split(':')[0]}:${com.localTime.split(':')[1]}`
            }
          })

          return {
            ...state,
            comments: __comments__
          }

        case 'POST_COMMENT':
          console.log('[ EventReducer ] Comment has been posted')

          const __comments__2 = state.comments
          __comments__2.push({
            eventId: action.eventId,
            author: action.userName,
            date: `${action.localTime.split(':')[0]}:${action.localTime.split(':')[1]}`,
            text: action.comment
          })

          return {
            ...state,
            comments: __comments__2,
            hasCommentBeenPosted: true
          }

        case 'SET_CAN_FETCH_NEXT_EVENT_SET':
          return {
            ...state,
            canFetchNextEventSet: action.value
          }

        case 'SET_HAS_COMMENT_BEEN_POSTED':
          return {
            ...state,
            hasCommentBeenPosted: action.value
          }

        case 'LOAD_DISCOVER_EVENTS':
          return {
            ...state,
            discoverEvents: action.events
          }

        default:
          return state;
    }
}

export default eventReducer;