
const initialState = {
    user: {
        id: '',
        name: '',
        firstName: '',
        pseudo: '',
        password: '',
        mail: '',
        favoriteCity: '',
        birthdate: '',
        sex: '',
        hobbies: [],
        avatar: '',
        events: {
            liked: [],
            participating: []
        }
    },
    isLogged: false,
    isLoggedOutSuccessful: false,
    hasSuccessfullyBeenCreated: false,
    mapCenter: undefined,
    userLocation: undefined,
    activateUserLocation: true,
    loginErrorMessage: '',
    paramaterModificationSuccessful: false,
    originalLocation: {
        latitude: '45.783852',
        longitude: '4.869060'
    }
}

const blankUser = {
    id: 0,
    name: '',
    firstName: '',
    pseudo: '',
    password: '',
    mail: '',
    favoriteCity: '',
    birthdate: '',
    sex: '',
    hobbies: [],
    avatar: undefined,
    events: {
        liked: [],
        participating: []
    }
}


// REDUCER
const userReducer = (state = initialState, action) => 
{
    const backUserToFront = backUser => {
        return {
            id: backUser.id,
            password: backUser.pwd,
            pseudo: backUser.pseudonym,
            name: backUser.lastName,
            firstName: backUser.firstName,
            mail: backUser.email,
            address: backUser.address,
            age: backUser.age,
            hobbies: backUser.tags,
            avatar: backUser.avatar,
            events: {
                liked: backUser.events.liked,
                participating: backUser.events.participating,
            }
        }
    }

    switch (action.type) 
    {
        case 'SET_LOGIN_ERROR_MESSAGE':
            return {
                ...state,
                loginErrorMessage: action.errorMessage
            }

        case 'SET_PARAMETER_MODIFICATION_SUCCESSFUL':
            return {
                ...state,
                paramaterModificationSuccessful: action.isSuccessful
            }

        case 'SET_MAP_CENTER':
            return {
                ...state,
                mapCenter: {
                    latitude: action.coordinates.latitude,
                    longitude: action.coordinates.longitude,

                    // how far out the map is zoomed
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                },
                originalLocation: {
                    latitude: action.coordinates.latitude,
                    longitude: action.coordinates.longitude,
                }
            }

        case 'SET_USER_LOCATION':
            return {
                ...state,
                userLocation: {
                    latitude: action.userLocation.latitude,
                    longitude: action.userLocation.longitude
                }
            }

        case 'LOG_USER':
            console.log('[ UserReducer ] User has been logged')
            const frontUser = backUserToFront(action.user)
            return {
                ...state,
                user: { ...frontUser },
                isLogged: true,
                isLoggedOutSuccessful: false
            }

        case 'DELETE_USER':
            return {
                ...state,
                user: blankUser,
                isLogged: false,
                isLoggedOutSuccessful: true,
                hasSuccessfullyBeenCreated: false
            }

        case 'LOG_OUT':
            console.log('[ UserReducer ] User logged out')
            return {
                ...state,
                user: { ...blankUser },
                isLogged: false,
                mapCenter: undefined,
                userLocation: undefined,
                isLoggedOutSuccessful: true
            }

        case 'CREATE_USER':
            console.log('[ UserReducer ] User has been created')
            const createdUser = backUserToFront(action.user)
            return {
                ...state,
                user: { ...createdUser },
                hasSuccessfullyBeenCreated: true
            }

        case 'UPDATE_USER':
            console.log('[ UserReducer ] User has been updated')
            return {
                ...state,
                user: { ...action.user }
            }

        case 'LIKE_EVENT':
            console.log(`[ UserReducer ] User has ${action.isLiking ? 'liked' : 'unliked'} an event`)
            const newUser = state.user
            if (action.isLiking && !newUser.events.liked.includes(action.eventId)) {
                newUser.events.liked.push(action.eventId)
            } else if (!action.isLiking && newUser.events.liked.includes(action.eventId)) {
                const index = newUser.events.liked.indexOf(action.eventId);
                if (index !== -1) {
                    newUser.events.liked.splice(index, 1);
                }
            }

            return {
                ...state,
                user: { ...newUser }
            }

        case 'PARTICIPATE_EVENT':
            console.log(`[ UserReducer ] User is ${action.isParticipating ? 'participating' : 'DEparticipating'} to an event`)
            const newUser2 = state.user
            if (action.isParticipating && !newUser2.events.participating.includes(action.eventId)) {
                newUser2.events.participating.push(action.eventId)
            } else if (!action.isParticipating && newUser2.events.participating.includes(action.eventId)) {
                const index = newUser2.events.participating.indexOf(action.eventId);
                if (index !== -1) {
                    newUser2.events.participating.splice(index, 1);
                }
            }

            return {
                ...state,
                user: { ...newUser2 }
            }

        default: 
            return state;
    }
}

export default userReducer;