
const initialState = {
    token
}


// REDUCER
const sessionReducer = (state = initialState, action) => {
    switch (action.type) 
    {
        case 'SET_SESSION_TOKEN':
            console.log('[ SessionReducer ] Session token has been set (' + action.token + ')')
            return {
                ...state,
                token: action.token
            }

        default: 
            console.log('[ SessionReducer ] Action inconnue !')
            return state;
    }
}

export default sessionReducer;