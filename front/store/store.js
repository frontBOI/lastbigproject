import { createStore } from 'redux';
import eventReducer from "./reducers/EventReducer";
import userReducer from "./reducers/UserReducer";
import sessionReducer from "./reducers/UserReducer";
import thunk from 'redux-thunk';
import { combineReducers, applyMiddleware  } from "redux";

export default store = createStore(
    combineReducers({
        eventReducer,
        userReducer,
        sessionReducer
    }),
    window.REDUX_DEVTOOLS_EXTENSION && window.REDUX_DEVTOOLS_EXTENSION(),
    applyMiddleware(thunk)
)