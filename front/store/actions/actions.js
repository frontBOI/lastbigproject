import {
    setSessionToken,
    createEvent_action,
    modifyEvent_action,
    loadEvents_action,
    updateUser_action,
    logUser_action,
    logOut_action,
    createUser_action,
    likeEvent_action,
    participateEvent_action,
    loadComment_action,
    postComment_action,
    setLoginErrorMessage,
    setParameterModificationSuccessful,
    setCanFetchNextEventSet,
    loadDiscoverEvents_action
} from './actions_pure'

import {
    customFetch,
    frontToBackUser,
    frontToBackEvent
} from './helper'

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
const isLocalDev = false
const serverIP = isLocalDev ? 
    '172.30.112.1' // Ici il faut mettre l'adresse IP de ta machine locale puisqu'on est pas encore sur AWS brah
    : 
    '35.208.236.230' // Adresse GCP
// -----------------------------------------------------------------------
// -----------------------------------------------------------------------

let __token__ = ''

export function setMapCenter (coordinates) {
    return {
        type: 'SET_MAP_CENTER',
        coordinates
    }
}

export function setUserLocation (userLocation) {
    return {
        type: 'SET_USER_LOCATION',
        userLocation
    }
}

export function reinitSuccessMessage () {
    return {
        type: 'REINIT_SUCCESS_MESSAGE'
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const createNewEvent = (event) => {
    return dispatch => {
        console.log('[ Fetch ] Creating event')

        const headers = {
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
        }

        const backEvent = frontToBackEvent(event)
        const body = JSON.stringify(backEvent)

        customFetch(
            `http://${serverIP}/api/event/create`, // url
            'Create event',                       // Describe what you are doing ?
            'POST',                               // method
            headers,                              // headers
            body,                                 // body

            // executed when fetch( ) is successful
            response => {
                dispatch(createEvent_action(event.event));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const modifyEvent = (event) => {
    return dispatch => 
    {
        console.log('[ Fetch ] Updating event')

        const headers = {
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
        }

        const backEvent = frontToBackEvent(event)
        const body = JSON.stringify(backEvent)

        customFetch(
            `http://${serverIP}/api/event/update/${event.id}`, // url
            'Modify event',                                   // Describe what you are doing ?
            'PUT',                                            // method
            headers,                                          // headers
            body,                                             // body

            // executed when fetch( ) is successful
            response => {
                dispatch(modifyEvent_action(event));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const loadEvents = (latitude, longitude, userId) => 
{
    return dispatch => 
    {
        const headers = {
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
        }
        const body = `[${latitude},${longitude}]`

        dispatch(setCanFetchNextEventSet(false))

        customFetch(
            `http://${serverIP}/api/event/upcoming_events`, // url
            'Load events',                                 // Describe what you are doing ?
            'POST',                                        // method
            headers,
            body,

            // executed when fetch( ) is successful
            rawEvents => 
            {
                console.log('[ Fetch ] Loaded ' + rawEvents.length + ' events (+ ceux de ' + userId + ')')

                // [HOTFIX] on ne garde que ceux du user courant
                const userEvents = rawEvents.filter(e => e.organisateurLocalId == userId)
            
                // sorting selon la date de début, de la plus récente à la plus ancienne
                rawEvents = rawEvents.sort((a, b) => {
                    return Date.parse(a.localDate) < Date.parse(b.localDate)
                })

                // parsing de l'ensemble des events
                let events = [];
                let locatedEvents = [];
                for (const event of rawEvents) {
                    var latTemp = (event.venueLat == null || event.venueLat == "0.0") ? "" : parseFloat(event.venueLat);
                    var lonTemp = (event.venueLon == null || event.venueLon == "0.0") ? "" : parseFloat(event.venueLon)

                    const parsedEvent = {
                        id: event.id,
                        name: event.name,
                        date: event.localDate,
                        hour: event.localTime,
                        description: event.description || 'Aucune description',
                        latitude: latTemp,
                        longitude: lonTemp,
                        link: event.eventLink,
                        address: event.venueAddressString ? event.venueAddressString.replace('\n', '') : undefined,
                        ticketing: event.ticketing,
                        coverPicture: event.avatar || 'https://picsum.photos/500/300',
                        comments: []
                    }

                    events.push(parsedEvent);
                    if (latTemp !== "" && lonTemp !== "") {
                        locatedEvents.push(parsedEvent)
                    }
                }

                // [HOTFIX] on ne garde que les 50 premiers évènements
                events = events.slice(0, 50)
                locatedEvents = locatedEvents.slice(0, 50)
                for (let e of userEvents) {
                    events.push(e)

                    var latTemp = (e.venueLat == null || e.venueLat == null || e.venueLat == "0.0") ? "" : parseFloat(e.venueLat);
                    var lonTemp = (e.venueLon == null || e.venueLon == undefined || e.venueLon == "0.0") ? "" : parseFloat(e.venueLon)
                    if (latTemp !== "" && latTemp.length > 0 && lonTemp !== "" && lonTemp.length > 0) {
                        locatedEvents.push(e)
                    }
                }

                dispatch(loadEvents_action(events, locatedEvents));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const updateUser = (user) => {
    return dispatch => 
    {
        const headers = {
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
        }
        const body = JSON.stringify(frontToBackUser(user))

        customFetch(
            `http://${serverIP}/api/user/update/${user.id}`, // url
            'Update user',                                  // Describe what you are doing ?
            'PUT',                                          // method
            headers,
            body,

            // executed when fetch( ) is successful
            response => {
                dispatch(setParameterModificationSuccessful(true))
                dispatch(updateUser_action(user))

                console.log(`> Updating discover`) 
                store.dispatch(loadDiscoverEvents(user.id))
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const logUser = (login, password) => {
    return dispatch => 
    {
        console.log('[ Fetch ] Logging user')

        customFetch(
            `http://${serverIP}/api/auth/login?login=${login}&pwd=${password}`, // url
            'Log user',                                     // Describe what you are doing ?
            'POST',                                         // method
            {},                                             // headers
            {},                                             // body

            // executed when fetch( ) is successful
            response => {
                const token = response.token
                const user = response.user

                user.events = {
                    liked: [],
                    participating: []
                }

                // récupération des évènement likés par le user
                user.events.liked = []

                // récupération des évènements auxquels le user participe
                user.events.participating = []

                __token__ = token
                dispatch(logUser_action(user));
                dispatch(setLoginErrorMessage(''))
                dispatch(setSessionToken(token))
            },

            // executed when fetch( ) has failed
            () => {
                dispatch(setLoginErrorMessage('Indentifiants incorrects !'))
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const logOut = (userId) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] Logging out`)

        customFetch(
            `http://${serverIP}/api/auth/logout/${userId}`, // url
            'Logout user',                                  // Describe what you are doing ?
            'POST',                                         // method
            {'Cookie': `token=${__token__}`},               // headers
            {},                                             // body

            // executed when fetch( ) is successful
            response => {
                dispatch(logOut_action(userId));
                __token__ = '' // reset
            },

            // executed when fetch( ) has failed
            () => {
                dispatch(logOut_action(userId));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const deleteUser = (userId) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] Deleting user`)

        customFetch(
            `http://${serverIP}/api/user/delete/${userId}`, // url
            'Delete user',                                  // Describe what you are doing ?
            'DELETE',                                          // method
            {'Cookie': `token=${__token__}`},               // headers
            {},                                             // body

            // executed when fetch( ) is successful
            response => {
                dispatch(deleteUser(userId));
                __token__ = '' // reset
            },

            // executed when fetch( ) has failed
            () => {
                dispatch(logOut_action(userId));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const createUser = (user) => {
    return dispatch => 
    {
        console.log('[ Fetch ] Creating user')

        const headers = { 'Content-Type': 'application/json' }
        const body = JSON.stringify(frontToBackUser(user))
        customFetch(
            `http://${serverIP}/api/user/create`, // url
            'Create user',                          // Describe what you are doing ?
            'POST',                                 // method
            headers,                                // headers
            body,

            // executed when fetch( ) is successful
            response => {
                const createdUser = response
                createdUser.avatar = '' // no avatar by default
                createdUser.events = {
                    liked: [],
                    participating: []
                }

                dispatch(createUser_action(response));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const likeEvent = (eventId, userId, isLiking) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] ${isLiking ? 'Liking':'Unliking'} event`)

        const headers = { 
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
         }
        const body = {}
        customFetch(
            `http://${serverIP}/api/user-event/${eventId}/${userId}/interested`, // url
            'Like event',                           // Describe what you are doing ?
            'POST',                                 // method
            headers,                                // headers
            body,

            // executed when fetch( ) is successful
            response => {
                dispatch(likeEvent_action(eventId, isLiking));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const participateEvent = (eventId, userId, isParticipating) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] ${isParticipating ? 'Participating':'DEparticipating'} to an event`)

        const headers = { 
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
         }
        const body = {}
        customFetch(
            `http://${serverIP}/api/user-event/${eventId}/${userId}/participate`, // url
            'Participate event',                    // Describe what you are doing ?
            'POST',                                 // method
            headers,                                // headers
            body,

            // executed when fetch( ) is successful
            response => {
                dispatch(participateEvent_action(eventId, isParticipating));
                dispatch(loadDiscoverEvents(userId))
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const loadComments = (eventId) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] Loading comments`)
        
        const headers = { 
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
        }
        const body = { }
        customFetch(
            `http://${serverIP}/api/event-comment/all/${eventId}`, // url
            'Loading comments',                         // Describe what you are doing ?
            'GET',                                      // method
            headers,                                    // headers
            body,

            // executed when fetch( ) is successful
            response => {
                if (!response.message) {
                    dispatch(loadComment_action(eventId, response));
                }
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const postComment = (eventId, userName, userId, comment) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] Posting comment`)

        // extraction de la date
        const today = new Date()
        let year = today.getUTCFullYear()
        let _day_ = today.getUTCDate()
        let day = _day_ < 10 ? '0' + _day_ : _day_
        let _month_ = today.getUTCMonth() + 1
        let month = _month_ < 10 ? '0' + _month_ : _month_
        const localDateFormatted = `${year}-${month}-${day}`

        // extraction de l'heure
        let _hours_ = today.getHours()
        let hours = _hours_ < 10 ? '0' + _hours_ : _hours_
        let _minutes_ = today.getMinutes()
        let minutes = _minutes_ < 10 ? '0' + _minutes_ : _minutes_
        let _seconds_ = today.getSeconds()
        let seconds = _seconds_ < 10 ? '0' + _seconds_ : _seconds_
        const localTimeFormatted = `${hours}:${minutes}:${seconds}`

        // et on construit ce qui sera envoyé dans le body
        const formatDate = `${localDateFormatted}T${localTimeFormatted}.000+00:00`
        
        const headers = { 
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
         }
        const body = JSON.stringify({
            content: comment,
            eventId,
            userId,
            userName,
            localDate: formatDate,
            localTime: localTimeFormatted
        })

        customFetch(
            `http://${serverIP}/api/event-comment/add`, // url
            'Posting comment',                         // Describe what you are doing ?
            'POST',                                    // method
            headers,                                   // headers
            body,

            // executed when fetch( ) is successful
            response => {
                dispatch(postComment_action(eventId, userName, localTimeFormatted, comment));
            }
        )
    }
}


// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
export const loadDiscoverEvents = (userId) => {
    return dispatch => 
    {
        console.log(`[ Fetch ] Loading discover events`)
        
        const headers = { 
            'Content-Type': 'application/json',
            'Cookie': `token=${__token__}`
         }
        const body = { }
        customFetch(
            `http://${serverIP}/api/user-suggest/getSuggestEvent/${userId}`, // url
            'Loading discover events',                  // Describe what you are doing ?
            'GET',                                      // method
            headers,                                    // headers
            body,

            // executed when fetch( ) is successful
            rawEvents => {
                console.log('[ Fetch ] ' + rawEvents.length + ' discover events')
            
                // sorting selon la date de début, de la plus récente à la plus ancienne
                rawEvents = rawEvents.sort((a, b) => {
                    return Date.parse(a.localDate) < Date.parse(b.localDate)
                })

                // parsing de l'ensemble des events
                const events = [];
                for (const event of rawEvents) {
                    var latTemp = (event.venueLat == null || event.venueLat == "0.0") ? "" : parseFloat(event.venueLat);
                    var lonTemp = (event.venueLon == null || event.venueLon == "0.0") ? "" : parseFloat(event.venueLon)

                    const parsedEvent = {
                        id: event.id,
                        name: event.name,
                        date: event.localDate,
                        hour: event.localTime,
                        description: event.description,
                        latitude: latTemp,
                        longitude: lonTemp,
                        link: event.eventLink,
                        address: event.venueAddressString ? event.venueAddressString.replace('\n', '') : undefined,
                        ticketing: event.ticketing,
                        coverPicture: event.avatar || 'https://picsum.photos/500/300',
                        comments: []
                    }

                    events.push(parsedEvent);
                }

                dispatch(loadDiscoverEvents_action(events));
            }
        )
    }
}