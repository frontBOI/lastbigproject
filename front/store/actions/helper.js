// ----------------------------------------------------------------------------
// @brief
//  Cette fonction prend en charge le lancement d'une requête vers une url donnée
// grâce au fetch(). Si le fetch réussit, alors la fonction 'callback' est exécutée
export function customFetch (
    url,                        // URL to fetch 
    description,                // for log purposes: describe shortly what you are trying to do ?
    method,                     // POST, GET, PUT...
    headers,                    // HTTP headers
    body,                       // body in case of a POST/PUT request
    callback,                   // executed when fetch( ) is successful
    errorCallback = () => { }   // executed when fetch( ) has failed
) {
    const args = method === 'GET' || method === 'HEAD' ? 
        { method, headers }
        :
        { method, headers, body }

    fetch(url, args)
        .then(r => handleWebServiceResponse(r))
        .then(response => 
        {
            throwErrorIfNotUsable(response)
            callback(response)
        }).catch(error => {
            console.log(`[ ERROR ] ${description}: ${error}`)
            errorCallback()
        })
}


// ----------------------------------------------------------------------------
// @brief
//  Conversion enter le modèle de données front d'un utilisateur et son pendant en back
export function frontToBackUser (frontUser) {
    return {
        id: frontUser.id,
        login: frontUser.pseudo,
        pseudonym: frontUser.pseudo,
        pwd: frontUser.password,
        lastName: frontUser.name,
        firstName: frontUser.firstName,
        email: frontUser.mail,
        address: frontUser.address,
        age: frontUser.age,
        tags: frontUser.hobbies,
        avatar: frontUser.avatar
    }
}


// ----------------------------------------------------------------------------
// @brief
//  Conversion enter le modèle de données front d'un event et son pendant en back
export function frontToBackEvent (event) 
{
    // extraction de la date
    let year = event.date.getUTCFullYear()
    let _day_ = event.date.getUTCDate()
    let day = _day_ < 10 ? '0' + _day_ : _day_
    let _month_ = event.date.getUTCMonth() + 1
    let month = _month_ < 10 ? '0' + _month_ : _month_
    const localDateFormatted = `${year}-${month}-${day}`

    // extraction de l'heure
    let _hours_ = event.hour.getHours()
    let hours = _hours_ < 10 ? '0' + _hours_ : _hours_
    let _minutes_ = event.hour.getMinutes()
    let minutes = _minutes_ < 10 ? '0' + _minutes_ : _minutes_
    let _seconds_ = event.hour.getSeconds()
    let seconds = _seconds_ < 10 ? '0' + _seconds_ : _seconds_
    const localTimeFormatted = `${hours}:${minutes}:${seconds}`

    // et on construit ce qui sera envoyé dans le body
    const formatDate = `${localDateFormatted}T${localTimeFormatted}.000+00:00`
    
    return {
        localDate: formatDate,
        name: event.name,
        duration: event.duration,
        localTime: localTimeFormatted,
        eventLink: event.link,
        ticketing: event.ticketing,
        description: event.description,
        venueName: event.address,
        avatar: event.coverPicture,
        venueAddressString: event.address,
        venueCity: event.city,
        organisateurLocalId: event.organisateurLocalId
    }
}


// ----------------------------------------------------------------------------
// @brief
//  Cette fonction reçoit en argument 'r' le premier résultat d'un fetch(), et 
// s'occupe de renvoyer la version expoitable en retour
// Si la réponse indique que le requête a échoué, ou bien que les données ne sont
// pas exploitables, alors cette fonction renvoie 'null'
function handleWebServiceResponse (r) 
{
    if (!r.ok) {
        return { 
            status: r.status,
            error: `La requête a renvoyé une erreur ${r.status}` 
        }
    }

    const contentType = r.headers.get('content-type')
    if (contentType.match(/application\/json/)) {
        return r.text().then(text => {
            return text ? JSON.parse(text) : {}
        })
    } else if (contentType.match(/text\/plain/)) {
        return r.text()
    } else {
        throw { 
            status: r.status,
            error: `La requête a renvoyé un content-type non géré (${contentType})` 
        }
    }
}


// ----------------------------------------------------------------------------
// @brief
//  Cette fonction reçoit en entrée le résultat d'une requête fetch() et lance
// une exception si un problème est détecté
function throwErrorIfNotUsable (response) {
    if (
        response == null ||
        typeof response === 'undefined' || 
        (response.status && 
        response.status !== 200 && 
        response.status !== 201)
    ) {
        if (response && response.error)
            throw response.error 
        else
            throw 'erreur inconnue'
    }
}

export function validateInteger (age) { return age.match(/^\d+$/g) }