
export function setSessionToken (token) {
    return {
        type: 'SET_SESSION_TOKEN',
        token
    }
}

export function setLoginErrorMessage (errorMessage) {
    return {
        type: 'SET_LOGIN_ERROR_MESSAGE',
        errorMessage
    }
}

export function setParameterModificationSuccessful (isSuccessful) {
    return {
        type: 'SET_PARAMETER_MODIFICATION_SUCCESSFUL',
        isSuccessful
    }
}

export function createEvent_action (event) {
    return {
        type: 'CREATE_EVENT',
        event
    }
}

export function modifyEvent_action (event) {
    return {
        type: 'MODIFY_EVENT',
        event
    }
}

export function loadEvents_action (events, locatedEvents) {
    return {
        type: 'LOAD_EVENTS',
        events,
        locatedEvents
    }
}

export function updateUser_action (user) {
    return {
        type: 'UPDATE_USER',
        user
    }
}

export function logUser_action (user) {
    return {
        type: 'LOG_USER',
        user
    }
}

export function logOut_action (userId) {
    return {
        type: 'LOG_OUT',
        userId
    }
}

export function deleteUser () {
    return {
        type: 'DELETE_USER'
    }
}

export function createUser_action (user) {
    return {
        type: 'CREATE_USER',
        user
    }
}

export function likeEvent_action (eventId, isLiking) {
    return {
        type: 'LIKE_EVENT',
        eventId,
        isLiking
    }
}

export function participateEvent_action (eventId, isParticipating) {
    return {
        type: 'PARTICIPATE_EVENT',
        eventId,
        isParticipating
    }
}

export function loadComment_action (eventId, comments) {
    return {
        type: 'LOAD_COMMENTS',
        eventId,
        comments
    }
}


export function loadDiscoverEvents_action (events) {
    return {
        type: 'LOAD_DISCOVER_EVENTS',
        events
    }
}


export function postComment_action (eventId, userName, localTime, comment) {
    return {
        type: 'POST_COMMENT',
        eventId,
        userName,
        localTime,
        comment
    }
}

export function setCanFetchNextEventSet (value) {
    return {
        type: 'SET_CAN_FETCH_NEXT_EVENT_SET',
        value
    }
}

export function setHasCommentBeenPosted_action (value) {
    return {
        type: 'SET_HAS_COMMENT_BEEN_POSTED',
        value
    }
}