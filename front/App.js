import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { MenuProvider } from 'react-native-popup-menu';

// views
import Login from './view/Login';
import HomeScreen from './view/Home';
import DiscoverScreen from './view/Discover';
import Compte from './model/Compte';
import EventDetails from './view/EventDetails';
import ParameterChanger from './view/ParameterChanger';
import CreateEvent from './view/CreateEvent';
import ModifyEvent from './view/ModifyEvent';
import SignIn from './view/SignIn';
import BottomNavigation from './components/BottomNavigation';

// store related
import store from './store/store';
import { Provider } from 'react-redux';
import { setMapCenter } from './store/actions/actions';

const Stack = createStackNavigator()

function geolocalize() 
{
  console.log('> Geolocalizing...')
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      position => {
        resolve({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        })
      },
      error => console.log(error.code, error.message),
      { 
        enableHighAccuracy: true, 
        timeout: 15000, 
        maximumAge: 10000 
      }
    );
  })
}

export default function App() 
{
  geolocalize().then(location => 
  {
    console.log('> You sir just got geolocalised: ' + location.latitude + ' / ' + location.longitude)
    store.dispatch(setMapCenter(location))
  })

  return (
    <Provider store={store}>
      <MenuProvider>
        <SafeAreaProvider>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
              <Stack.Screen name="Login"            component={Login} options={{ headerShown: false }} />
              <Stack.Screen name="Accueil"          component={HomeScreen} options={{ headerShown: false }} />
              <Stack.Screen name="Decouvrir"        component={DiscoverScreen} />
              <Stack.Screen name="EventDetails"     component={EventDetails} options={({ route }) => ({ title: route.params.name })}/>
              <Stack.Screen name="BottomTabs"       component={BottomNavigation} options={{ headerShown: false }} />
              <Stack.Screen name="ParameterChanger" component={ParameterChanger} options={({ route }) => ({ title: route.params.name })}/>
              <Stack.Screen name="CreateEvent"      component={CreateEvent} options={({ route }) => ({ title: route.params.name })}/>
              <Stack.Screen name="ModifyEvent"      component={ModifyEvent} options={({ route }) => ({ title: route.params.name })}/>
              <Stack.Screen name="SignIn"           component={SignIn} options={({ route }) => ({ title: 'Créer mon compte' })}/>
              <Stack.Screen name="Compte">
                {props => <Compte displayType='DETAILED'/>}
              </Stack.Screen>
            </Stack.Navigator>
          </NavigationContainer>
        </SafeAreaProvider>
      </MenuProvider>
    </Provider>
  );
}