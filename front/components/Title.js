import React from 'react';
import { StyleSheet, Text } from 'react-native';

function Title (props) {
    return <Text 
                style={[
                    style.header, 
                    {
                        marginTop: props.spacing || 0, 
                        marginLeft: props.spacing || 0
                    }]}
            >
                {props.value}
            </Text>
}

const style = StyleSheet.create({
    header: {
        marginBottom: 20,
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        padding: 10,
        backgroundColor: '#2c3e50',

        // cette propriété émule un "width: auto"
        alignSelf:"flex-start"
    }
});

export default Title;