import React, { Component } from 'react'
import { StyleSheet, TextInput, View, Text } from 'react-native'
import SoundPlayer from '../../components/SoundPlayer'

export default class Password extends Component
{
    constructor (props) {
        super(props)

        this.state = {
            previousPassword: '',
            newPassword: '',
            newBisPassword: '',
            user: props.user,
            newUser: props.user
        }

        const sonMotDePasse = require(`../../assets/sounds/motDePasse.mp3`)
        SoundPlayer(sonMotDePasse)
    }

    componentDidMount () {
        const { childRef } = this.props;
        childRef(this);
    }

    componentWillUnmount() {
        const { childRef } = this.props;
        childRef(undefined);
    }

    isFormValid () {
        const isPreviouPasswordOk = this.state.previousPassword === this.state.user.password;
        const areNewPasswordsIdentical = 
            this.state.newPassword.length > 0 && 
            this.state.newBisPassword.length > 0 && 
            this.state.newPassword == this.state.newBisPassword;

        if (isPreviouPasswordOk && areNewPasswordsIdentical) {
            return true
        }

        return false
    }

    // la fonction validate est appelée par le parent ParameterChanger afin de valider
    // le résultat de la modif et de la dispatch ensuite
    // @returns
    /*
        {
            success: true | false,
            data: { whatever data to be dispatched when the parent calls this validate function }
        }
    */
    validate () {
        if (this.isFormValid()) {
            return {
                success: true,
                data: {
                    ...this.state.user,
                    password: this.state.newPassword
                }
            }   
        } else {
            return {
                success: false,
                errorMessage: 'Revérifie les champs c\'est pas bon'
            }
        }
    }

    render () {
        return (
            <View>
                <View style={style.labelInputWrapper}>
                    <Text style={style.label}>Mon ancien mot de passe</Text>
                    <TextInput
                        style={style.input}
                        onChangeText={text => this.setState({ previousPassword: text})}
                        value={this.state.previousPassword}
                        secureTextEntry={true}
                    />
                </View>

                <View style={style.labelInputWrapper}>
                    <Text style={style.label}>Mon nouveau mot de passe</Text>
                    <TextInput
                        style={style.input}
                        onChangeText={text => this.setState({ newPassword: text})}
                        value={this.state.newPassword}
                        secureTextEntry={true}
                    />
                </View>

                <View style={style.labelInputWrapper}>
                    <Text style={style.label}>Confirmer mon nouveau mot de passe</Text>
                    <TextInput
                        style={style.input}
                        onChangeText={text => this.setState({ newBisPassword: text})}
                        value={this.state.newBisPassword}
                        secureTextEntry={true}
                    />
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    labelInputWrapper: {
        padding: 10
    },
    label: {
        fontSize: 15,
        fontStyle: 'italic'
    },
    input: { 
        height: 25, 
        borderColor: 'gray', 
        borderWidth: 1, 
        width: '75%',
        marginTop: 5,
        backgroundColor: 'white',
        padding: 5
    }
});