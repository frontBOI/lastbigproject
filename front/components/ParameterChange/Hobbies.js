import React, { Component } from 'react'
import { StyleSheet, TextInput, View, Text } from 'react-native'

// store related
import { loadDiscoverEvents } from '../../store/actions/actions';

export default class Hobbies extends Component
{
    constructor (props) {
        super(props)

        // 0) préparation
        let fancyInterests = '';
        for (let i of this.props.user.hobbies) {
            fancyInterests += i + ', ';
        }
        fancyInterests = fancyInterests.substring(0, fancyInterests.length - 2); // remove last ', '

        this.state = {
            user: props.user,
            newHobbies: props.user.hobbies.join(',')
        }
    }

    componentDidMount () {
        const { childRef } = this.props;
        childRef(this);
    }

    componentWillUnmount() {
        const { childRef } = this.props;
        childRef(undefined);
    }

    // la fonction validate est appelée par le parent ParameterChanger afin de valider
    // le résultat de la modif et de la dispatch ensuite
    // @returns
    /*
        {
            success: true | false,
            data: { whatever data to be dispatched when the parent calls this validate function }
        }
    */
    validate () {
        return {
            success: true,
            data: {
                ...this.state.user,
                hobbies: this.state.newHobbies.split(',')
            }
        }
    }

    render () {
        return (     
            <View>
                <View style={style.labelInputWrapper}>
                    <Text style={style.label}>Modifier mes centres d'intérêt (veillez à les séparer par une virgule)</Text>
                    <TextInput
                        style={[style.input, {width: '100%', height: 40}]}
                        onChangeText={text => this.setState({ newHobbies: text })}
                        multiline={true}
                        value={this.state.newHobbies}
                    />
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    labelInputWrapper: {
        padding: 10
    },
    label: {
        fontSize: 15,
        fontStyle: 'italic'
    },
    input: { 
        height: 25, 
        borderColor: 'gray', 
        borderWidth: 1, 
        width: '75%',
        marginTop: 5,
        backgroundColor: 'white',
        padding: 5
    }
});