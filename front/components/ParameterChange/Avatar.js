import React, { Component } from 'react'
import { StyleSheet, View, Image } from 'react-native'
import ImageUpload from '../Upload'
import SoundPlayer from '../../components/SoundPlayer'

export default class Avatar extends Component
{
    constructor (props) {
        super(props)

        this.state = {
            hasLoadedOnce: false,
            user: this.props.user
        }

        const sonModifierAvatar = require(`../../assets/sounds/avatarModify.mp3`)
        SoundPlayer(sonModifierAvatar)
    }

    componentDidMount () {
        const { childRef } = this.props;
        childRef(this);
    }

    componentWillUnmount() {
        const { childRef } = this.props;
        childRef(undefined);
    }

    handleNewAvatarUpload (newAvatarBase64) {
        this.setState({ hasLoadedOnce: true })
        const newAvatar = 'data:image/png;base64,' + newAvatarBase64
        this.setState({ user: {
            ...this.state.user,
            avatar: newAvatar
        } })
    }

    // la fonction validate est appelée par le parent ParameterChanger afin de valider
    // le résultat de la modif et de la dispatch ensuite
    // @returns
    /*
        {
            success: true | false,
            data: { whatever data to be dispatched when the parent calls this validate function }
        }
    */
    validate () {
        return ! this.state.hasLoadedOnce ?
            { 
                success: false,
                errorMessage: 'Aucun avatar chargé'
            }
            :
            {
                success: true,
                data: this.state.user
            }
    }

    render () {
        return (     
            <View>
                <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
                    <Image 
                        style={style.avatar}
                        source={
                            this.state.user.avatar && this.state.user.avatar.length > 0 ?
                                { uri: this.state.user.avatar }
                                :
                                require('../../assets/user.png')
                        }
                    />
                </View>

                <ImageUpload 
                    uploadCallback={newAvatarBase64 => this.handleNewAvatarUpload(newAvatarBase64)}
                    text={"Uploader un nouvel avatar"}
                    display={'PAGE'}
                />
            </View>
        )
    }
}

const style = StyleSheet.create({
    avatar: {
        width: 250,
        height: 250
    }
})