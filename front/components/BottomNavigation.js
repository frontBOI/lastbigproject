import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SoundPlayer from '../components/SoundPlayer'

// views
import HomeScreen from '../view/Home';
import DiscoverScreen from '../view/Discover';
import MyEvents from '../view/MyEvents';
import MyFriends from '../view/MyFriends';

function playTabSound (target) 
{
    let soundName = ''
    let fileRequired
    switch (target) {
        case 'HOME':
            soundName = 'home.mp3'
            fileRequired = require(`../assets/sounds/home.mp3`)
            break

        case 'DISCOVER':
            soundName = 'discover.mp3'
            fileRequired = require(`../assets/sounds/discover.mp3`)
            break

        case 'MY EVENTS':
            soundName = 'myEvents.mp3'
            fileRequired = require(`../assets/sounds/myEvents.mp3`)
            break

        case 'MY FRIENDS':
            soundName = 'myFriends.mp3'
            fileRequired = require(`../assets/sounds/tapadami.mp3`)
            break

        default:
            soundName = 'yeah2.mp3'
            break
    }

    SoundPlayer(fileRequired)
}

export default function BottomNavigation () 
{
    const Tab = createBottomTabNavigator();

    const tabElements = [
        {
            name: 'Home',
            component: HomeScreen,
            label: 'Home',
            iconName: 'home'
        },
        {
            name: 'Discover',
            component: DiscoverScreen,
            label: 'Decouvrir',
            iconName: 'earth'
        },
        {
            name: 'My Events',
            component: MyEvents,
            label: 'Mes évènements',
            iconName: 'ticket'
        },
        {
            name: 'My Friends',
            component: MyFriends,
            label: 'Mes amis',
            iconName: 'account-group'
        },
    ];

    return (
        <Tab.Navigator 
            tabBarOptions={{
                activeTintColor: '#2c3e50',
                showLabel: false,
            }}
        >
            {
                tabElements.map((tab, index) => {
                    return (
                        <Tab.Screen 
                            name={tab.name}
                            component={tab.component} 
                            key={index}
                            listeners={{
                                tabPress: e => {
                                    const target = e.target.split('-')[0].toUpperCase()
                                    playTabSound(target)
                                }
                            }}
                            options={{
                                tabBarLabel: tab.label,
                                tabBarIcon: ({ color, size }) => 
                                (
                                    <MaterialCommunityIcons 
                                        name={tab.iconName} 
                                        color={color} 
                                        size={size} 
                                    />
                                ),
                            }}
                        />
                    )
                })
            }
        </Tab.Navigator>
    )
}