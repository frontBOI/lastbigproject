import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

// image manipulation
import * as ImagePicker from 'expo-image-picker'
import * as ImageManipulator from 'expo-image-manipulator';

class ImageUpload extends Component
{
    constructor (props) {
        super(props);

        this.state = {
            hasLibraryPermission: false
        }

        // request library access permission
        if (Platform.OS !== 'web') {
            ImagePicker.requestMediaLibraryPermissionsAsync().then(status => {
                if (status.granted === true) {
                    this.setState({hasLibraryPermission: true})
                }
            });
        }

        this.openDocumentPicker = this.openDocumentPicker.bind(this);
    }

    async openDocumentPicker () 
    {
        try {
            if (this.state.hasLibraryPermission) {
                let image = await ImagePicker.launchImageLibraryAsync({
                    mediaTypes: ImagePicker.MediaTypeOptions.All,
                    allowsEditing: true,
                    aspect: [4, 3],
                    quality: 1 // 0: compression - 1: maximum quality
                });

                if (!image.cancelled) 
                {
                    // réduction de la taille de l'image
                    const resizeDimensions = 128
                    const resizedImage = await ImageManipulator.manipulateAsync(
                        image.uri,
                        [{ resize: { width: resizeDimensions, height: resizeDimensions} }],
                        { 
                            compress: 1, 
                            format: ImageManipulator.SaveFormat.PNG,
                            base64: true
                        }
                    );
                    
                    // puis communication du base64 au composant parent
                    if (!resizedImage.cancelled) {
                        this.props.uploadCallback(resizedImage.base64)
                    }
                }
            }
        } catch (e) {
            console.log('[ ERROR ] Document Picker: ' + e)
        }
    }

    render () {
        let containerStyle, labelStyle, iconSize
        switch (this.props.display) {
            case 'LINE':
                iconSize = 35
                labelStyle = style.labelLine
                containerStyle = style.containerLine
                break

            case 'PAGE':
            default:
                iconSize = 100
                labelStyle = style.labelPage
                containerStyle = style.containerPage
                break
        }

        return (
            <TouchableHighlight 
                onPress={this.openDocumentPicker}
            >
                <View style={containerStyle}>
                    <MaterialCommunityIcons style={{}} name={'upload-outline'} color={'#2c3e50'} size={iconSize} />
                    <Text style={labelStyle}>{this.props.text}</Text>
                </View>
            </TouchableHighlight>
        )
    }
}

const style = StyleSheet.create({
    containerLine: {
        marginTop: 20,
        flexDirection: 'row'
    },

    containerPage: {
        marginTop: 20,
        flexDirection: 'column',
        alignItems: 'center'
    },
    labelLine: {
        color: '#2c3e50',
        fontSize: 17,
        paddingTop: 4,
        paddingLeft: 6
    },
    labelPage: {
        color: '#2c3e50',
        fontSize: 20
    }
});

export default (ImageUpload);