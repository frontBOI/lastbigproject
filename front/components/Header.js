import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

// fonts
import { useFonts } from 'expo-font'

// views
import Compte from '../model/Compte';

export default function Header () 
{
    let [fontsLoaded] = useFonts({
        'Vogue': require('../assets/fonts/Vogue.ttf')
    })

    let title;
    let appName = 'Around Us'
    if (!fontsLoaded) {
        title = (
            <Text style={style.title}>{ appName }</Text>
        )
    } else {
        title = (
            <Text style={{...style.title, ...style.titleFonted}}>{ appName }</Text>
        )
    }

    return (
        <SafeAreaView>
            <View style={style.wrapper}>
                { title }
                <Compte displayType='HEADER' />
            </View>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: '#2c3e50'
    },
    title: {
        fontSize: 40,
        margin: 10,
        paddingTop: 5,
        color: 'white'
    },
    titleFonted: {
        fontFamily: 'Vogue'
    }
});