import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MapView, { Marker, Callout } from 'react-native-maps';
import { useNavigation } from '@react-navigation/native';

// store related
import store from '../store/store';
import { connect } from 'react-redux';
import { loadEvents } from '../store/actions/actions';

function Map (props) {
    const LyonLocation = {
        latitude: 45.75,
        longitude: 4.85,

        // how far out the map is zoomed
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    }

    const navigation = useNavigation();
    const [region, setRegion] = useState({ latitude: LyonLocation.latitude, longitude: LyonLocation })

    const handleAnnotationPress = (event) => {
        navigation.navigate('EventDetails', { name: event.name, event })
    }

    // explication: le compilateur m'a fait péter une erreur absurde à répétition alors que rien ne semble devoir la trigger
    // en gros, elle se déclenche lors d'un drag du user... Alors après de longues recherches sans succès, je vais dev le fait
    // que erreur = trigger d'un drag user mdr et ça marche niquel comme ça
    const onUserDrag = (newRegion) =>
    {
        try {
            // comparing before-drag region and after-frag region to prevent map to detect ultra small changes
            // such as the ones triggered during map loading (bruh)
            if (
                newRegion.latitude.toFixed(3) !== this.state.region.latitude.toFixed(3) && 
                newRegion.longitude.toFixed(3) !== this.state.region.longitude.toFixed(3)
            ) {
                // on arrive ici quand le drag est successful, normalement
                this.setState({region: { latitude: newRegion.latitude, longitude: newRegion.longitude }})
            }
        } catch (e) 
        {
            // partie exécutée lors d'un drag du user
            // (?) on ne peut lancer de requête que si on a reçu la précédente
            if (props.canFetchNextEventSet) 
            {
                console.log(`> Fetching new set of events (${newRegion.latitude} / ${newRegion.longitude})`)
                store.dispatch(loadEvents(newRegion.latitude, newRegion.longitude, props.user.id))
            }
        }
    }

    return (
        <MapView
            // the initial region is on Lyon if 'activateUserLocation' is false
            // or it is the user's location if it got retrieved
            initialRegion={
                props.activateUserLocation && props.mapCenter ? 
                    props.mapCenter 
                    : 
                    LyonLocation
            }
            showsPointsOfInterest={false}
            showsUserLocation={true}
            followsUserLocation={true}
            style={style.map}
            onRegionChangeComplete={(region, details) => onUserDrag(region, details)}
        >
            {
                props.events.map((event, index) => 
                {
                    return (
                        <Marker
                            key={index}
                            coordinate={{
                                latitude: event.latitude,
                                longitude: event.longitude
                            }}
                            title={event.name}
                            description={event.description}
                        >
                            <Callout 
                                style={style.markerCallout} 
                                tooltip={true} 
                                onPress={() => handleAnnotationPress(event)}
                            >
                                <View>
                                    <Text style={style.calloutTitle}>{event.name}</Text>
                                    {/* <Text style={style.calloutSubinfo}>A 200m</Text>
                                    <Text style={style.calloutSubinfo}>Commence dans 10mn</Text> */}
                                </View>
                            </Callout>
                        </Marker>
                    )
                })
            }
        </MapView>
    )
}

const style = StyleSheet.create({
    map: {
        height: '84%'
    },
    markerCallout: {
        backgroundColor: '#2c3e50',
        padding: 15,
        borderRadius: 150
    },
    calloutTitle: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        maxWidth: '100%',
        marginBottom: 10
    },
    calloutSubinfo: {
        color: 'white'
    }
});

const mapStateToProps = state => {
    return {
        user: state.userReducer.user,
        mapCenter: state.userReducer.mapCenter,
        userLocation: state.userReducer.userLocation,
        activateUserLocation: state.userReducer.activateUserLocation,
        canFetchNextEventSet: state.eventReducer.canFetchNextEventSet
    }
}
export default connect(mapStateToProps)(Map);