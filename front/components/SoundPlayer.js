import { Audio } from 'expo-av'

async function SoundPlayer (fileRequired)
{
    Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
        shouldDuckAndroid: true,
        staysActiveInBackground: true,
        playThroughEarpieceAndroid: true
    })

    let sound = await new Audio.Sound()
    const status = {
        shouldPlay: true
    }

    await sound.loadAsync(fileRequired, status, false)
    sound.playAsync()
}

export default SoundPlayer