import React from 'react';
import { View } from 'react-native';

export default function LineSeparator () {
    return <View style={{ borderBottomColor: 'black', borderBottomWidth: 1 }} />
}