import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { SearchBar } from 'react-native-elements';
import {Picker} from '@react-native-picker/picker';

export default class Search extends Component
{
    constructor (props) {
        super(props);

        this.state = {
            searchValue: '',
            pickerValue: ''
        }

        this.pickerOptions = [
            'Distance',
            'Temporalité',
            'Lieu',
            'Catégorie'
        ];

        this.updateSearchValue = this.updateSearchValue.bind(this);
        this.forceCallback = this.forceCallback.bind(this);
    }

    updateSearchValue (value) 
    {
        this.setState({ searchValue: value });

        const foundElements = this.props.events.filter(e => {
            if (e && e.name) {
                return e.name.toUpperCase().includes(value.toUpperCase())
            }
        });
        this.props.searchCallback(foundElements);
    }

    forceCallback () {
        this.updateSearchValue(this.state.searchValue)
    }

    render () {
        return (
            <View style={[style.wrapper, this.props.passedStyle]}>
                <SearchBar
                    containerStyle={style.input}
                    inputStyle={{height: 25}}
                    onChangeText={value => this.updateSearchValue(value)}
                    placeholder="Chercher un évènement..."
                    value={this.state.searchValue}
                    lightTheme={true}
                    round={true}
                />

                {/* <Picker
                    selectedValue={this.state.pickerValue}
                    style={style.picker}
                    onValueChange={itemValue => this.setPickerValue(itemValue)}
                    mode='dropdown'
                    prompt='Choisissez votre filtre'
                >
                    {
                        this.pickerOptions.map((option, index) => {
                            return (
                                <Picker.Item key={index} label={option} value={option} />
                            )
                        })
                    }
                </Picker> */}
            </View>
        )
    }
}

const style = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        top: 550,
        flexDirection: 'row',
    },
    input: {
        backgroundColor: 'rgb(189, 198, 207)', 
        padding: 0,
        width: '100%'
    },
    picker: {
        height: 50,
        width: '30%',
        backgroundColor: 'white'
    }
});