#!/bin/bash

 cd back/commonuser && mvn clean install -DskipTests && cd ../..
# cd config && cp config.properties ../back/event/src/main/java/resources cd ..
cd back/event && mvn clean install -DskipTests && cd ../..
# cd back/event/src/main/java/resources && rm config.properties
# cd back/eventInviting && mvn clean install -DskipTests && cd ../..
cd back/eventComment && mvn clean install -DskipTests && cd ../..
cd back/userEvent && mvn clean install -DskipTests && cd ../..

# cd config && cp config.properties ../back/user/src/main/java/resources cd ..
cd back/user && mvn clean install -DskipTests && cd ../..
#cd back/user/src/main/java/resources && rm config.properties
cd back/authentication && mvn clean install -DskipTests && cd ../..
# cd back/userInviting && mvn clean install -DskipTests && cd ../..
cd back/userSuggestion && mvn clean install -DskipTests && cd ../..

# cd back/ticketOffice && mvn clean install -DskipTests && cd ../..
# cd back/ticketTransaction && mvn clean install -DskipTests && cd ../..
#modif pour test cicd

docker-compose up -d --build
