package com.bg.userevent.service;

import lombok.NonNull;
import org.springframework.http.ResponseEntity;

public class HandleErrorRequest {
    private static String error = "erreur lors de la requete (indéterminé)"; //by default

    public static String getError()
    {
        return error;
    }
    public static void setMSError(String microservice)
    {
        error = "Problème de communication avec le MS "+ microservice;
    }

    public static void setHttpError(String microservice, String statusCode)
    {
        error = "Problème de communication avec le MS " + microservice + " ou mauvaise requete (Status Code :  " + statusCode + ")...";
    }

    public static void noEventsOnMS(String microservice)
    {
        error = "Aucun evenement trouvé dans le MS " + microservice;
    }

    public static void noUserOnMS(String id)
    {
        error = "L'utilisateur [" + id + "] n'existe pas";
    }

    public static void setOtherError(String personnalError)
    {
        error = personnalError;
    }

    @NonNull
    public static ResponseEntity<Object> sendBadRequest() {
        return ResponseEntity.badRequest()
                .body("{\"message\" : \"" + HandleErrorRequest.getError() + "\" }");
    }

    //Faire la suite...
}
