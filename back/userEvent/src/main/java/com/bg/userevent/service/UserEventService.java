package com.bg.userevent.service;

import com.bg.userevent.config.RestTemplateClient;
import com.bg.userevent.model.EventModel;
import com.bg.userevent.model.UserEventModel;
import com.bg.userevent.model.UserModel;
import com.bg.userevent.repository.UserEventRepository;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserEventService {

        private final UserEventRepository userEventRepository;
        private final String baseUrlMSEvent = "http://event-service:8080/";
        private final String baseUrlMSUser ="http://user-service:8080/";
        private static final Logger logger = Logger.getLogger(UserEventService.class);

        RestTemplateClient restTemplate;

    public UserEventService(UserEventRepository userEventRepository, RestTemplateClient restTemplate){
        this.userEventRepository = userEventRepository;
        this.restTemplate = restTemplate;
    }


        //récupérer la liste des participants à chaque évènement
        public List<UserEventModel> getAllUserEvents() {
            logger.info("Recuperation de la liste des participants à chaque evenement...");
            List<UserEventModel> userEventList = new ArrayList<>();
            userEventRepository.findAll().forEach(userEventList::add);
            logger.info("Nombre de participants :" + userEventList.size());
            List<EventModel> events = convertUserEventToEvent(userEventList);
            return userEventList;
        }



        //récupérer la liste de tous les participants à un évènement donné
        public List<UserModel> getAllParticipateEvent(Integer eventId) {
            logger.info("Récuparation de la liste des participants sur l'évènement " + eventId + "...");
            if(isEventExist(eventId) == null)
            {
                return null;
            }
            List<UserEventModel> userEventList = new ArrayList<>();
            userEventRepository.findByEventIdAndStatus(eventId, UserEventModel.Status.PARTICIPATE).forEach(userEventList::add);
            logger.info("Nombre de participants sur l'evenement [" + eventId + "] : " + userEventList.size());
            return convertUserEventToUsers(userEventList);
        }

        //récupérer la liste de tous les intéressés à un évènement donné
        public List<UserModel> getAllInterestedEvent(Integer eventId) {
            logger.info("Récuparation de la liste des intéressés sur l'évènement " + eventId + "...");
            if(isEventExist(eventId) == null)
            {
                return null;
            }
            List<UserEventModel> userEventList = new ArrayList<>();
            userEventRepository.findByEventIdAndStatus(eventId, UserEventModel.Status.INTERESTED).forEach(userEventList::add);
            logger.info("Nombre de participants sur l'evenement " + eventId + ":" + userEventList.size());
            return convertUserEventToUsers(userEventList);
        }

        //récupérer si cet user est intéressé ou participe ? quel est son statut ?
        public Optional<UserEventModel> getUserEvent(Integer userId, Integer eventId) {
            logger.info("Recupération de l'état de l'utilisateur " + userId + " associé à l'evenement " + eventId + "...");
            return userEventRepository.findByUserIdAndEventId(userId, eventId);
        }

        //dire qu'un utilisateur participe à un évènement
        public String addParticipationUserEvent(Integer userId, Integer eventId) {
            UserEventModel userEventModel;
            logger.info("Modification de l'état de l'utilisateur " + userId + " associé à l'evenement " + eventId + "...");
            if(isUserExist(userId)!=null && isEventExist(eventId)!=null)
            {
                userEventModel = getUserEventModel(userId, eventId);
                userEventModel.setStatus(UserEventModel.Status.PARTICIPATE);
                userEventRepository.save(userEventModel);
                return "";
            }
            HandleErrorRequest.setOtherError("L'ID User " + userId + " ou l'Event Id" + eventId + "n'est/ne sont pas dans la BDD MS User et/ou MS Event");
            return HandleErrorRequest.getError();
        }

        //dire qu'un utilisateur est intéressé par un évènement
        public String addInterestedUserEvent(Integer userId, Integer eventId) {
            UserEventModel userEventModel;
            logger.info("Modification de l'état de l'utilisateur " + userId + "associé à l'evenement " + eventId + "...");
            if(isUserExist(userId)!=null && isEventExist(eventId)!=null)
            {
            userEventModel = getUserEventModel(userId, eventId);
            userEventModel.setStatus(UserEventModel.Status.INTERESTED);
                userEventRepository.save(userEventModel);
                return "";
            }
            HandleErrorRequest.setOtherError("L'ID User " + userId + " ou l'Event Id" + eventId + "n'est/ne sont pas dans la BDD MS User et/ou MS Event");
            return HandleErrorRequest.getError();
        }
    //Get User-Event if exist or create it
    private UserEventModel getUserEventModel(Integer userId, Integer eventId) {
        UserEventModel userEventModel;
        if(userEventRepository.findByUserIdAndEventId(userId,eventId).isPresent()) {
            logger.warn("L'utilisateur " + userId + " avec " + eventId + " existe déjà, mise à jour de son état...");
            userEventModel = userEventRepository.findByUserIdAndEventId(userId,eventId).get();
        }
        else {
            userEventModel = new UserEventModel();
            userEventModel.setUserId(userId);
            userEventModel.setEventId(eventId);
        }
        return userEventModel;
    }


        public List<UserModel> getUsersFromEvent(Integer eventId) {
            logger.info("Récupération des l'utilisateurs de l'evenement  " + eventId + "...");
            EventModel event = isEventExist(eventId);
            if(event!= null) {
                logger.info("Récupération des utilisateurs de l'event " + event.getName() + "...");
                List<UserEventModel> events = userEventRepository.findByEventId(eventId);
                //Check if event still exists
                return convertUserEventToUsers(events);
            }
            return null;

        }

    public List<EventModel> getEventsByUserId(Integer id) {
        UserModel user = isUserExist(id);
        if(user != null) {
            logger.info("Récupération des events de l'utilisateur " + user.getLogin() + "...");
            List<UserEventModel> events = userEventRepository.findByUserId(id);
            if(events != null) {
                return convertUserEventToEvent(events);
            }
            logger.error("Aucun evenements associé à l'utilisateur [" + id + "]");
        }

        return null;
    }



    //region Delete On Database
    private void deleteUser(Integer id) {
            List<UserEventModel> users = userEventRepository.findByUserId(id);
            if(users.size()==0) {
                return;
            }
        logger.info("Suppression de " + users.size() + " event dans la BDD...");
        userEventRepository.deleteAll(users);
    }

    private void deleteEvent(Integer id) {
        List<UserEventModel> events = userEventRepository.findByEventId(id);
        if(events.size()==0) {
            return;
        }
        logger.info("Suppression de " + events.size() + " event dans la BDD...");
        userEventRepository.deleteAll(events);
    }

    // effacer une réponse à un évènement pour un utilisateur
    public boolean deleteUserEvent(Integer userId, Integer eventId) {
        logger.info("Suppresion de l'utlisateur " + userId + " avec " + eventId + "...");
        UserEventModel userEventModel;
        if(userEventRepository.findByUserIdAndEventId(userId,eventId).isPresent()){

            userEventModel = userEventRepository.findByUserIdAndEventId(userId,eventId).get();
            userEventRepository.delete(userEventModel);
            return true;
        }
        logger.error("L'utilisateur " + userId + " avec " + eventId + "n'existe pas");
        return false;
    }
    //endregion

    //region Check On MS User
    public UserModel isUserExist(Integer id) {
        try {
            UserModel user = restTemplate.getForObject(baseUrlMSUser + "/" + id,UserModel.class);
            if(user==null)
            {
                logger.warn("L'utilisateur " + id + "n'existe pas");
                deleteUser(id);
                return null;
            }
            else
            {
                logger.debug("L'utilisateur " + id + "a été trouvé (login=" + user.getPseudonym() + ")");
                return user;
            }
        }catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("user",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("user");
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
    }

    public List<UserModel> convertUserEventToUsers(List<UserEventModel> userEventList) {
        //List UserEventModel to list Integer
        List<Integer> eventsIdList = userEventList.stream()
                .map(UserEventModel::getUserId)
                .collect(Collectors.toList());

        //Check on MS User event for each ids
        try {
        List<UserModel> listUsers = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<Integer>> entity = new HttpEntity<>(eventsIdList, headers);
        UserModel[] users = restTemplate.postForObject(baseUrlMSUser + "users", entity, UserModel[].class);
        if(users!= null) {
            listUsers = Arrays.asList(users);
            logger.info("Le MS Users a renvoyé [" + listUsers.size() + "] elements parmis les [" + userEventList.size() + "] envoyés");
            return listUsers;

        }

        }catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("user",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("user");
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        logger.info("Le MS Event n'a pas renvoyé d'elements parmis les ["  + userEventList.size() + "] envoyés ");
        return null;
        //TODO : Delete on database each event does not exist anymore
    }
    //endregion

    //region Check on MS Event
    public EventModel isEventExist(Integer id) {
        try
        {
            EventModel event = restTemplate.getForObject(baseUrlMSEvent + "/" +id,EventModel.class);
            if(event==null)
            {
                logger.warn("L'event [" + id + "] n'existe pas");
                deleteEvent(id);
                return null;
            }
            else
            {
                logger.debug("L'event [" + id + "] a été trouvé (titre=" + event.getName() + ")");
                return event;
            }
        }
        catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("event",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("event");
            logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
            return null;
        }

    }

    private List<EventModel> convertUserEventToEvent(List<UserEventModel> userEventList) {
        //List UserEventModel to list Integer
        List<Integer> eventsIdList = userEventList.stream()
                .map(UserEventModel::getEventId)
                .collect(Collectors.toList());

        //Check on MS Event event for each ids
        List<EventModel> listEvent = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<List<Integer>> entity = new HttpEntity<>(eventsIdList, headers);
            EventModel[] events = restTemplate.postForObject(baseUrlMSEvent + "events/", entity, EventModel[].class);
            if (events != null) {
                listEvent = Arrays.asList(events);
                logger.info("Le MS Event a renvoyé [" + listEvent.size() + "] elements parmis les [" + userEventList.size() + "] envoyés");
                return listEvent;
            }
      }
        catch (HttpStatusCodeException e) {
        //Cover 4XX and 5XX HttpStatus Error
        HandleErrorRequest.setHttpError("event",e.getStatusCode().toString());
        logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
        return null;
     }
        catch (ResourceAccessException e) {
        HandleErrorRequest.setMSError("event");
        logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
        return null;
     }
        logger.info("Le MS Event n'a pas renvoyé d'elements parmis les ["  + userEventList.size() + "] envoyés ");
        return null;

        //TODO : Delete on database each event does not exist anymore
    }
    //endregion






    }

