package com.bg.userevent;

import org.apache.catalina.User;
import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class UserEventApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserEventApplication.class, args);
    }

}
