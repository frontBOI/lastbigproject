package com.bg.userevent.controller;

import com.bg.userevent.model.EventModel;
import com.bg.userevent.model.UserEventModel;
import com.bg.userevent.model.UserModel;
import com.bg.userevent.service.HandleErrorRequest;
import com.bg.userevent.service.UserEventService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "User-Event", description = "API d'User-Event")
public class UserEventController {

    private final UserEventService userEventService;
    private static final Logger logger = Logger.getLogger(UserEventController.class);

    public UserEventController(UserEventService userEventService) {
        this.userEventService = userEventService;
    }


    @Operation(summary = "Simple Hello World", description = "Permet de savoir si le serveur est lancé", tags = { "Hello World" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Erreur (Communication)") })
    @RequestMapping("/")
    private HttpStatus isAlive(){
        logger.info("UserEvent service is Alive");
        return HttpStatus.OK;
    }

    @Operation(summary = "Récupérer la liste des participants à chaque évènement", description = "Retourne une list<UserEventModel>", tags = { "GetAllEvents" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserEventModel.class))),})
    @RequestMapping(method = RequestMethod.GET, value="/all")
    private List<UserEventModel> getAllEvents() {
        return userEventService.getAllUserEvents();
    }


    @Operation(summary = "Trouver les Users d'un Evenement donné", description = "Retourne une list<UserModel>", tags = { "GetUsersFromEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS User ou MS Event ou liste null") })
    @RequestMapping(method = RequestMethod.GET, value= "/users/{eventId}")
    private ResponseEntity<Object> getUsersFromEvent(
            @Parameter(description="ID De l'Utilisateur, ne peut pas être vide", required=true)
            @PathVariable String eventId) {
        List<UserModel> ret = userEventService.getUsersFromEvent(Integer.valueOf(eventId));
        if(ret!=null)
        {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(ret);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    //afficher tous les evenements d'un utilisateur, participe, interesse confondu
    @Operation(summary = "Afficher tous les evenements d'un utilisateur, participe, interesse confondu", description = "Retourne une list<EventModel>", tags = { "GetEventsFromUserId" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou MS User ou liste null") })
    @RequestMapping(method = RequestMethod.GET, value = "/events/{userId}")
    public ResponseEntity<Object> getEventsFromUserId(
            @Parameter(description="ID De l'Utilisateur, ne peut pas être vide", required=true)
            @PathVariable Integer userId)
    {
        List<EventModel> eventsFromUser = userEventService.getEventsByUserId(Integer.valueOf(userId));
        if(eventsFromUser!=null)
        {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(eventsFromUser);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @Operation(summary = "Récupère le status d'un évènement pour une personne", description = "Retourne un objet UserEventModel", tags = { "GetUserEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation, retourne null si il ne participe pas à l'évenement",
                    content = @Content(schema = @Schema(implementation = UserEventModel.class))) })
    @RequestMapping(method = RequestMethod.GET, value="/status/{eventId}/{userId}")
    //get le status d'un évènement pour une personne
    private UserEventModel getUserEvent(
            @Parameter(description="ID de L'evenement, ne peut pas être vide", required=true)
            @PathVariable Integer eventId,
            @Parameter(description="ID De l'Utilisateur, ne peut pas être vide", required=true)
            @PathVariable Integer userId) {
        Optional<UserEventModel> userEvent;
        userEvent = userEventService.getUserEvent(userId, (eventId));
        return userEvent.orElse(null);
    }

    @Operation(summary = "Récupère la liste des participants à un evenement", description = "Retourne une list<UserEventModel>", tags = { "GetUserEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou MS User ou liste null") })
    @RequestMapping(method = RequestMethod.GET, value="/{eventId}/participate")
    //get la liste des users qui vont participer
    private ResponseEntity<Object> getAllParticipateEvent(
            @Parameter(description="ID de L'evenement, ne peut pas être vide", required=true)
            @PathVariable Integer eventId) {
        List<UserModel> usersFromEvent = userEventService.getAllParticipateEvent(eventId);
        if(usersFromEvent!=null)
        {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(usersFromEvent);
        }
        String msgError =  "{\"message\" : \"Impossible d'aboutir à la requête.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
    }

    @Operation(summary = "Récupère la liste des interessé à un evenement", description = "Retourne une list<UserEventModel>", tags = { "GetAllInterestedEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou MS User ou liste null") })
    @RequestMapping(method = RequestMethod.GET, value="/{eventId}/interested")
    //get la liste des users qui sont interessés
    private ResponseEntity<Object> getAllInterestedEvent(
            @Parameter(description="ID de L'evenement, ne peut pas être vide", required=true)
            @PathVariable String eventId) {
        List<UserModel> usersFromEvent = userEventService.getAllInterestedEvent(Integer.valueOf(eventId));
        if(usersFromEvent!=null)
        {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(usersFromEvent);
        }
        return HandleErrorRequest.sendBadRequest();
    }

    @Operation(summary = "Faire participer un utilisateur à un evenement", description = "Faire participer un evenement", tags = { "AddParticipationUserEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou MS User problème de requete") })
    @RequestMapping(method = RequestMethod.POST, value = "/{eventId}/{userId}/participate")
    public ResponseEntity<Object> addParticipationUserEvent(
            @Parameter(description="ID de L'evenement, ne peut pas être vide", required=true)
            @PathVariable Integer eventId,
            @Parameter(description="ID de L'utilisateur, ne peut pas être vide", required=true)
            @PathVariable Integer userId) {
        String rep = userEventService.addParticipationUserEvent(userId, eventId);
        if(rep.length()>0)
        {
            String msgError =  "{\"message\" : \""+rep+"\" }";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(msgError);
        }
        String msgSucc =  "{\"message\" : \"L'utilisateur participe maintenant à l'évènement.\" }";
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgSucc);

    }

    @Operation(summary = "Faire interesser un utilisateur à un evenement", description = "Faire participer un evenement", tags = { "AddInterestedUserEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou MS User problème de requete") })
    @RequestMapping(method = RequestMethod.POST, value = "/{eventId}/{userId}/interested")
    public ResponseEntity<Object> addInterestedUserEvent(
            @Parameter(description="ID de L'evenement, ne peut pas être vide", required=true)
            @PathVariable Integer eventId,
            @Parameter(description="ID de L'utilisateur, ne peut pas être vide", required=true)
            @PathVariable Integer userId) {
        String rep = userEventService.addInterestedUserEvent(userId, eventId);
        if(rep.length()>0)
        {
            String msgError =  "{\"message\" : \""+rep+"\" }";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(msgError);
        }
        String msgSucc =  "{\"message\" : \"L'utilisateur s'intéresse maintenant à l'évènement.\" }";
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgSucc);
    }

    @Operation(summary = "Supprimer un User-Event", description = "Supprimer un User-Event", tags = { "DeleteUserEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Mauvaise requete ou User-Event n'existe pas)") })
    @RequestMapping(method = RequestMethod.DELETE, value = "/{eventId}/{userId}")
    public ResponseEntity<Object> deleteUserEvent(
            @Parameter(description="ID de L'evenement, ne peut pas être vide", required=true)
            @PathVariable Integer eventId,
            @Parameter(description="ID de L'utilisateur, ne peut pas être vide", required=true)
            @PathVariable Integer userId) {
        if(userEventService.deleteUserEvent(userId, eventId))
        {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(null);
        }
        return HandleErrorRequest.sendBadRequest();
    }


}
