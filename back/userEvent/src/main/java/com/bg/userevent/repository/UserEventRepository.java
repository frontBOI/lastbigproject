
package com.bg.userevent.repository;

import com.bg.userevent.model.UserEventModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface UserEventRepository extends CrudRepository<UserEventModel, Integer> {
    @Query("SELECT e FROM UserEventModel e WHERE e.userId = ?1 AND e.eventId = ?2")
    Optional<UserEventModel> findByUserIdAndEventId(Integer userId, Integer eventId);
    @Query("SELECT e FROM UserEventModel e WHERE e.eventId = ?1 AND e.status = ?2")
    List<UserEventModel> findByEventIdAndStatus(Integer eventId, UserEventModel.Status status);
    @Query("SELECT e FROM UserEventModel e WHERE e.userId = ?1")
    List<UserEventModel> findAllById(Integer userId);
    List<UserEventModel> findByUserId(Integer userId);
    List<UserEventModel> findByEventId(Integer eventId);
}


