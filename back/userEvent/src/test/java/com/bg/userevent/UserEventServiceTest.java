package com.bg.userevent;

import com.bg.userevent.config.RestTemplateClient;
import com.bg.userevent.model.EventModel;
import com.bg.userevent.model.UserEventModel;
import com.bg.userevent.model.UserModel;
import com.bg.userevent.repository.UserEventRepository;
import com.bg.userevent.service.UserEventService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserEventServiceTest {

    private static final Logger logger = Logger.getLogger(UserEventServiceTest.class);
    private final String baseUrlMSUser ="http://user-service:8080/";
    private final String baseUrlMSEvent = "http://event-service:8080/";
    private final String baseUrlMSUserEvent = "http://user-event-service:8080/";
    private final String baseUrlMSPrediction = "http://user-prediction-service:5000/";



    @Mock
    private RestTemplateClient restTemplate;
    @Mock private UserEventRepository userEventRepoMock;

    @InjectMocks
    @Spy
    private UserEventService serviceMock;
    private UserModel user;
    private final List<EventModel> events = new ArrayList<>();

    private AutoCloseable closeable;


    @Before
    public void init()  {
        closeable = MockitoAnnotations.openMocks(this);
        System.out.println("Initialisation...");
        //region initialisationJson
        String jsonUser1="{\n"+
                "\"login\":\"momo2\",\n"+
                "\"pwd\":\"momo2\",\n"+
                "\"pseudonym\":\"momo2\",\n"+
                "\"lastName\":\"momoq2\",\n"+
                "\"firstName\":\"momoq2\",\n"+
                "\"email\":\"momo@gmail.com\",\n"+
                "\"address\":\"chezlacremière\",\n"+
                "\"age\":23,\n"+
                "\"tags\":[\"sport\",\"musique\"]\n"+
                "}";
        //UserModel user1=null;

        //users.add(user1);
        String jsonEvent1 = "    {\n" +
                "        \"id\": 142,\n" +
                "        \"meetupId\": \"275562802\",\n" +
                "        \"name\": \"Faciliter le travail à distance avec Windows Virtual Desktop\",\n" +
                "        \"duration\": 3600000,\n" +
                "        \"localDate\": \"2021-01-27T00:00:00.000+00:00\",\n" +
                "        \"localTime\": \"12:30:00\",\n" +
                "        \"utcOffset\": 3600000,\n" +
                "        \"venueName\": \"Online event\",\n" +
                "        \"venueLat\": null,\n" +
                "        \"venueLon\": null,\n" +
                "        \"venueAddressString\": null,\n" +
                "        \"organisateurMeetupId\": 32322977,\n" +
                "        \"organisateurLocalId\": null,\n" +
                "        \"eventLink\": \"https://www.meetup.com/Azure-Group-Lyon/events/275562802/\",\n" +
                "        \"description\": \"<p>### MEETUP AZURE NANTES EN VISIO ###</p>\",\n" +
                "        \"visibility\": false,\n" +
                "        \"ticketing\": null,\n" +
                "        \"categorie\": []\n" +
                "    },";

        String jsonEvent2 = "    {\n" +
                "        \"id\": 143,\n" +
                "        \"meetupId\": \"lqjgrqycccbbc\",\n" +
                "        \"name\": \"Le dispositif Pinel 2019 en détail\",\n" +
                "        \"duration\": 7200000,\n" +
                "        \"localDate\": \"2021-01-20T00:00:00.000+00:00\",\n" +
                "        \"localTime\": \"19:00:00\",\n" +
                "        \"utcOffset\": 3600000,\n" +
                "        \"venueName\": null,\n" +
                "        \"venueLat\": null,\n" +
                "        \"venueLon\": null,\n" +
                "        \"venueAddressString\": null,\n" +
                "        \"organisateurMeetupId\": 22201510,\n" +
                "        \"organisateurLocalId\": null,\n" +
                "        \"eventLink\": \"https://www.meetup.com/Meetup-independance-financiere/events/lqjgrqycccbbc/\",\n" +
                "        \"description\": \"<p>Présentation du dispositif Pinel pour acquérir un logment en réduisant ses impôts</p> \",\n" +
                "        \"visibility\": false,\n" +
                "        \"ticketing\": null,\n" +
                "        \"categorie\": []\n" +
                "    }";
        //endregion
        ObjectMapper mapper = new ObjectMapper();
        EventModel event1 = null;
        try {
            event1 = mapper.readValue(jsonEvent1, EventModel.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        EventModel event2 = null;
        try {
            event2 = mapper.readValue(jsonEvent2, EventModel.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try{
            user = mapper.readValue(jsonUser1, UserModel.class);
        }catch(JsonProcessingException e){
            e.printStackTrace();
        }
        events.add(event1);
        events.add(event2);

    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }

    @Test
    public void getAllUserEventsWhenEventExist() {

    }

    @Test
    public void getAllParticipateEventWhenEventExist() {
        //init
        Integer id = 3;
        Integer eventId = 1;
        UserEventModel ueModel = new UserEventModel();
        ueModel.setId(1);
        ueModel.setEventId(1);
        ueModel.setUserId(1);
        ueModel.setStatus(UserEventModel.Status.PARTICIPATE);

        List<UserEventModel> ueList = new ArrayList<>();
        List<UserModel> uList = new ArrayList<>();
        ueList.add(ueModel);
        uList.add(user);


        //when
        Mockito.doReturn(events.get(1)).when(serviceMock).isEventExist(eventId);
        Mockito.when(userEventRepoMock.findByEventIdAndStatus(eventId, UserEventModel.Status.PARTICIPATE)).thenReturn(ueList);
        Mockito.doReturn(uList).when(serviceMock).convertUserEventToUsers(ueList);

        //do
        serviceMock.getAllParticipateEvent(id);
        //Assert.assertEquals("GET ALL PARTICIPATE EVENT WHEN EVENT EXSIT", user, uList);
    }

    @Test
    public void getAllInterestedEvent() {
    }

    @Test
    public void getUserEvent() {
    }

    @Test
    public void addParticipationUserEvent() {
    }

    @Test
    public void addInterestedUserEvent() {
    }

    @Test
    public void getUsersFromEvent() {
    }

    @Test
    public void getEventsByUserId() {
    }

    @Test
    public void deleteUserEvent() {
    }

    @Test
    public void UserDoesntExistTest() {
        Integer id = 48080;
        Mockito.when(restTemplate.getForObject(baseUrlMSUser + "/" + id, UserModel.class))
                .thenReturn(null);
        UserModel test = serviceMock.isUserExist(id);
        Assert.assertNull("USER DOESNT EXIST", test);
    }


    @Test
    public void UserExistTest() {
        Integer id = 3;
        Mockito.when(restTemplate.getForObject(baseUrlMSUser + "/" + id, UserModel.class))
                .thenReturn(user);
        UserModel testUser = serviceMock.isUserExist(id);
        Assert.assertEquals("USER EXIST", testUser, user);


    }

    @Test
    public void EventExistTest() {
        Integer eventId = 1;
        Mockito.when(restTemplate.getForObject(baseUrlMSEvent + "/" + eventId, EventModel.class))
                .thenReturn(events.get(1));
        EventModel event = serviceMock.isEventExist(eventId);
        Assert.assertEquals("EVENT EXIST", event, events.get(1));
    }

    @Test
    public void EventDoesntExistTest() {
        Integer eventId = 1;
        Mockito.when(restTemplate.getForObject(baseUrlMSEvent + "/" + eventId, EventModel.class))
                .thenReturn(null);
        EventModel event = serviceMock.isEventExist(eventId);
        Assert.assertNull("EVENT DOESN'T EXIST", event);
    }

}
