package com.bg.eventcomment.controller;

import com.bg.eventcomment.model.EventCommentModel;
import com.bg.eventcomment.service.EventCommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EventCommentController {
    private final EventCommentService eventCommentService;

    public EventCommentController(EventCommentService eventCommentService) {
        this.eventCommentService = eventCommentService;
    }

    @RequestMapping("/")
    public String isAwake() {
        System.out.println("wesh");
        return "wesh";
    }

    //add a comment
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public ResponseEntity<Object> addEventComment(@RequestBody EventCommentModel event) {
        EventCommentModel comment = eventCommentService.addEventComment(event);
        if(comment != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(comment);
        }
        String msgError =  "{\"message\" : \"Commentaire non ajouté en base\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
    }


    //delete a comment
    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public ResponseEntity<Object> deleteEventComment(@PathVariable String id) {
        if(eventCommentService.deleteEventComment(id)){
            String msgError =  "{\"message\" : \"Le commentaire a bien été supprimé.\" }";
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(msgError);
        }

        String msgError =  "{\"message\" : \"Le commentaire n'a pas été supprimé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
    }


    //update a comment
    @RequestMapping(method = RequestMethod.PUT, value = "/update/{id}")
    public ResponseEntity<Object> updateEventComment(@RequestBody EventCommentModel eventComment, @PathVariable String id) {
        //eventComment.setCommentId(Integer.valueOf(id));
        if (!id.equals(eventComment.getCommentId())) {
            String msgError =  "{\"message\" : \"Commentaire non ajouté en base car les id sont différents\" }";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(msgError);
        }
        EventCommentModel comment = eventCommentService.updateEventComment(eventComment);
        if(comment!= null){
            return ResponseEntity.status(HttpStatus.OK).body(comment);
        }
        String msgError =  "{\"message\" : \"Commentaire non ajouté en base\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);

    }

    //get comments from an event
    @RequestMapping(method = RequestMethod.GET, value = "/all/{id}")
    public ResponseEntity<Object> getEventComments (@PathVariable String id) {
        List<EventCommentModel> comments;
        comments = eventCommentService.getAllEventComments(id);
        if(!comments.isEmpty()){
            return ResponseEntity.status(HttpStatus.OK).body(comments);
        }
        String msgVide =  "{\"message\" : \"Aucun commentaire n'a été trouvé.\" }";
        return ResponseEntity.status(HttpStatus.OK)
                .body(msgVide);
    }
}
