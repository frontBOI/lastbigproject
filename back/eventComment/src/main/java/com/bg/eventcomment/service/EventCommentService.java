package com.bg.eventcomment.service;

import com.bg.eventcomment.config.RestTemplateClient;
import com.bg.eventcomment.model.EventCommentModel;
import com.bg.eventcomment.model.UserModel;
import com.bg.eventcomment.repository.EventCommentRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
public class EventCommentService {
    private final EventCommentRepository eventCommentRepository;
    private final String baseUrlMSUser ="http://user-service:8080/";

    RestTemplate restTemplate = new RestTemplate();

    private static final Logger logger = Logger.getLogger(EventCommentService.class);

    //constructeur
    public EventCommentService(EventCommentRepository eventCommentRepository) {
        this.eventCommentRepository = eventCommentRepository;
    }

    //ajouter un commentaire d'utilisateur sur un evenement
    public EventCommentModel addEventComment(EventCommentModel eventComment) {
        logger.debug("Ajout d'un commentaire de l'utilisateur [" + eventComment.getUserId() + "]");
        UserModel user = isUserExist(eventComment.getUserId());
        if(user!=null) {
            try {
                eventComment.setUserName(user.getPseudonym());
                EventCommentModel newEventComment  = eventCommentRepository.save(eventComment);
                return newEventComment;
            }
            catch (Exception e) {
                logger.debug("Error encountered : " + e.toString());
                return null;
            }

        }
        return null;
    }

    public List<EventCommentModel> getAllEventComments(String id) {
        return eventCommentRepository.findByEventId(Integer.valueOf(id));
    }

    public Optional<EventCommentModel> getEventComment(Integer id) {
        return eventCommentRepository.findById(id);
    }

    public EventCommentModel updateEventComment(EventCommentModel eventComment) {

        Optional<EventCommentModel> eventCommentExisting = eventCommentRepository.findByCommentId(eventComment.getEventId());
        if(eventCommentExisting.isEmpty()) {
            return null;
        }

        UserModel user = isUserExist(eventComment.getUserId());
        if(user!=null) {
            eventComment.setUserName(user.getPseudonym());
        }
        else
        {
            return null;
        }
        try {
            eventCommentRepository.save(eventComment);
        }
        catch (Exception e) {
            logger.debug("Error encountered : " + e.toString());
            return null;
        }

        Optional<EventCommentModel> eventCommentUpdated = eventCommentRepository.findByCommentId(eventComment.getEventId());

        return eventCommentUpdated.get();
    }

    public boolean deleteEventComment(String id) {
        Optional<EventCommentModel> comment = getEventComment(Integer.valueOf(id));
        if(comment.isPresent()){
            eventCommentRepository.deleteById(Integer.valueOf(id));
            return true;
        }
        return false;
    }

    //region on MS User
    public UserModel isUserExist(Integer id) {
        try {

            UserModel user = restTemplate.getForObject(baseUrlMSUser + "/" + id,UserModel.class);
            if(user==null)
            {
                HandleErrorRequest.noUserOnMS(id.toString());
                logger.warn(HandleErrorRequest.getError());
                return null;
            }
            else
            {
                logger.debug("L'utilisateur [" + id + "] a été trouvé (login = " + user.getPseudonym() + ")");
                return user;
            }
        }
        catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("user",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("user");
            logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
            return null;
        }


    }
    //endregion

}
