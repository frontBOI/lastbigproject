package com.bg.eventcomment.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class EventModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String meetupId;
    private String name;
    private Integer duration;
    private Date localDate;
    private Time localTime;
    private Integer utcOffset;
    private String venueName;
    private String venueLat;
    private String venueLon;
    private String venueAddressString;
    private Integer organisateurMeetupId;
    private Integer organisateurLocalId;
    private String eventLink;
    @Column(columnDefinition = "TEXT")
    private String description;
    private Boolean visibility;
    private Integer ticketing;
    @ElementCollection
    private List<String> categorie = new ArrayList<String>();

    public EventModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMeetupId() {
        return meetupId;
    }

    public void setMeetupId(String meetupId) {
        this.meetupId = meetupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public void setLocalDate(Date date) {
        this.localDate = date;
    }

    public Time getLocalTime() {
        return localTime;
    }

    public void setLocalTime(Time time) {
        this.localTime = time;
    }

    public Integer getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(Integer offset) {
        this.utcOffset = offset;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueLat() {
        return venueLat;
    }

    public void setVenueLat(String lattitude) {
        this.venueLat = lattitude;
    }

    public String getVenueLon() {
        return venueLon;
    }

    public void setVenueLon(String longitude) {
        this.venueLon = longitude;
    }

    public String getVenueAddressString() {
        return venueAddressString;
    }

    public void setVenueAddressString(String adresse) {
        this.venueAddressString = adresse;
    }

    public Integer getOrganisateurMeetupId() {
        return organisateurMeetupId;
    }

    public void setOrganisateurMeetupId(Integer orgaMeetupId) {
        this.organisateurMeetupId = orgaMeetupId;
    }

    public Integer getOrganisateurLocalId() {
        return organisateurLocalId;
    }

    public void setOrganisateurLocalId(Integer orgaInterneId) {
        this.organisateurLocalId = orgaInterneId;
    }

    public String getEventLink() {
        return eventLink;
    }

    public void setEventLink(String meetupLink) {
        this.eventLink = meetupLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibilityPublic) {
        this.visibility = visibilityPublic;
    }

    public Integer getTicketing() {
        return ticketing;
    }

    public void setTicketing(Integer ticketing) {
        this.ticketing = ticketing;
    }
    public List<String> getCategorie() {
        return categorie;
    }

    public void setCategorie(List<String> categorie) {
        this.categorie = categorie;
    }
}
