package com.bg.eventcomment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventCommentApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventCommentApplication.class, args);
    }

}
