package com.bg.eventcomment.repository;

import com.bg.eventcomment.model.EventCommentModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EventCommentRepository extends CrudRepository<EventCommentModel, Integer> {
    @Query("SELECT e FROM EventCommentModel e WHERE e.eventId = ?1")
    List<EventCommentModel> findByEventId(Integer eventId);
    Optional<EventCommentModel> findByCommentId(Integer commentId);


}
