package com.bg.activemq.rest;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventDTO {
    private Integer id;
    private String meetupId;
    private String name;
    private Integer duration;
    private Date localDate;
    private Time localTime;
    private Integer utcOffset;
    private String venueName;
    private String venueLat;
    private String venueLon;
    private String venueAddressString;
    private Integer organisateurMeetupId;
    private Integer organisateurLocalId;
    private String eventLink;
    private String description;
    private Boolean visibility;
    private Integer ticketing;
    private List<String> categorie = new ArrayList<String>();

    public EventDTO()
    {
    }

    public EventDTO(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getMeetupId() {
        return meetupId;
    }

    public String getName() {
        return name;
    }

    public Integer getDuration() {
        return duration;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public Time getLocalTime() {
        return localTime;
    }

    public Integer getUtcOffset() {
        return utcOffset;
    }

    public String getVenueName() {
        return venueName;
    }

    public String getVenueLat() {
        return venueLat;
    }

    public String getVenueLon() {
        return venueLon;
    }

    public String getVenueAddressString() {
        return venueAddressString;
    }

    public Integer getOrganisateurMeetupId() {
        return organisateurMeetupId;
    }

    public Integer getOrganisateurLocalId() {
        return organisateurLocalId;
    }

    public String getEventLink() {
        return eventLink;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public Integer getTicketing() {
        return ticketing;
    }

    public List<String> getCategorie() {
        return categorie;
    }
}
