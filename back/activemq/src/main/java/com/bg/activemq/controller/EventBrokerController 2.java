package com.bg.activemq.controller;

import com.bg.activemq.rest.EventDTO;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class EventBrokerController {
    @MessageMapping("/hello")
    @SendTo("/topic/events")
    public EventDTO greeting(EventDTO eventMess) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new EventDTO(eventMess.getName());
    }

}
