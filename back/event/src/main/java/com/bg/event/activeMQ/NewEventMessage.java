package com.bg.event.activeMQ;

import java.io.Serializable;

public class NewEventMessage implements Serializable {
    private final int id;
    private final String name;

    public NewEventMessage(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "NewEventMessage{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}