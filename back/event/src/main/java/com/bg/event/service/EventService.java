package com.bg.event.service;

import com.bg.event.activeMQ.Sender;
import com.bg.event.apiConfig.EventAPI;
import com.bg.event.model.EventModel;
import com.bg.event.repository.EventRepository;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EventService {
    //TEST
    @Autowired
    private Sender sender;

    private ObjectMapper mapper;
    //


    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
        this.mapper = new ObjectMapper();
    }

    public EventModel addEvent(EventModel event) {
//         needed to avoid detached entity passed to persist error
        if (event.getId() != null) {
            Optional<EventModel> ev = getEvent(event.getId());
            if (ev.isPresent()) {
                return null;
            }
        }
        eventRepository.save(event);
        this.publishEventESB(event);
        return event;
    }

    public List<EventModel> getAllEvents() {
        List<EventModel> eventList = new ArrayList<>();
        eventRepository.findAll().forEach(eventList::add);
        if (eventList.size() == 0)
            return null;
        return eventList;
    }

    public Optional<EventModel> getEvent(Integer id) {
        return eventRepository.findById(id);
    }

    public EventModel updateEvent(EventModel event) {
        Optional<EventModel> ev = getEvent(event.getId());
        if(ev.isPresent())
        {
            ev = Optional.of(event);
            eventRepository.save(ev.get());
            return ev.get();
        }
        return null;
    }

    public boolean deleteEvent(String id) {
        Optional<EventModel> ev = getEvent(Integer.valueOf(id));
        if(ev.isPresent()) {
            eventRepository.deleteById(Integer.valueOf(id));
            return true;
        }
        return false;

    }

    public List<EventModel> getUpcomingEventsAPI(String lat, String lon) {
        return EventAPI.getInstance().getUpcomingEventsAPI(lat,lon);
    }

    public List<EventModel> getUpcomingEventsDB(String lat, String lon) {
        String lat1 = String.valueOf(Float.parseFloat(lat) - 1);
        String lat2 = String.valueOf(Float.parseFloat(lat) + 1);
        String lon1 = String.valueOf(Float.parseFloat(lon) - 1);
        String lon2 = String.valueOf(Float.parseFloat(lon) + 1);
        return  eventRepository.findAllEventsDB(lat1,lat2,lon1,lon2);
    }

    public void updateDBEvent(List<EventModel> listEvent) {
        List<EventModel> listEventToSave = new ArrayList<>();
        for (EventModel event : listEvent) {
            if(eventRepository.findByMeetUpId(event.getMeetupId()) == null)
            {
                listEventToSave.add(event);
                this.publishEventESB(event);
            }
        }

        //TEST
        /*
        EventAddRequest event = new EventAddRequest();
        event.setDescription("TEST NOA");
        event.setName("TEST NOA");
        sender.send(event);*/
        /*
        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
        assertThat(receiver.getLatch().getCount()).isEqualTo(0);*/


        System.out.println("Nombre d'évènement en plus à sauvegarder : "+listEventToSave.size());
        eventRepository.saveAll(listEventToSave);
    }
    //List id event -> objets events
    public Iterable<EventModel> getEventsByIdEvent(List<String> listIdEvent) {
        List<Integer> iterableListId = new ArrayList<>();
        for(String s : listIdEvent) iterableListId.add(Integer.valueOf(s));
        return eventRepository.findAllById(iterableListId);
    }

    public Iterable<EventModel> getEventsByMeetupIdEvent(List<String> listMeetupId) {
        return eventRepository.findByMeetupIdIn(listMeetupId);
    }

    public Optional<EventModel> getEventsByFiltreDistance(String lat1, String lat2, String lon1, String lon2) {
        return eventRepository.findByDistance(lat1, lat2, lon1, lon2);
    }

    public Optional<EventModel> getEventsByFiltreDate(Date date1, Date date2) {
        return eventRepository.findByDate(date1, date2);
    }

    private void publishEventESB(EventModel eventModel){
        try {
            String eventJSON = this.mapper.writeValueAsString(eventModel);
            sender.send(eventJSON);

        }
        catch (JsonProcessingException e){
            e.printStackTrace();
        }
    }

    public Optional<EventModel> getEventsByFiltrePlace(String place) {
        return eventRepository.findByPlace(place);
    }

    public EventModel setEventCoordinatesFromAddress(EventModel event) {
        //API http://api.positionstack.com/v1/forward?access_key=8b603d24d5d5b86440c9fe64f1b74527&query=[address]
        if (event.getVenueAddressString() == null || event.getVenueAddressString().isEmpty()
                || event.getVenueCity() == null || event.getVenueCity().isEmpty()){// Si pas d'adresse pas de coordonnées
            return event;
        }
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = null;
        String uri = "http://api.positionstack.com/v1/forward?" +
                "access_key=8b603d24d5d5b86440c9fe64f1b74527" +
                "&query="+event.getEntireAddress() +
                "&output=json";
        uri = uri.replaceAll(" ","%20");
        request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                //                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = null;
        try {
            response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if (response.statusCode() != 200){
            System.out.println(response.body().split("message\":\"")[1].split("\"")[0]);
            return event;
        }else {
            Object body = null;
            try {
                body = new JSONParser().parse(response.body());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ArrayList listAddress = (ArrayList) ((JSONObject) body).get("data");
            if (((JSONObject) listAddress.get(0)).get("latitude") != null
                    && ((JSONObject) listAddress.get(0)).get("longitude") != null){
                event.setVenueLat(((JSONObject) listAddress.get(0)).get("latitude").toString());
                event.setVenueLon(((JSONObject) listAddress.get(0)).get("longitude").toString());
            }
        }
        return event;
    }


}

