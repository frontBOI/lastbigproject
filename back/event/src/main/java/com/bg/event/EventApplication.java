package com.bg.event;

import com.bg.event.activeMQ.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableJms
public class EventApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventApplication.class, args);
		//Test

	}

}
