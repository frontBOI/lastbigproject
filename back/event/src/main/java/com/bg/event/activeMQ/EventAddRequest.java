package com.bg.event.activeMQ;

import java.sql.Time;
import java.util.Date;

public class EventAddRequest {
    private Integer id;
    private String meetupId;
    private String name;
    private Integer duration;
    private Date localDate;
    private Time localTime;
    private Integer utcOffset;
    private String venueName;
    private String venueLat;
    private String venueLon;
    private String venueAddressString;
    private Integer organisateurMeetupId;
    private Integer organisateurLocalId;
    private String eventLink;
    private String description;
    private Boolean visibility;
    private Integer ticketing;

    public EventAddRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMeetupId() {
        return meetupId;
    }

    public void setMeetupId(String meetupId) {
        this.meetupId = meetupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public void setLocalDate(Date localDate) {
        this.localDate = localDate;
    }

    public Time getLocalTime() {
        return localTime;
    }

    public void setLocalTime(Time localTime) {
        this.localTime = localTime;
    }

    public Integer getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(Integer utcOffset) {
        this.utcOffset = utcOffset;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueLat() {
        return venueLat;
    }

    public void setVenueLat(String venueLat) {
        this.venueLat = venueLat;
    }

    public String getVenueLon() {
        return venueLon;
    }

    public void setVenueLon(String venueLon) {
        this.venueLon = venueLon;
    }

    public String getVenueAddressString() {
        return venueAddressString;
    }

    public void setVenueAddressString(String venueAddressString) {
        this.venueAddressString = venueAddressString;
    }

    public Integer getOrganisateurMeetupId() {
        return organisateurMeetupId;
    }

    public void setOrganisateurMeetupId(Integer organisateurMeetupId) {
        this.organisateurMeetupId = organisateurMeetupId;
    }

    public Integer getOrganisateurLocalId() {
        return organisateurLocalId;
    }

    public void setOrganisateurLocalId(Integer organisateurLocalId) {
        this.organisateurLocalId = organisateurLocalId;
    }

    public String getEventLink() {
        return eventLink;
    }

    public void setEventLink(String eventLink) {
        this.eventLink = eventLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    public Integer getTicketing() {
        return ticketing;
    }

    public void setTicketing(Integer ticketing) {
        this.ticketing = ticketing;
    }

    @Override
    public String toString() {
        return "EventAddRequest{" +
                "id=" + id +
                ", meetupId='" + meetupId + '\'' +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", localDate=" + localDate +
                ", localTime=" + localTime +
                ", utcOffset=" + utcOffset +
                ", venueName='" + venueName + '\'' +
                ", venueLat='" + venueLat + '\'' +
                ", venueLon='" + venueLon + '\'' +
                ", venueAddressString='" + venueAddressString + '\'' +
                ", organisateurMeetupId=" + organisateurMeetupId +
                ", organisateurLocalId=" + organisateurLocalId +
                ", eventLink='" + eventLink + '\'' +
                ", description='" + description + '\'' +
                ", visibility=" + visibility +
                ", ticketing=" + ticketing +
                '}';
    }
}
