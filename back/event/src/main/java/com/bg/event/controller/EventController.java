package com.bg.event.controller;

import com.bg.event.model.EventModel;
import com.bg.event.service.EventService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class EventController {
    private final EventService eventService;
    private static Logger logger =
            LoggerFactory.getLogger(EventController.class);

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @Operation(summary = "Simple Hello World",
            description = "Permet de savoir si le serveur est lancé", tags = { "Hello World" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Erreur (Communication)") })
    @RequestMapping("/")
    public HttpStatus isAlive()
    {
        logger.info("Event service is Alive!");
        return HttpStatus.OK;
    }

    @Operation(summary = "Récupération de tous les events",
            description = "Retourne une list<EventModel>", tags = { "GetAllEvents" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou liste null") })
    @RequestMapping("/all")
    private ResponseEntity<Object> getAllEvents() {
        List<EventModel> events = eventService.getAllEvents();
        if(events != null) {
            return ResponseEntity.status(HttpStatus.OK).body(events);
        }
        String msgError =  "{\"message\" : \"Aucun évènement n'a été trouvé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
//        return eventService.getAllEvents();
    }
    @Operation(summary = "Récupération d'un event via un id'",
            description = "Retourne un EventModel", tags = { "GetEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou objet null") })
    @RequestMapping("/{id}")
    private ResponseEntity<Object> getEvent(@PathVariable String id) {
        Optional<EventModel> revent;
        int idInt = Integer.parseInt(id);
        revent = eventService.getEvent(idInt);
//        return revent.orElse(null);
        if(!revent.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(revent);
        }
        String msgError =  "{\"message\" : \"Aucun évènement n'a été trouvé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
    }
    @Operation(summary = "Récupération de tous les events via une liste d'id",
            description = "Retourne une list<EventModel>", tags = { "GetAllEventsFromId" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou liste null") })
    @RequestMapping(method = RequestMethod.POST, value = "/events")
    private ResponseEntity<Object> getEventsFromId(@RequestBody String[] listIdEvent) {
        logger.debug("List ID Event recus :" + listIdEvent.toString());
        Iterable<EventModel> listEvent = eventService.getEventsByIdEvent(Arrays.asList(listIdEvent));
        if(listEvent.iterator().hasNext()) {
            return ResponseEntity.status(HttpStatus.OK).body(listEvent);
        }
        String msgError =  "{\"message\" : \"Aucun évènement n'a été trouvé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(null);
//        return eventService.getEventsByIdEvent(Arrays.asList(listIdEvent));
    }

    @Operation(summary = "Récupération de tous les events via une liste d'id",
            description = "Retourne une list<EventModel>", tags = { "GetEventsFromMeetupId" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou liste null") })
    @RequestMapping(method = RequestMethod.POST, value = "/meetup_events")
    private ResponseEntity<Object> getEventsFromMeetupId(@RequestBody String[] listIdEvent) {
        logger.debug("List ID Event recus :" + listIdEvent.toString());
        Iterable<EventModel> listEvent = eventService.getEventsByMeetupIdEvent(Arrays.asList(listIdEvent));
        if(listEvent.iterator().hasNext()) {
            return ResponseEntity.status(HttpStatus.OK).body(listEvent);
        }
        String msgError =  "{\"message\" : \"Aucun évènement n'a été trouvé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(null);
//        return eventService.getEventsByIdEvent(Arrays.asList(listIdEvent));
    }

    @Operation(summary = "Récupération de tous les events selon une position",
            description = "Retourne une list<EventModel>", tags = { "getUpcomingEvents" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou liste null") })
    @RequestMapping(method = RequestMethod.POST, value = "/upcoming_events")
    private ResponseEntity<Object> getUpcomingEvents(@RequestBody String[] coord) {
        logger.debug("Coordonnées recues : " + coord[0] + "||" + coord[1]);
        //Appel API pour récup les 200 prochains évé selon lat/long
        List<EventModel> listEvent = eventService.getUpcomingEventsAPI(coord[0], coord[1]);
        System.out.println("Evènements API : "+listEvent.size());
        //Maj de la bdd des événements qui ne sont pas déjà dans la base
        eventService.updateDBEvent(listEvent);
        //Requete sur les évènements de la base des évènements selon lat/long
        listEvent = eventService.getUpcomingEventsDB(coord[0], coord[1]);
        if(listEvent.iterator().hasNext()) {
            return ResponseEntity.status(HttpStatus.OK).body(listEvent);
        }
        String msgError =  "{\"message\" : \"Aucun évènement n'a été trouvé pour ces coordonnées.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
//        return listEvent;
    }
    @Operation(summary = "Récupération de tous les events selon une position",
            description = "Retourne une list<EventModel>", tags = { "getUpcomingEvents" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou liste null") })
    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public ResponseEntity<Object> addEvent(@RequestBody EventModel event) {
        EventModel ev = eventService.setEventCoordinatesFromAddress(event);
        ev = eventService.addEvent(ev);
        if(ev!=null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(ev);
        }
        String msgError =  "{\"message\" : \"Evènement déjà présent dans la bdd\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/update/{id}")
    public ResponseEntity<Object> updateEvent(@RequestBody EventModel event, @PathVariable String id) {
        event.setId(Integer.valueOf(id));
        EventModel ev = eventService.updateEvent(event);
        if(ev!=null) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(ev);
        }
        String msgError =  "{\"message\" : \"L'évènement n'a pas pu être trouvé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);

    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public ResponseEntity<Object> deleteEvent(@PathVariable String id) {
        if(eventService.deleteEvent(id)) {
            String msgError =  "{\"message\" : \"L'évènement a bien été supprimé.\" }";
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(msgError);
        }
        String msgError =  "{\"message\" : \"L'évènement n'a pas pu être supprimé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/findByDistance/{lat1}/{lat2}/{lon1}/{lon2}")
    private Optional<EventModel> getEventsByFiltreDistance (@PathVariable String lat1, String lat2, String lon1, String lon2){
        return eventService.getEventsByFiltreDistance(lat1 ,lat2 ,lon1 ,lon2);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/findByDate")
    private Optional<EventModel> getEventsByFiltreDate (@RequestBody Date date1, Date date2){
        return eventService.getEventsByFiltreDate(date1, date2);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/findByPlace")
    private Optional<EventModel> getEventsByFiltrePlace (@RequestBody String place ){
        return eventService.getEventsByFiltrePlace(place);
    }

    @RequestMapping("/refresh")
    public void refreshAPI()
    {

    }
}
