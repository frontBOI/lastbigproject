package com.bg.event.repository;

import com.bg.event.model.EventModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface EventRepository extends CrudRepository<EventModel, Integer> {
    @Query("SELECT e FROM EventModel e WHERE ( e.venueLat BETWEEN ?1 AND ?2 AND e.venueLon BETWEEN ?3 AND ?4 ) " +
            "OR ( e.venueLat is null AND e.venueLon is null )")
    List<EventModel> findAllEventsDB(String lat1,String lat2, String lon1, String lon2);

    @Query("SELECT e FROM EventModel e WHERE e.meetupId = ?1")
    EventModel findByMeetUpId(String meetupId);

    @Query("SELECT e FROM EventModel e WHERE ( e.venueLat BETWEEN ?1 AND ?2 ) AND ( e.venueLon BETWEEN ?3 AND ?4 )")
    Optional<EventModel> findByDistance(String lat1,String lat2,  String lon1, String lon2);

    @Query("SELECT e FROM EventModel e WHERE ( e.localDate BETWEEN ?1 AND ?2 ) ")
    Optional<EventModel> findByDate(Date date1, Date date2);

    @Query("SELECT e FROM EventModel e WHERE e.venueAddressString like ?1")
    Optional<EventModel> findByPlace(String place);

    Optional<EventModel> findByCategorie(String categorie);

    Iterable<EventModel> findByMeetupIdIn( List<String> listMeetupIds);


    //Optional<EventModel> findByPrix(String price );
    //Optional<EventModel> findByPublic(String publicVise);
}
