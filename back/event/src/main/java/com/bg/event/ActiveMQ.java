package com.bg.event;

public final class ActiveMQ {
    // ActiveMQ config
    public static final String BROKER_URL = "tcp://activemq:1883"; // activemq:61613 stomp://0.0.0.0:61613
    public static final String CLIENT_ID = "JAVACLIENT"; // activemq:61613 stomp://0.0.0.0:61613
    public static final int BROKER_PORT = 61613;
    public static final String BROKER_USER_NAME = "admin";
    public static final String BROKER_PASSWORD = "admin";

    // ActiveMQ bus
    public static final String QUEUE_ADDEVENT = "/queue/addEvent";
    public static final String LOG_BUS = "around.Log.New";
}
