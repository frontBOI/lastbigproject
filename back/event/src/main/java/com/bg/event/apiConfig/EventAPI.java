package com.bg.event.apiConfig;

import com.bg.event.controller.EventController;
import com.bg.event.model.EventModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Time;
import java.util.*;

public class EventAPI {
    private static final EventAPI instance = new EventAPI();
    public String code;
    public String token;
    private Date expirationTokenDate;

    private EventAPI() {
        System.out.println("Instanciation du Event API");
        try {
            this.code = this.requestingAuth();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.token = this.requestAccessToken(this.code);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static final EventAPI getInstance()
    {
        return instance;
    }

    private String requestingAuth() throws IOException {
        String redirectUri = getFinalURL("https://secure.meetup.com/oauth2/authorize?client_id=qr7t5kevqsrn8u89016mho9glg&redirect_uri=http://localhost/&response_type=anonymous_code");
        return redirectUri.split("code=")[1];

    }

    public String requestAccessToken(String code) throws IOException, InterruptedException, ParseException {
        System.out.println("Request Access Token");
        if (this.isTheTokenExpired()) {
            var values = new HashMap<String, String>() {{
//            put("client_id", "kt5r2eap027qqmqrfmsis6h8pr");
//            put ("client_secret", "komfu3tt324nrbnoo7rmi94r1i&");
//            put ("client_secret", "komfu3tt324nrbnoo7rmi94r1i&");
            }};

            var objectMapper = new ObjectMapper();
            String requestBody = objectMapper
                    .writeValueAsString(values);

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://secure.meetup.com/oauth2/access?" +
                            "client_id=qr7t5kevqsrn8u89016mho9glg&" +
                            "client_secret=vlu7df6ph1vncb6kema2h6oncr&" +
                            "grant_type=anonymous_code&" +
                            "redirect_uri=http://localhost/&code=" + code))
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();

            HttpResponse<String> response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
            Object body = new JSONParser().parse(response.body());
            JSONObject bodyJson = (JSONObject) body;
            this.expirationTokenDate = new Date();
            this.expirationTokenDate.setTime(this.expirationTokenDate.getTime() + 3600000);
            return bodyJson.getAsString("access_token");
        }
        return this.token;
    }

    private String getFinalURL(String url) throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        con.setInstanceFollowRedirects(false);
        con.connect();
        con.getInputStream();

        if (con.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || con.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
            String redirectUrl = con.getHeaderField("Location");
            return redirectUrl;
        }
        return url;
    }

    private Boolean isTheTokenExpired() {
        Date now = new Date();
        if (this.expirationTokenDate != null) {
            return !now.before(this.expirationTokenDate);
        }
        return true;
    }

    public List<EventModel> getUpcomingEventsAPI(String lat, String lon) {
        var values = new HashMap<String, String>() {{
            put("lat", lat);
            put ("lon", lon);
            put ("page", "50");
            put ("radius", "smart");
        }};

        var objectMapper = new ObjectMapper();
        try {
            String requestBody = objectMapper
                    .writeValueAsString(values);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = null;
        try {
            request = HttpRequest.newBuilder()
                    .uri(URI.create("https://api.meetup.com/find/upcoming_events?"+
                            "lon="+lon+
                            "&page=50"+
                            "&radius=smart"+
                            "&lat="+lat))
                    .header("Authorization","Bearer "+this.requestAccessToken(this.requestingAuth()))
    //                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        HttpResponse<String> response = null;
        try {
            response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (response.statusCode() != 200){
            return new ArrayList<>();
        }
        Object body = null;
        try {
            body = new JSONParser().parse(response.body());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONObject bodyJson = (JSONObject) body;
        try {
            return this.parseEventModel(bodyJson);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private List<EventModel> parseEventModel(JSONObject bodyJson) throws ParseException {
        List<EventModel> eventsModelList = new ArrayList<>();
        JSONObject city = (JSONObject) bodyJson.get("city");
        ArrayList events = (ArrayList) bodyJson.get("events");
        for (int i = 0; i < events.size(); i++) {
            EventModel eventModel = new EventModel();
            Object group = new JSONParser().parse(((JSONObject) events.get(i)).getAsString("group"));
            JSONObject groupJson = (JSONObject) group;

            eventModel.setDuration(this.parseWithDefault(((JSONObject) events.get(i)).getAsString("duration"),0));
            eventModel.setMeetupId(((JSONObject) events.get(i)).getAsString("id"));
            eventModel.setName(((JSONObject) events.get(i)).getAsString("name"));
            eventModel.setLocalDate(java.sql.Date.valueOf(((JSONObject) events.get(i)).getAsString("local_date")));
            eventModel.setLocalTime(Time.valueOf(((JSONObject) events.get(i)).getAsString("local_time")+":00"));
            eventModel.setUtcOffset(Integer.valueOf(((JSONObject) events.get(i)).getAsString("utc_offset")));
            eventModel.setEventLink(((JSONObject) events.get(i)).getAsString("link"));
            eventModel.setDescription(((JSONObject) events.get(i)).getAsString("description"));
            if (((JSONObject) events.get(i)).getAsString("visibility") == "public") {
                eventModel.setVisibility(true);
            } else {
                eventModel.setVisibility(false);
            }
            //GROUP
            eventModel.setOrganisateurMeetupId(Integer.valueOf(groupJson.getAsString("id")));

            //Venue if not hidden by orga
            String venue = ((JSONObject) events.get(i)).getAsString("venue");
            if (venue != null){
                JSONObject venueJson = (JSONObject) new JSONParser().parse(venue);
                eventModel.setVenueName(venueJson.getAsString("name"));
                eventModel.setVenueAddressString(venueJson.getAsString("address_1"));
                eventModel.setVenueCity(venueJson.getAsString("city"));
                eventModel.setVenueLat(venueJson.getAsString("lat"));
                eventModel.setVenueLon(venueJson.getAsString("lon"));
            }
            eventsModelList.add(eventModel);
        }
        return eventsModelList;
    }

    private int parseWithDefault(String number, int defaultVal) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return defaultVal;
        }
    }
}
