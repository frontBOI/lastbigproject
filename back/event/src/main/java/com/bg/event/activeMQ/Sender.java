package com.bg.event.activeMQ;

import com.bg.event.ActiveMQ;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sender {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(Sender.class);

    private MqttClient client;
    private MqttConnectOptions connOpts;

    public Sender() {
        try {
            MemoryPersistence persistence = new MemoryPersistence();
            this.client = new MqttClient(ActiveMQ.BROKER_URL,ActiveMQ.CLIENT_ID,persistence);
            this.connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
        } catch (MqttException e){
            e.printStackTrace();
            this.client = null;
        }
    }

    public LogMessage send(String eventJSON) {
        //LOGGER.info("sending message='{}'",message.toString());
        //final LogMessage event = new LogMessage(ActiveMQ.NEW_EVENT_BUS, message.toString());
        //jmsTemplate.convertAndSend(ActiveMQ.NEW_EVENT_BUS, event.getContent());
        //System.out.println("Test envoie");
        //System.out.println(eventJSON);
        try {/*
            StompConnection connection = new StompConnection();
            connection.open(ActiveMQ.BROKER_URL, ActiveMQ.BROKER_PORT);
            connection.connect(ActiveMQ.BROKER_USER_NAME, ActiveMQ.BROKER_PASSWORD);
            connection.send(ActiveMQ.QUEUE_ADDEVENT, eventJSON);
            //System.out.println("Message envoyé normalement");*/
            if(this.client!=null){
                this.client.connect(connOpts);
                MqttMessage message = new MqttMessage(eventJSON.getBytes());
                message.setQos(2);
                this.client.publish(ActiveMQ.QUEUE_ADDEVENT,message);
                this.client.disconnect();
            } else {
                System.out.println("Cannot send message through MQTT, MQTT client has a problem");
            }

        }
        catch (MqttException e){
            LOGGER.error("ERROR");
            e.printStackTrace();
            //System.out.println("ERROR");
        }
        LogMessage event = new LogMessage(ActiveMQ.QUEUE_ADDEVENT, eventJSON);
        return event;
    }




//    private static final Logger log = LoggerFactory.getLogger(
//            Sender.class);
//
//    @Autowired
//    private JmsTemplate jmsSender;
//
//    @MessageMapping("/hello")
//    @SendTo("/topic/events")
//    public LogMessage send(EventAddRequest message) {
//        log.info("sending message='{}'",message.toString());
//        final LogMessage event = new LogMessage(ActiveMQ.NEW_EVENT_BUS, message.toString());
//        jmsSender.convertAndSend(ActiveMQ.NEW_EVENT_BUS, event.getContent());
//        return event;
//    }

//  @JmsListener(destination = "greetings")
//  @SendTo("/topic/greetings")
//  public Greeting greeting(@Valid Greeting greeting) {
//    log.info("Received greeting {}", greeting.getContent());
//    simpSender.convertAndSend("/topic/greetings", greeting);
//    return greeting;
//  }

}
