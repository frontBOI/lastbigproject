package com.bg.event.service;

import org.apache.log4j.*;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.hibernate.service.spi.ServiceException;

import java.io.*;

/**
 *  Classe permettant des appels de WS
 */
public class HttpRequestClient {
    private static final Logger logger = LogManager.getLogger(HttpRequestClient.class);

    public HttpRequestClient()
    {

    }
    /**
     * Envoie une requête GET
     *
     * @param URL adresse destinataire
     * @return le flux JSON retourné
     * @throws ServiceException Exception
     */
    public String sendGETrequest(String URL) throws ServiceException {
        logger.trace("Sending GET request to : " + URL);
        GetMethod get = null;
        HttpClient client = new HttpClient();
        try {
            get = new GetMethod(URL);
            // Les Web Services ne renvoient que du contenu sous forme de flux JSON
            get.setRequestHeader("Accept", "text/json");
            client.executeMethod(get);
            InputStream in = get.getResponseBodyAsStream();
            String response = "";
            try {
                response = readInputStream(in);
                logger.info("Response is : " + response);
            } catch (IOException e) {
                logger.error(e);
                throw new ServiceException("Une erreur est survenue lors de la réception de la réponse.", e);
            }
            return response;

        } catch (HttpException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de l'envoi de la requête.", e);
        } catch (IOException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de l'envoi de la requête.", e);
        } finally {
            if (get != null)
                get.releaseConnection();
        }
    }


    /**
     * Envoie une requête POST
     *
     * @param URL     adresse destinataire
     * @param message flux JSON à envoyer avec la requête
     * @return flux JSON retourné
     * @throws ServiceException
     */
    public String sendPOSTrequest(String URL, String message) throws ServiceException {
        logger.info("Sending POST request to : " + URL + "\r\nwith data : " + message);

        PostMethod post = null;
        HttpClient client = new HttpClient();
        InputStream in = null;
        try {
            post = new PostMethod(URL);
            // Les Web Services ne renvoient que du contenu sous forme de flux JSON
            post.setRequestHeader("Accept", "*/*");
            post.setRequestHeader("Content-Type", "application/json");
            RequestEntity entity = new StringRequestEntity(message, "text/json", "utf-8");
            post.setRequestEntity(entity);
            client.executeMethod(post);
            in = post.getResponseBodyAsStream();
            String response = "";
            try {
                response = readInputStream(in);
                logger.info("Response is : " + response);
            } catch (IOException e) {
                logger.error(e);
                throw new ServiceException("Une erreur est survenue lors de la réception de la réponse.", e);
            }
            return response;

        } catch (UnsupportedEncodingException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de la construction de la requête.", e);
        } catch (HttpException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de l'envoi de la requête.", e);
        } catch (IOException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de l'envoi de la requête.", e);
        } finally {
            if (post != null)
                post.releaseConnection();
        }
    }

    public String sendPOSTrequest(String URL, String param, String value) throws ServiceException {
        logger.info("Sending POST request to : " + URL + "\r\nwith data : " + param + "and value : \n" + value);

        PostMethod post = null;
        HttpClient client = new HttpClient();
        InputStream in = null;
        try {
            post = new PostMethod(URL);
            // Les Web Services ne renvoient que du contenu sous forme de flux JSON
            post.addParameter(param,value);
            client.executeMethod(post);
            in = post.getResponseBodyAsStream();
            String response = "";
            try {
                response = readInputStream(in);
                logger.info("Response is : " + response);
            } catch (IOException e) {
                logger.error(e);
                throw new ServiceException("Une erreur est survenue lors de la réception de la réponse.", e);
            }
            return response;

        } catch (UnsupportedEncodingException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de la construction de la requête.", e);
        } catch (HttpException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de l'envoi de la requête.", e);
        } catch (IOException e) {
            logger.error(e);
            throw new ServiceException("Une erreur est survenue lors de l'envoi de la requête.", e);
        } finally {
            if (post != null)
                post.releaseConnection();
        }
    }


    /**
     * Convertit le flux de réponse en chaine de caractères
     *
     * @param in Flux de réponse
     * @return réponse sous forme de chaine de caractères
     * @throws IOException
     */
    private String readInputStream(InputStream in) throws IOException {
        StringBuilder buffer = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String read = br.readLine();
        while (read != null) {
            buffer.append(read);
            read = br.readLine();
        }

        return buffer.toString();
    }
}
