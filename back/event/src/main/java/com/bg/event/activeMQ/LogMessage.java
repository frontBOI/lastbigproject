package com.bg.event.activeMQ;

import java.io.Serializable;

public class LogMessage implements Serializable {
    private final String bus;
    private final String content;

    public LogMessage(String bus, String content) {
        this.bus = bus;
        this.content = content;
    }

    public String getBus() {
        return bus;
    }

    public String getContent() {
        return content;
    }
}
