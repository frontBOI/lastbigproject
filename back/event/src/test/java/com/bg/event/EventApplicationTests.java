package com.bg.event;

import com.bg.event.activeMQ.EventAddRequest;
import com.bg.event.activeMQ.Sender;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.bg.event.model.EventModel;
import com.bg.event.service.HttpRequestClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

//@SpringBootTest
//class EventApplicationTests {
//
//	@Test
//	void contextLoads() {
//	}
//
//}

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventApplicationTests {

    @Autowired
    private Sender sender;

//    @Test
//    public void testReceive() throws Exception {
//        EventAddRequest event = new EventAddRequest();
//        event.setDescription("TEST NOA");
//        event.setName("TEST NOA");
//        sender.send(event);
//
//        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
//        assertThat(receiver.getLatch().getCount()).isEqualTo(0);
//    }

    private final String baseUrlMSEvent = "http://event-service:8080/";
    private static Logger logger = LoggerFactory.getLogger(EventApplicationTests.class);

    @org.junit.jupiter.api.Test
    public void isAliveTest() throws Exception{
        RestTemplate restTemplate = new RestTemplate();
        HttpStatus status = restTemplate.getForObject("http://localhost:8080/",HttpStatus.class);
        Assert.assertEquals("IS ALIVE", HttpStatus.OK, status);
    }
//    @org.junit.jupiter.api.Test
//    public void updateEventTest() throws Exception{
//
//        EventModel event = new EventModel();
//
//        String body = user.toString();
//        Gson g = new Gson();
//        logger.debug("tags = " + g.toJson(body));
//        HttpRequestClient httpClient = new HttpRequestClient();
//        String response = httpClient.sendPOSTrequest("http://localhost:5000/predict","strToPredict",g.toJson(body));
//        Assert.assertEquals("UPDATE EVENT OK",true, (response.length()>0));
//
//    }

    @org.junit.jupiter.api.Test
    public void getEventsFromId() throws Exception{
        HttpRequestClient httpClient = new HttpRequestClient();
        List<Integer> events = new ArrayList<>();
        events.add(1);
        events.add(2);
        String ev = events.toString();
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<Integer>> entity = new HttpEntity<List<Integer>>(events, headers);
        EventModel[] event = restTemplate.postForObject("http://localhost/api/event/events/",entity,EventModel[].class);
        List<EventModel>  listEvent = Arrays.asList(event);
        if(event == null)
        {
            logger.info("Aucun evenement reçu");
        }
        logger.debug("event[0] =" + event[0].toString());
        Assert.assertEquals("GET EVENT FROM ID OK",true, (event.length>0));

    }
}