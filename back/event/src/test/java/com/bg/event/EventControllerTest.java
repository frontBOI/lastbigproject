package com.bg.event;

import com.bg.event.activeMQ.Sender;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import com.bg.event.controller.EventController;
import com.bg.event.model.EventModel;
import com.bg.event.repository.EventRepository;
import com.bg.event.service.EventService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(controllers = {EventController.class, EventService.class})
@ExtendWith(SpringExtension.class)
public class EventControllerTest {
    private final String baseUrlMSEvent = "http://event-service:8080/";
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(EventControllerTest.class);
    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private EventController controller;

    @Mock
    private EventRepository eventRepository;

    private final List<EventModel> events = new ArrayList<>();

    @Before
    public void init() {
        logger.debug("Initialisation...");
        //region json
        String jsonEvent1 = "    {\n" +
                "        \"id\": 142,\n" +
                "        \"meetupId\": \"275562802\",\n" +
                "        \"name\": \"Faciliter le travail à distance avec Windows Virtual Desktop\",\n" +
                "        \"duration\": 3600000,\n" +
                "        \"localDate\": \"2021-01-27T00:00:00.000+00:00\",\n" +
                "        \"localTime\": \"12:30:00\",\n" +
                "        \"utcOffset\": 3600000,\n" +
                "        \"venueName\": \"Online event\",\n" +
                "        \"venueLat\": null,\n" +
                "        \"venueLon\": null,\n" +
                "        \"venueAddressString\": null,\n" +
                "        \"organisateurMeetupId\": 32322977,\n" +
                "        \"organisateurLocalId\": null,\n" +
                "        \"eventLink\": \"https://www.meetup.com/Azure-Group-Lyon/events/275562802/\",\n" +
                "        \"description\": \"<p>### MEETUP AZURE NANTES EN VISIO ###</p>\",\n" +
                "        \"visibility\": false,\n" +
                "        \"ticketing\": null,\n" +
                "        \"categorie\": []\n" +
                "    },";

        String jsonEvent2 = "    {\n" +
                "        \"id\": 143,\n" +
                "        \"meetupId\": \"lqjgrqycccbbc\",\n" +
                "        \"name\": \"Le dispositif Pinel 2019 en détail\",\n" +
                "        \"duration\": 7200000,\n" +
                "        \"localDate\": \"2021-01-20T00:00:00.000+00:00\",\n" +
                "        \"localTime\": \"19:00:00\",\n" +
                "        \"utcOffset\": 3600000,\n" +
                "        \"venueName\": null,\n" +
                "        \"venueLat\": null,\n" +
                "        \"venueLon\": null,\n" +
                "        \"venueAddressString\": null,\n" +
                "        \"organisateurMeetupId\": 22201510,\n" +
                "        \"organisateurLocalId\": null,\n" +
                "        \"eventLink\": \"https://www.meetup.com/Meetup-independance-financiere/events/lqjgrqycccbbc/\",\n" +
                "        \"description\": \"<p>Présentation du dispositif Pinel pour acquérir un logment en réduisant ses impôts</p> \",\n" +
                "        \"visibility\": false,\n" +
                "        \"ticketing\": null,\n" +
                "        \"categorie\": []\n" +
                "    }";
        //endregion
        ObjectMapper mapper = new ObjectMapper();
        EventModel event1 = null;
        try {
            event1 = mapper.readValue(jsonEvent1, EventModel.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        EventModel event2 = null;
        try {
            event2 = mapper.readValue(jsonEvent2, EventModel.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        events.add(event1);
        events.add(event2);
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public void isAliveTest() throws Exception{
        mvc.perform(MockMvcRequestBuilders
                .get("/").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
//    @Test
//    public void getEvent() throws Exception{
//        //Ajout event 1 dans la bdd
//
//        when(eventRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(this.events.get(1)));
//        EventModel event = controller.e(1L);
//
//        verify(userRepository).findOne(1l);
//
//        assertEquals(1l, user.getId().longValue());
//    }
}