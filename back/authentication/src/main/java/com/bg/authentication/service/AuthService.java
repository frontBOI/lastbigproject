package com.bg.authentication.service;

import com.bg.authentication.repository.AuthRepository;
import com.bg.commonuser.model.UserModelCommon;

import com.bg.authentication.model.AuthModel;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.Optional;

@Service
public class AuthService {

    @Autowired
    private AuthRepository authRepository;

    private final String baseUrlMSUser = "http://user-service:8080/";
    private final RestTemplate restTemplate;
    public AuthService() {
        restTemplate = new RestTemplate();
    }

    // vérifie que le user existe et return un token si oui et que le token n'existe pas
    public JSONObject login(String login, String pwd) {
        //verification du user
        UserModelCommon user = restTemplate.getForObject(baseUrlMSUser + "getByLoginAndPwd/" + login + "/" + pwd, UserModelCommon.class);
        System.out.println("[DEBUG] user received : " + user.toString());

        JSONObject obj = new JSONObject();

        if (user == null) {
            System.out.println("[DEBUG] : no user retrieved" );
            obj.put("token", "");
            return obj;
        }

        //verification qu'une session n'existe pas
        AuthModel session = null;
        Optional<AuthModel> isSessionExisting = authRepository.findByUserId(user.getId());
        if (isSessionExisting.isPresent()) {
            System.out.println("[DEBUG] session retrieved : " + isSessionExisting.get().toString());
            //verification de la session existante
            try {
                if (!isSessionExpired(isSessionExisting.get())) {
                    obj.put("token", isSessionExisting.get().getToken());
                    obj.put("user", user);
                    return obj;
                }
            }
            catch (Exception e) {
                System.out.println("[ERROR] : " + e.toString());
                obj.put("token", "");
                return obj;
            }
            authRepository.delete(isSessionExisting.get());
            System.out.println("[DEBUG] session existing deleted ");
        }

        //creation du token
        String token = createToken(user);
        obj.put("token", token);
        obj.put("user", user);
        return obj;
    }

    public Boolean logout(String id, String token) {
        Optional<AuthModel> session = authRepository.findByUserId(Integer.parseInt(id));
        if (session.isPresent()){
            System.out.println("[DEBUG] token db : " + session.get().getToken().toString() + " token received : " + token);
            if (session.get().getToken().equals(token)) {
                System.out.println("[DEBUG] session existing and same token");
                try {
                    authRepository.delete(session.get());
                    return true;
                }
                catch (Exception e) {
                    System.out.println("[ERROR] : " + e.toString());
                }

            }
            System.out.println("[DEBUG] session existing but not same token");
        }
        System.out.println("[DEBUG] session not existing");
        return false;
    }

    //create session and return token
    private String createToken(UserModelCommon user) {
        AuthModel session = new AuthModel(user.getId());
        authRepository.save(session);
        System.out.println("[DEBUG] following session has been created and saved : " + session.toString());
        return session.getToken();
    }



    //return true if token exists
    public Boolean checkToken(String token) {
        Optional<AuthModel> session = authRepository.findSessionByToken(token);
        System.out.println("[DEBUG] session retrieved : " + session.toString());
        if (session.isEmpty()) {
            return false;
        }
        if (isSessionExpired(session.get())) {
            return false;
        }
        return true;
    }

    //check if token is still valid
    private Boolean isSessionExpired(AuthModel session) {
        Date currentDate = new Date();
        if (session.getTokenExpiryDate().after(currentDate)) {
            return false;
        }
        return true;
    }

}
