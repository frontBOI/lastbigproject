package com.bg.authentication.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Entity
public class AuthModel {

    private static final long serialVersionUID = 2733795832476568049L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int length = 72; //durée du token (heure)
    private String token;
    private Date tokenCreationDate;
    private Date tokenExpiryDate;
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int id) {
        this.id = id;
    }

    public AuthModel(){

    }

    public AuthModel(int userId)
    {
        this.token = this.generateRandomToken();
        this.tokenCreationDate = new Date();
        this.tokenExpiryDate = this.addHoursToJavaUtilDate(this.tokenCreationDate,length);
        this.userId = userId;

    }

    public Date getTokenCreationDate()
    {
        return tokenCreationDate;
    }
    public Date getTokenExpiryDate()
    {
        return tokenExpiryDate;
    }

    public String getToken()
    {
        return token;
    }


    private String generateRandomToken() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        //System.out.println("Token:"+generatedString);
        return generatedString;
    }

    public Date addHoursToJavaUtilDate(Date date, int hours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        return calendar.getTime();
    }


}
