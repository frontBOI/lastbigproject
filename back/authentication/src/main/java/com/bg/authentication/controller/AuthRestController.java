package com.bg.authentication.controller;

//import model.UserModelCommon;

import com.bg.authentication.service.AuthService;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
@RestController
public class AuthRestController {

    @Autowired
    private AuthService authService;

    @RequestMapping("/status")
    private HttpStatus getStatus() {
        return HttpStatus.OK;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    private ResponseEntity<Object> login(@RequestParam("login") String login, @RequestParam("pwd") String pwd) {
        System.out.println("[DEBUG] login received : " + login.toString() + " & " + pwd.toString());

        JSONObject obj = new JSONObject();


        if (( login.length() == 0 ) || ( pwd.length() == 0 )) {
            obj.put("token", "");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(obj);
        }
        JSONObject session = authService.login(login,pwd);
        //obj.put("token", token);

        if (session.get("token").toString().length() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(session);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(session);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/checkToken")
    public ResponseEntity<Object> checkToken(@CookieValue(value = "token") String token) {
        System.out.println("[DEBUG] cookie received : " + token);

        if(token.length() > 0) {
            if(authService.checkToken(token)) {
                return ResponseEntity.status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("");
            }
            else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("");
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .contentType(MediaType.APPLICATION_JSON)
                .body("");
    }

    @RequestMapping(method = RequestMethod.POST, value = "/logout/{id}")
    private ResponseEntity<Object> logout(@PathVariable String id,@CookieValue(value = "token") String token) {
        System.out.println("[DEBUG] logout id received : " + id.toString() );
        JSONObject obj = new JSONObject();
        if (authService.logout(id, token)) {
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("");
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body("");
    }

}
