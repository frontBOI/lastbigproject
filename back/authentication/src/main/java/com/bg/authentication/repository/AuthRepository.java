package com.bg.authentication.repository;

import com.bg.authentication.model.AuthModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends CrudRepository<AuthModel, Integer> {
    Optional<AuthModel> findByUserId(int userId);
    Optional<AuthModel> findSessionByToken(String token);
}
