package com.bg.tickettransaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TickettransactionApplication {

    public static void main(String[] args) {
        SpringApplication.run(TickettransactionApplication.class, args);
    }

}
