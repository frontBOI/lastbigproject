package model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserModelCommon implements Serializable {

    private Integer id;
    private String pseudonym;
    private Integer age;
    private String login;
    private String pwd;
    private String lastName;
    private String firstName;
    private String email;

    public UserModelCommon() {}

    public UserModelCommon(Integer id, String pseudonym, Integer age, String login, String pwd, String lastName, String firstName, String email, String address, ArrayList<String> tags) {
        this.id = id;
        this.pseudonym = pseudonym;
        this.age = age;
        this.login = login;
        this.pwd = pwd;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.address = address;
        this.tags = tags;
    }

    private String address;
    private ArrayList<String> tags;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
}
