package com.bg.eventinviting.repository;

import com.bg.eventinviting.model.EventInvitingModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventInvitingRepository extends CrudRepository<EventInvitingModel, Integer> {
    @Query("SELECT e FROM EventInvitingModel e WHERE e.userDestinatedId = ?1 ")
    List<EventInvitingModel> findByUserId(Integer userId);

    @Query("SELECT i FROM EventInvitingModel i WHERE i.id = ?1 ")
    EventInvitingModel findbyInvitationId(Integer invitationId);

}
