package com.bg.eventinviting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventInvitingApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventInvitingApplication.class, args);
    }

}
