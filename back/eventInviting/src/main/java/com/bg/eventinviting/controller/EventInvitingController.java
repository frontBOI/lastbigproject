package com.bg.eventinviting.controller;

import com.bg.eventinviting.model.EventInvitingModel;
import com.bg.eventinviting.service.EventInvitingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EventInvitingController {

    @Autowired
    private EventInvitingService eventInvitingService;
    private static Logger logger = Logger.getLogger(EventInvitingController.class);


    @RequestMapping("/")
    private String isAlive()
    {
        logger.info("IS ALIVE!");
        return "Je suis la";
    }

    //get invitation
    @RequestMapping("/inviteToEvent/{id}")
    private List<EventInvitingModel> getEventInvitationByUser(@PathVariable String id)
    {
        return eventInvitingService.getEventInvitationByUser(Integer.valueOf(id));
    }

    //add invitation
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    private ResponseEntity<Object> addEventInvitationByUser(@RequestBody EventInvitingModel invitation)
    {
        EventInvitingModel eventInvitation = eventInvitingService.addEventInvitationByUserBis(invitation);
        if(eventInvitation!=null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(eventInvitation);
        }
        String msgError =  "{\"message\" : \"Evènement déjà présent dans la bdd\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);

        //pas de convert body du coup
        //return ResponseEntity.status(eventInvitingService.addEventInvitationByUser(invitation));
    }

    //delete invitation
    @RequestMapping(method = RequestMethod.DELETE, value = "/inviteToEvent/{id}")
    private ResponseEntity<Object> getEventInvitationByUser(@PathVariable Integer id)
    {
        List<EventInvitingModel> rep = eventInvitingService.getEventInvitationByUser(Integer.valueOf(id));
        if(rep!=null)
        {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(rep);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

    }

    @RequestMapping(method=RequestMethod.DELETE,value="/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable String id) {
        boolean ret = eventInvitingService.deleteEventInvitation(Integer.valueOf(id));

        if (ret) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ret);
        }else{
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(ret);
        }
    }

    //update invitation
    @RequestMapping(method = RequestMethod.PUT, value = "/update/{id}")
    public ResponseEntity<Object> updateEvent(@RequestBody EventInvitingModel invitation, @PathVariable String id) {
        invitation.setId(Integer.valueOf(id));
        EventInvitingModel ev = eventInvitingService.updateEventInvitation(invitation);
        if(ev!=null) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(ev);
        }
        String msgError =  "{\"message\" : \"L'évènement n'a pas pu être trouvé.\" }";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(msgError);

    }

    //lien avec ms event


    //lien avec ms user

}
