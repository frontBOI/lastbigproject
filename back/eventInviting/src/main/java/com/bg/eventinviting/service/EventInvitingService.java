package com.bg.eventinviting.service;

import com.bg.eventinviting.model.EventInvitingModel;
import com.bg.eventinviting.model.EventModel;
import com.bg.eventinviting.model.UserModel;
import com.bg.eventinviting.repository.EventInvitingRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


@Service
public class EventInvitingService {

    @Autowired
    private EventInvitingRepository eventInvitingRepository;
    private final String baseUrlMSUser ="http://user-service:8080/";
    private final String baseUrlMSEvent = "http://event-service:8080/";
    private final RestTemplate restTemplate;

    private static Logger logger = Logger.getLogger(EventInvitingService.class);
    public EventInvitingService()
    {
        restTemplate = new RestTemplate();
    }

    //get invitation
    public List<EventInvitingModel> getEventInvitationByUser(Integer userId) {
        logger.info("Recuperation de la liste des invitations pour un utilisateur a chaque evenement...");
        List<EventInvitingModel> eventInvitationList = new ArrayList<>();
        eventInvitingRepository.findByUserId(userId).forEach(eventInvitationList::add);
        logger.info("Nombre d'invitation total à des evenements de ce personnage :" + eventInvitationList.size());

        return eventInvitationList;

    }

    //creation d'une invitation a event from user 1 to user 2
    public String addEventInvitationByUser(EventInvitingModel invitation) {
        logger.info("Creation d'une invitation a un evenement d'un utilisateur 1 : " + invitation.getUserDestinatedId() + "a un utilisateur 2 : " + invitation.getUserDestinatedId() + " associe a l'evenement " + invitation.getEventId() + "...");

        if(isUserExist(invitation.getUserOriginId())!=null && isUserExist(invitation.getUserDestinatedId())!=null && isEventExist(invitation.getEventId())!=null){
            eventInvitingRepository.save(invitation);
            return "cest ok mon khey";

        }

        return "ton invitation de " + invitation.getUserOriginId() + "a " + invitation.getUserDestinatedId() + " pour l'evenement " + invitation.getEventId() + "n'a pas ete cree en BDD bg...";
    }

    //creation d'une invitation a event from user 1 to user 2 bis
    public EventInvitingModel addEventInvitationByUserBis(EventInvitingModel invitation) {
        if (invitation.getId() != null) {
            List<EventInvitingModel> ev = getEventInvitationByUser(invitation.getId());
            if (ev.isEmpty()){
                return null;
            }
        }
        eventInvitingRepository.save(invitation);
        return invitation;
    }

    //delete invitation
    public boolean deleteEventInvitation(Integer id) {
        EventInvitingModel invitation = eventInvitingRepository.findbyInvitationId(id);
        if(invitation==null){
            return false;
        }else {
            logger.info("Suppression de l'invitation d'id" + invitation.getId() + " dans la BDD...");
            eventInvitingRepository.delete(invitation);
            return true;
        }
    }

    //update invitation
    public String updateEventInvitationByUserAndEvent(Integer invitationId) {
        EventInvitingModel invitation = new EventInvitingModel();
        invitation = eventInvitingRepository.findbyInvitationId(invitationId);

        logger.info("Modification d'une invitation a un evenement d'un utilisateur 1 : " + invitation.getUserDestinatedId() + "a un utilisateur 2 : " + invitation.getUserDestinatedId() + " associe a l'evenement " + invitation.getEventId() + "...");

        if(isUserExist(invitation.getUserOriginId())!=null && isUserExist(invitation.getUserDestinatedId())!=null && isEventExist(invitation.getEventId())!=null){
            eventInvitingRepository.save(invitation);
            return "cest ok mon kho";

        }else{
            return "cest pas ok mon kho, update non realise sur l'invitation numero" + invitation.getId();

        }

    }

    public EventInvitingModel updateEventInvitation(EventInvitingModel invitation) {
        List<EventInvitingModel> inv = getEventInvitationByUser(invitation.getUserDestinatedId());
        if(inv.isEmpty())
        {
            return null;
        }
        eventInvitingRepository.save(inv.get(0));
        return inv.get(0);

    }

    public UserModel isUserExist(Integer id) {
        try {
            UserModel user = restTemplate.getForObject(baseUrlMSUser + "/" + id,UserModel.class);
            if(user==null)
            {
                logger.warn("L'utilisateur " + id + "n'existe pas");
                return null;
            }
            else
            {
                logger.debug("L'utilisateur " + id + "a été trouvé (login=" + user.getPseudonym() + ")");
                return user;
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return null;
    }

    public EventModel isEventExist(Integer id) {
        try
        {
            EventModel event = restTemplate.getForObject(baseUrlMSEvent + "/" +id,EventModel.class);
            if(event==null)
            {
                logger.warn("L'event " + id + "n'existe pas");
                return null;
            }
            else
            {
                logger.debug("L'event " + id + "a été trouvé (titre=" + event.getName() + ")");
                return event;
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return null;
    }

    //lien avec ms event

}
