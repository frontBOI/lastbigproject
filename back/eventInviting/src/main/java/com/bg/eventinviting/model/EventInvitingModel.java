package com.bg.eventinviting.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;

@Entity
public class EventInvitingModel {

    @Id
    private Integer id;
    private String pseudonym;
    private Integer userOriginId;
    private Integer userDestinatedId;
    private Integer eventId;

    public EventInvitingModel(Integer id, String pseudonym, Integer userOriginId, Integer userDestinatedId, Integer eventId) {
        this.id = id;
        this.pseudonym = pseudonym;
        this.userOriginId = userOriginId;
        this.userDestinatedId = userDestinatedId;
        this.eventId = eventId;
    }

    public EventInvitingModel()
    {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public Integer getUserOriginId() {
        return userOriginId;
    }

    public void setUserOriginId(Integer userOriginId) {
        this.userOriginId = userOriginId;
    }

    public Integer getUserDestinatedId() {
        return userDestinatedId;
    }

    public void setUserDestinatedId(Integer userDestinatedId) {
        this.userDestinatedId = userDestinatedId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }




}
