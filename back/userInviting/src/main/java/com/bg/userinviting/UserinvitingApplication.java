package com.bg.userinviting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserinvitingApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserinvitingApplication.class, args);
    }

}
