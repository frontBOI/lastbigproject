package com.bg.userinviting.repository;

import com.bg.userinviting.model.UserInvitingModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInvitingRepository extends CrudRepository<UserInvitingModel, Integer> {
    @Query("SELECT e FROM UserInvitingModel e WHERE e.userDestinatedId = ?1 ")
    List<UserInvitingModel> findByUserId(Integer userId);

    @Query("SELECT i FROM UserInvitingModel i WHERE i.id = ?1 ")
    UserInvitingModel findbyInvitationId(Integer invitationId);

}
