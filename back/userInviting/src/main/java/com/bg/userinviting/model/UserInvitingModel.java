package com.bg.userinviting.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class UserInvitingModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer userOriginId;
    private Integer userDestinatedId;
    public enum Status { PENDING, ACCEPTED, REFUSED }
    private Status status;

    public UserInvitingModel(Integer id, Integer userOriginId, Integer userDestinatedId, Status status) {
        this.id = id;
        this.userOriginId = userOriginId;
        this.userDestinatedId = userDestinatedId;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserOriginId() {
        return userOriginId;
    }

    public void setUserOriginId(Integer userOriginId) {
        this.userOriginId = userOriginId;
    }

    public Integer getUserDestinatedId() {
        return userDestinatedId;
    }

    public void setUserDestinatedId(Integer userDestinatedId) {
        this.userDestinatedId = userDestinatedId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
