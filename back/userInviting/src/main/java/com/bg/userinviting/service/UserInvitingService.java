package com.bg.userinviting.service;

import com.bg.userinviting.repository.UserInvitingRepository;
import com.bg.userinviting.model.UserInvitingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserInvitingService {

    @Autowired
    private UserInvitingRepository userInvitingRepository;
    private final String baseUrlMSUser ="http://user-service:8080/";
    private final String baseUrlMSEvent = "http://event-service:8080/";
    private final RestTemplate restTemplate;

    private static Logger logger = Logger.getLogger(UserInvitingService.class);
    public UserInvitingService()
    {
        restTemplate = new RestTemplate();
    }


    //Voir toutes les invitations d'amis qu'on a reçues
    public List<UserInvitingModel> getAllInvitations(Integer destinatedId) {
        logger.info("Recuperation de la liste des invitations d'amis pour un utilisateur donné...");
        List<UserInvitingModel> userInvitationList = new ArrayList<>();
        userInvitingRepository.findByUserId(destinatedId).forEach(userInvitationList::add);
        logger.info("Nombre d'invitation total à des evenements de ce personnage :" + userInvitationList.size());

        return userInvitationList;
    }

    //Accepter l'invitation de qqn
    public Optional<UserInvitingModel> acceptAnInvitation(String id) {
        return userInvitingRepository.findById(Integer.valueOf(id));
    }


    //Refuser l'invitation de qqn
    public boolean getUserByLoginAndPwd(String login, String pwd) {
        return userInvitingRepository.findByLoginAndPwd(login,pwd);
    }

    //Supprimer l'invitation qu'on a envoyé
    public boolean getUserByLogin(String login) {
        return userInvitingRepository.save(login);
    }


}
