package com.bg.userinviting.model;

import com.bg.commonuser.model.UserModelCommon;

public class ConvertUserModel {

    public static UserModel convertToUser(UserModelCommon userModelCommon) {
        if (userModelCommon == null) { return null;}
        UserModel userModel = new UserModel();
        userModel.setId(userModelCommon.getId());
        userModel.setEmail(userModelCommon.getEmail());
        userModel.setLastName(userModelCommon.getLastName());
        userModel.setLogin(userModelCommon.getLogin());
        userModel.setPwd(userModelCommon.getPwd());
        userModel.setFirstName(userModelCommon.getFirstName());
        userModel.setAge(userModelCommon.getAge());
        userModel.setPseudonym(userModelCommon.getPseudonym());
        userModel.setAddress(userModelCommon.getAddress());
        userModel.setTags(userModelCommon.getTags());
        return  userModel;
    }

    public static UserModelCommon convertToCommonUser(UserModel userModel) {
        if (userModel == null) { return null;}
        UserModelCommon userModelCommon = new UserModelCommon();
        userModelCommon.setId(userModel.getId());
        userModelCommon.setEmail(userModel.getEmail());
        userModelCommon.setLastName(userModel.getLastName());
        userModelCommon.setLogin(userModel.getLogin());
        userModelCommon.setPwd(userModel.getPwd());
        userModelCommon.setFirstName(userModel.getFirstName());
        userModelCommon.setAge(userModel.getAge());
        userModelCommon.setPseudonym(userModel.getPseudonym());
        userModelCommon.setAddress(userModel.getAddress());
        userModelCommon.setTags(userModel.getTags());
        return userModelCommon;
    }

    /*public static List<UserModel> convertToCommonUserList(List<UserModelLocal> userModelLocalList) {
        if (userModelLocalList == null) {return null;}
        List<UserModel> userModelList = new ArrayList<>();
        for (UserModelLocal userModelLocal : userModelLocalList) {
            userModelList.add(convertToCommonUser(userModelLocal));
        }
        return  userModelList;
    }*/
}
