package com.bg.user.service;

import com.bg.user.controller.UserRestController;
import com.bg.user.model.UserModel;
import com.bg.user.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    public UserRepository userRepository;
    private static Logger logger = Logger.getLogger(UserService.class);

    //Get all users object
    public List<UserModel> getAllUsers() {
        List<UserModel> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    //Get specific user by id (string id)
    public Optional<UserModel> getUser(Integer id) {
        return userRepository.findById(id);
    }

    //Get specific user by login (string id)
    /*public Optional<UserModel> getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }*/

    // Get specific user by pwd and login
    public Optional<UserModel> getUserByLoginAndPwd(String login, String pwd) {
        return userRepository.findByLoginAndPwd(login,pwd);
    }

    // Get specific user by login
    public Optional<UserModel> getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    //Add user in bdd
    //Add cards for this user
    public Optional<UserModel> addUser(UserModel user) {

        Optional<UserModel> isAlreadyExistUser = userRepository.findByLogin(user.getLogin());
        if (isAlreadyExistUser.isPresent()) {
            logger.error("user already exists : ");
            return null;
        }

        try {
            userRepository.save(user);
        }
        catch (Exception e) {
            logger.error("Bad Request "+ e.toString());
            return null;
        }
        Optional<UserModel> newUser = userRepository.findByLogin(user.getLogin());
        logger.debug(" user created : " + newUser.get().getLogin());
        return newUser;
    }

    //Update user information
    public Optional<UserModel> updateUser(UserModel user) {
        try {
            userRepository.save(user);
        }
        catch (Exception e) {

            return null;
        }

        Optional<UserModel> updatedUser = userRepository.findByLogin(user.getLogin());
        logger.debug("user created : " + updatedUser.toString());
        return updatedUser;

    }

    //Delete user with all his cards
    //Return HTTP status code
    public HttpStatus deleteUser(String id) {
        try {
            userRepository.deleteById(Integer.valueOf(id));
        }catch (Exception e) {
            logger.error(e.toString());
            return HttpStatus.BAD_REQUEST;
        }
        return HttpStatus.OK;
    }

    public Iterable<UserModel> getUsersByIdEvent(List<String> listIdUser) {
        List<Integer> iterableListId = new ArrayList<>();
        for(String s : listIdUser) iterableListId.add(Integer.valueOf(s));
        return userRepository.findAllById(iterableListId);
    }

}
