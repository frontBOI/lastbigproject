package com.bg.user.repository;

import com.bg.user.model.UserModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Integer> {
    Optional<UserModel> findByLoginAndPwd(String login, String pwd);

    Optional<UserModel> findByLogin(String login);

}
