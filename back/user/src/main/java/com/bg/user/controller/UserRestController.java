package com.bg.user.controller;

import com.bg.commonuser.model.UserModelCommon;
import com.bg.user.model.ConvertUserModel;
import com.bg.user.model.UserModel;
import com.bg.user.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "User", description = "API d'User")
public class UserRestController {

    @Autowired
    private UserService userService;
    private static Logger logger = Logger.getLogger(UserRestController.class);

    @Operation(summary = "Simple Hello World", description = "Permet de savoir si le serveur est lancé", tags = { "Hello World" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Erreur (Communication)") })
    @RequestMapping(method = RequestMethod.GET, value= "/status")
    private HttpStatus getStatus() {
        logger.info("User is Alive");
        return HttpStatus.OK;
    }

    @Operation(summary = "Récupérer la liste des users", description = "Retourne une list<UserModel>", tags = { "GetAllUsers" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = UserModel.class))),})
    @RequestMapping(method = RequestMethod.GET, value="/users")
    private List<UserModel> getAllUsers() {
        return  userService.getAllUsers();
    }

    @RequestMapping(method = RequestMethod.GET, value="/{id}")
    private UserModelCommon getUser(@PathVariable Integer id) {
        Optional<UserModel> optionalUser;
        optionalUser = userService.getUser(id);
        return ConvertUserModel.convertToCommonUser(optionalUser.orElse(null));
    }

    @RequestMapping(method= RequestMethod.POST,value="/create")
    public ResponseEntity<Object> addUser(@RequestBody UserModelCommon commonUser) {

        UserModel user = new UserModel();
        try {
            user = ConvertUserModel.convertToUser(commonUser);
            logger.debug("user received : " + user.toString());
        }
        catch (Exception e) {
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Optional<UserModel> answer = userService.addUser(user);

        if (answer.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(answer);
        }

        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(answer);
    }

    @RequestMapping(method=RequestMethod.PUT,value="/update/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModelCommon commonUser,@PathVariable String id) {
        commonUser.setId(Integer.valueOf(id));
        Optional<UserModel> updatedUser = userService.updateUser(ConvertUserModel.convertToUser(commonUser));

        if (updatedUser.isPresent()){
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(ConvertUserModel.convertToCommonUser(updatedUser.get()));
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(null);
        }

    }

    @RequestMapping(method=RequestMethod.DELETE,value="/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable String id) {
        return ResponseEntity
                .status(userService.deleteUser(id))
                .contentType(MediaType.APPLICATION_JSON)
                .body(null);
    }

    @RequestMapping(method=RequestMethod.GET,value="/getByLoginAndPwd/{login}/{pwd}")
    public ResponseEntity<Object> getUserByLoginAndPwd(@PathVariable String login, @PathVariable String pwd) {
        Optional<UserModel> optionalUser;
        optionalUser = userService.getUserByLoginAndPwd(login,pwd);
        if ( optionalUser.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(ConvertUserModel.convertToCommonUser(optionalUser.get()));
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(null);
    }

    @RequestMapping(method=RequestMethod.GET,value="/getByLogin/{login}")
    public ResponseEntity<Object> getUserByLogin(@PathVariable String login) {
        Optional<UserModel> optionalUser;
        optionalUser = userService.getUserByLogin(login);
        if ( optionalUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(ConvertUserModel.convertToCommonUser(optionalUser.get()));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(null);
    }

    /*@RequestMapping(method=RequestMethod.GET,value="/auth/create")
    private String connect(@RequestParam("login") String login, @RequestParam("pwd") String pwd) {
        return userService.connectUser(login,pwd);
    }*/

    @RequestMapping(method = RequestMethod.POST, value = "/users")
    private Iterable<UserModel> getUsersFromId(@RequestBody String[] listIdUsers) {
        logger.debug("List ID Users reçus :" + listIdUsers.toString());
        return userService.getUsersByIdEvent(Arrays.asList(listIdUsers));
    }

}
