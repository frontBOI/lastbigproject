package com.bg.user.service;

import com.bg.user.config.RestTemplateClient;
import com.bg.user.model.UserModel;
import com.bg.user.repository.UserRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private static final Logger logger=Logger.getLogger(UserServiceTest.class);

    @Mock
    private RestTemplateClient restTemplate;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    @Spy
    private UserService serviceMock;
    private UserModel user;
    private final List<UserModel> users=new ArrayList<>();

    private AutoCloseable closeable;

    @Before
    public void init(){
        closeable=MockitoAnnotations.openMocks(this);
        System.out.println("Initialisation...");
        String jsonUser1="{\n"+
        "\"login\":\"momo2\",\n"+
        "\"pwd\":\"momo2\",\n"+
        "\"pseudonym\":\"momo2\",\n"+
        "\"lastName\":\"momoq2\",\n"+
        "\"firstName\":\"momoq2\",\n"+
        "\"email\":\"momo@gmail.com\",\n"+
        "\"address\":\"chezlacremière\",\n"+
        "\"age\":23,\n"+
        "\"tags\":[\"sport\",\"musique\"]\n"+
        "}";
        ObjectMapper mapper=new ObjectMapper();
        //UserModel user1=null;
        try{
            user=mapper.readValue(jsonUser1,UserModel.class);
        }catch(JsonProcessingException e){
            e.printStackTrace();
        }
        //users.add(user1);
    }

    @Test
    public void addUserWhenUserExistTest()  {
        //test1:If User Already Exist => Send Error message and return null
        logger.info("user = " + user.getLogin());
        Mockito.when(userRepository.findByLogin(user.getLogin())).thenReturn(Optional.ofNullable(user));
        Optional<UserModel> testUser = serviceMock.addUser(user);
        Assert.assertEquals("Test Add User Already Exist", null, testUser);
    }

    @Test
    public void addUserThrowExceptionTest() {
        //test2:If User Not Exist But Saving Fail Return null
        Mockito.when(userRepository.findByLogin(user.getLogin())).thenReturn(Optional.ofNullable(null));
        Mockito.doThrow(new RuntimeException("ExceptionFake")).when(userRepository).save(user);
        Optional<UserModel> testUser2 = serviceMock.addUser(user);
        Assert.assertEquals("Test Add User Save Fail", null, testUser2);
    }

    @Test
    public void addUserTest() {
        //test3:If User exist and saving sucess in repo (with id)
        Mockito.when(userRepository.findByLogin(user.getLogin())).thenReturn(Optional.ofNullable(null),Optional.of(user));
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Optional<UserModel> testUser3 = serviceMock.addUser(user);
        Assert.assertEquals("Test Add User Save Success",user,testUser3.get());
    }

    @Test
    public void updateUser(){
        //test1:ifsavefail,returnnull
        Mockito.doReturn(null).when(serviceMock).userRepository.save(user);
        Optional<UserModel>testUser=serviceMock.updateUser(user);
        Assert.assertEquals("testupdateusersavefail",null,testUser);

        //test2:ifsavesuccessbutfindByLoginfail,returnnull
        Mockito.doReturn(null).when(serviceMock).userRepository.findByLogin(user.getLogin());
        Mockito.doReturn(user).when(serviceMock).userRepository.save(user);
        Optional<UserModel>testUser2=serviceMock.addUser(user);
        Assert.assertEquals("testupdateusersavesuccessbutfindByLoginfail",null,testUser2);

        //test3:ifsavesuccessandfindByLoginreturnuser,returnuser
        Mockito.doReturn(user).when(serviceMock).userRepository.findByLogin(user.getLogin());
        Mockito.doReturn(user).when(serviceMock).userRepository.save(user);
        Optional<UserModel>testUser3=serviceMock.addUser(user);
        Assert.assertEquals("testaddusersavesuccessandfindByLoginsuccess",user,testUser3);
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}