package com.bg.usersuggestion.service;

import com.bg.usersuggestion.config.RestTemplateClient;
import com.bg.usersuggestion.model.EventModel;
import com.bg.usersuggestion.model.UserModel;
import com.bg.usersuggestion.model.UserSuggestionModel;
import com.bg.usersuggestion.repository.UserSuggestionRepository;
import lombok.NonNull;
import net.minidev.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserSuggestionService {

    private final UserSuggestionRepository userRepository;
    private final String baseUrlMSUser ="http://user-service:8080/";
    private final String baseUrlMSEvent = "http://event-service:8080/";
    private final String baseUrlMSUserEvent = "http://user-event-service:8080/";
    private final String baseUrlMSPrediction = "http://user-prediction-service:5000/";

    RestTemplateClient restTemplate;

    private static final Logger logger = Logger.getLogger(UserSuggestionService.class);
    public UserSuggestionService(UserSuggestionRepository userRepository, RestTemplateClient restTemplate)
    {
        this.userRepository = userRepository;
        this.restTemplate = restTemplate;
    }



    public boolean addUserSuggestionOnDb(Integer idUser, List<EventModel> trueEvents) {

        if(trueEvents.size() == 0) {
            logger.warn("Pas de sauvegarde dans la Database car aucun évenements trouvés...");
            HandleErrorRequest.setOtherError("Aucun evenement trouvé dans le MS Event parmis les événements proposées");
            return false;
        }
        List<Integer> eventsIdList = trueEvents.stream()
                .filter(Objects::nonNull)
                .map(EventModel::getId)
                .collect(Collectors.toList());
        logger.debug("Liste d'EventId :" + eventsIdList.toString());
        logger.info("Sauvegarde de la liste d'ID dans la Database... ");
        UserSuggestionModel userSuggestion = new UserSuggestionModel(idUser, eventsIdList);
        try {
            userRepository.save(userSuggestion);
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return true;
    }

    public ResponseEntity<Object> getSuggestEventsByUserId(Integer id) {
        logger.debug("Get Suggest Events By User Id");
        UserModel user = isUserExist(id);
        if (user == null)  return sendBadRequest();
        logger.info("Récupération des events de l'utilisateur " + id + "...");
        List<EventModel> events = getEventsFromUserId(id);
        List<String> suggestMeetupIds = sendEventToMSPrediction(events, user.getTags());

        if (suggestMeetupIds == null) return sendBadRequest();
        List<EventModel> trueEvents = convertMeetupIdToEvent(suggestMeetupIds);
        if (trueEvents == null) return sendBadRequest();
        if(addUserSuggestionOnDb(user.getId(), trueEvents)) {
            return ResponseEntity.ok().body(trueEvents);
        }
        else {
            return sendBadRequest();
        }

    }

    @NonNull
    private ResponseEntity<Object> sendBadRequest() {
        return ResponseEntity.badRequest()
                .body("{\"message\" : \"" + HandleErrorRequest.getError() + "\" }");
    }


    //region Delete On Database
    private void deleteUser(Integer id) {
        List<UserSuggestionModel> users = userRepository.findByUserId(id);
        if(users.size()==0) return;
        logger.info("Suppression de " + users.size() + " event dans la BDD...");
        try {
            userRepository.deleteAll(users);
        }
        catch(Exception e)
        {
            logger.error(e.toString());
        }
    }

    //TODO : Quand User-Event n'est plus interessé/participer le supprimer de la database MS-Suggest
    private void deleteEvent(Integer id) {
        List<UserSuggestionModel> events = userRepository.findByEventId(id);
        if(events.size()==0) return;
        logger.info("Suppression de " + events.size() + " event dans la BDD...");
        try {
            userRepository.deleteAll(events);
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }
    //endregion

    //region on MS Predict
    public List<String> sendEventToMSPrediction(List<EventModel> events, ArrayList<String> tags) {
        JSONObject json = new JSONObject();
        if(tags.size() != 0) {
            logger.debug("Tags de L'user :" + tags.toString());
            json.put("listTags",tags);
        }
        List<String> eventsMeetups = new ArrayList<>();
        if(events!=null && events.size()>0)
        {
            logger.debug("Taille de events :" + events.size());
             eventsMeetups = events.stream()
                    .map(EventModel::getMeetupId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            logger.debug("Taille de events meetups :" + eventsMeetups.size());

        }
        json.put("listEvtMeetUpID",eventsMeetups);
        logger.debug("Json a envoyer :" + json.toString());
        try {
            ResponseEntity<String[]> response = restTemplate.postForEntity(baseUrlMSPrediction + "predict",json, String[].class);
            if(response.getBody() != null) {
                if (response.getStatusCode() == HttpStatus.OK && response.getBody().length > 0) { //Handle empty data
                    //Cast to List<Integer>
                    logger.info("Reponse recu du MS Predict : " + Arrays.toString(response.getBody()));
                    Iterator<String> results = Arrays.stream(response.getBody()).iterator();
                    List<String> list = new ArrayList<String>();
                    results.forEachRemaining(list::add);
                    logger.info("Total des meetupId reçu : " + list.size());
                    return list;

                } else {
                    logger.info("Aucune réponse reçu du MS Predict... (code Erreur : " + response.getStatusCode() + ")");
                }
            }
            HandleErrorRequest.noEventsOnMS("predict");
            logger.warn(HandleErrorRequest.getError());
            return null;
        }
        catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("predict",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("predict");
            logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
            return null;
        }
    }
    //endregion

    //region on MS User
    public UserModel isUserExist(Integer id) {
        try {
            UserModel user = restTemplate.getForObject(baseUrlMSUser + "/" + id,UserModel.class);
            if(user==null)
            {
                HandleErrorRequest.noUserOnMS(id.toString());
                logger.warn(HandleErrorRequest.getError());
                deleteUser(id);
                return null;
            }
            else
            {
                logger.debug("L'utilisateur [" + id + "] a été trouvé (login = " + user.getPseudonym() + ")");
                return user;
            }
        }
        catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("user",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("user");
            logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
            return null;
        }


    }
    //endregion

    //region on MS User-Event
    public List<EventModel> getEventsFromUserId(Integer id) {
        try {
            List<EventModel> listEvent;
            EventModel[] events = restTemplate.getForObject(baseUrlMSUserEvent + "events/" + id,EventModel[].class);
            if (events != null) {
                listEvent = Arrays.asList(events);
                logger.info("Le MS User-Event a renvoyé [" + listEvent.size() + "] elements pour l'utilisateur [" + id + "] !");
                return listEvent;
            }
                return null;
        } catch (Exception e) {
            HandleErrorRequest.setMSError("user-event");
            logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
            return null;
        }

    }

    public List<EventModel> convertMeetupIdToEvent(List<String> ids) {

        //Check on MS Event event for each ids
        List<EventModel> listEvent;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<List<String>> entity = new HttpEntity<>(ids, headers);
            EventModel[] events = restTemplate.postForObject(baseUrlMSEvent + "meetup_events", entity, EventModel[].class);
            if (events != null) {
                listEvent = Arrays.asList(events);
                logger.info("Le MS Event a renvoyé [" + listEvent.size() + "] elements parmis les [" + ids.size() + "] envoyés");
                return listEvent;
            }
            HandleErrorRequest.setOtherError("Les IDs proposées par le MS Predict ne concorde pas avec les ID du MS Event");
            logger.warn(HandleErrorRequest.getError());
            return null;
        }   catch (HttpStatusCodeException e) {
            //Cover 4XX and 5XX HttpStatus Error
            HandleErrorRequest.setHttpError("event",e.getStatusCode().toString());
            logger.error(HandleErrorRequest.getError() + "\n" + e.toString());
            return null;
        }
        catch (ResourceAccessException e) {
            HandleErrorRequest.setMSError("event");
            logger.error(HandleErrorRequest.getError() +  "\n" + e.toString());
            return null;
        }

        //TODO : Delete on database each event does not exist anymore
    }
    //endregion
}
