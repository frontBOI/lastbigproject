package com.bg.usersuggestion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UserSuggestionModel implements Serializable {

    @Id
    private Integer userId;
    @ElementCollection
    private List<Integer> eventId;




    public void setUserId(Integer userId) { this.userId = userId; }
    public Integer getUserId() { return userId; }


    public UserSuggestionModel() {

    }

    public UserSuggestionModel(Integer userId, List<Integer> eventId) {
        this.eventId = eventId;
        this.userId = userId;
    }

    public List<Integer> getEventId() {
        return eventId;
    }

    public void setEventId(ArrayList<Integer> eventId) {
        this.eventId = eventId;
    }
}
