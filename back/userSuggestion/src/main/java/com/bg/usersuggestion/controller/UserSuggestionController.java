package com.bg.usersuggestion.controller;



import com.bg.usersuggestion.model.EventModel;
import com.bg.usersuggestion.service.UserSuggestionService;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Suggestion", description = "API de Suggestion")
public class UserSuggestionController {

    private final UserSuggestionService userService;
    private static final Logger logger = Logger.getLogger(UserSuggestionController.class);


    public UserSuggestionController(UserSuggestionService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Simple Hello World", description = "Permet de savoir si le serveur est lancé", tags = { "Hello World" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404", description = "Erreur (Communication)") })
    @RequestMapping("/")
    public HttpStatus isAlive()
    {
        logger.info("User Suggestion service is Alive! Version 20-01-17h56");
        return HttpStatus.OK;
    }
    @Operation(summary = "Trouver les EventModel suggérés", description = "Retourne une list<EventModel>", tags = { "GetSuggestEvent" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = EventModel.class))),
            @ApiResponse(responseCode = "400", description = "Erreur (Communication avec le MS Event ou MS Prediction ou liste null") })
    @RequestMapping(value = "/getSuggestEvent/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getSuggestEvent(
            @Parameter(description="ID De l'Utilisateur, ne peut pas être vide", required=true)
            @PathVariable Integer id) {
        logger.debug("Get Suggest Event");
        return userService.getSuggestEventsByUserId(id);

    }

 /*
    @RequestMapping("/updateEventFromUser/{id}")
    private ResponseEntity<Object> updateEventFromUser(@PathVariable Integer id, @RequestBody List<Integer> events) {
        UserModel user = userService.isUserExist(id);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        logger.info(" Number event found for user :" + events.size());
        List<Integer> suggestEvents = userService.updateSuggestEvents(user, events);
        userService.addUserSuggestionOnDb(id, suggestEvents);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(null);
    }

  */
}
