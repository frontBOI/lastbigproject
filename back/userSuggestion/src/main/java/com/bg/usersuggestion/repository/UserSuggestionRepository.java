package com.bg.usersuggestion.repository;

import com.bg.usersuggestion.model.UserSuggestionModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserSuggestionRepository extends CrudRepository<UserSuggestionModel, Integer> {
    List<UserSuggestionModel> findByUserId(Integer userId);
    List<UserSuggestionModel> findByEventId(Integer eventId);


}
