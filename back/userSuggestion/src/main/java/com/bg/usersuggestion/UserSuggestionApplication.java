package com.bg.usersuggestion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class UserSuggestionApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserSuggestionApplication.class, args);
    }

}
