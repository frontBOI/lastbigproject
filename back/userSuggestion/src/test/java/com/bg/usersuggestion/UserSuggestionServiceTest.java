package com.bg.usersuggestion;

import com.bg.usersuggestion.config.RestTemplateClient;
import com.bg.usersuggestion.model.EventModel;
import com.bg.usersuggestion.model.UserModel;
import com.bg.usersuggestion.repository.UserSuggestionRepository;
import com.bg.usersuggestion.service.UserSuggestionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.*;


@RunWith(MockitoJUnitRunner.class)
public class UserSuggestionServiceTest {

    private static final Logger logger = Logger.getLogger(UserSuggestionServiceTest.class);
    private final String baseUrlMSUser ="http://user-service:8080/";
    private final String baseUrlMSEvent = "http://event-service:8080/";
    private final String baseUrlMSUserEvent = "http://user-event-service:8080/";
    private final String baseUrlMSPrediction = "http://user-prediction-service:5000/";



    @Mock
    private RestTemplateClient restTemplate;
    @Mock private UserSuggestionRepository userSuggestionRepository;

    @InjectMocks
    @Spy
    private UserSuggestionService serviceMock;
    private UserModel user;
    private final List<EventModel> events = new ArrayList<>();

    private AutoCloseable closeable;


    @Before
    public void init()  {
        closeable = MockitoAnnotations.openMocks(this);
        System.out.println("Initialisation...");
        String jsonUser1="{\n"+
                "\"login\":\"momo2\",\n"+
                "\"pwd\":\"momo2\",\n"+
                "\"pseudonym\":\"momo2\",\n"+
                "\"lastName\":\"momoq2\",\n"+
                "\"firstName\":\"momoq2\",\n"+
                "\"email\":\"momo@gmail.com\",\n"+
                "\"address\":\"chezlacremière\",\n"+
                "\"age\":23,\n"+
                "\"tags\":[\"sport\",\"musique\"]\n"+
                "}";
        String jsonEvent1 = "    {\n" +
                "        \"id\": 142,\n" +
                "        \"meetupId\": \"275562802\",\n" +
                "        \"name\": \"Faciliter le travail à distance avec Windows Virtual Desktop\",\n" +
                "        \"duration\": 3600000,\n" +
                "        \"localDate\": \"2021-01-27T00:00:00.000+00:00\",\n" +
                "        \"localTime\": \"12:30:00\",\n" +
                "        \"utcOffset\": 3600000,\n" +
                "        \"venueName\": \"Online event\",\n" +
                "        \"venueLat\": null,\n" +
                "        \"venueLon\": null,\n" +
                "        \"venueAddressString\": null,\n" +
                "        \"organisateurMeetupId\": 32322977,\n" +
                "        \"organisateurLocalId\": null,\n" +
                "        \"eventLink\": \"https://www.meetup.com/Azure-Group-Lyon/events/275562802/\",\n" +
                "        \"description\": \"<p>### MEETUP AZURE NANTES EN VISIO ###</p>\",\n" +
                "        \"visibility\": false,\n" +
                "        \"ticketing\": null,\n" +
                "        \"categorie\": []\n" +
                "    },";

        String jsonEvent2 = "    {\n" +
                "        \"id\": 143,\n" +
                "        \"name\": \"Le dispositif Pinel 2019 en détail\",\n" +
                "        \"duration\": 7200000,\n" +
                "        \"localDate\": \"2021-01-20T00:00:00.000+00:00\",\n" +
                "        \"localTime\": \"19:00:00\",\n" +
                "        \"utcOffset\": 3600000,\n" +
                "        \"venueName\": null,\n" +
                "        \"venueLat\": null,\n" +
                "        \"venueLon\": null,\n" +
                "        \"venueAddressString\": null,\n" +
                "        \"organisateurMeetupId\": 22201510,\n" +
                "        \"organisateurLocalId\": null,\n" +
                "        \"eventLink\": \"https://www.meetup.com/Meetup-independance-financiere/events/lqjgrqycccbbc/\",\n" +
                "        \"description\": \"<p>Présentation du dispositif Pinel pour acquérir un logment en réduisant ses impôts</p> \",\n" +
                "        \"visibility\": false,\n" +
                "        \"ticketing\": null,\n" +
                "        \"categorie\": []\n" +
                "    }";
        //endregion
        ObjectMapper mapper = new ObjectMapper();
        EventModel event1 = null;
        try {
            event1 = mapper.readValue(jsonEvent1, EventModel.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        EventModel event2 = null;
        try {
            event2 = mapper.readValue(jsonEvent2, EventModel.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try{
            user = mapper.readValue(jsonUser1, UserModel.class);
        }catch(JsonProcessingException e){
            e.printStackTrace();
        }
        events.add(event1);
        events.add(event2);

    }

    @After public void releaseMocks() throws Exception {
        closeable.close();
    }

    /*    Test lorsque l'user défini Existe
Supposition :
 - Le MS Predict renvoie 2 ID Events
 - Le MS Event connait ces 2 ID Events et renvoie 2 EventModel
*/
    @Test
    public void getSuggestEventTestWithGoodIdWithEvent() {
        Integer id = 3;
        List<String> meetupsIdFound = new ArrayList<>();
        meetupsIdFound.add("1");
        meetupsIdFound.add("2");

        //when
        Mockito.doReturn(user).when(serviceMock).isUserExist(id);
        Mockito.doReturn(events).when(serviceMock).getEventsFromUserId(id);
        Mockito.doReturn(meetupsIdFound).when(serviceMock).sendEventToMSPrediction(events, user.getTags());
        Mockito.doReturn(events).when(serviceMock).convertMeetupIdToEvent(meetupsIdFound);


        ResponseEntity<Object> test = serviceMock.getSuggestEventsByUserId(id);
        Assert.assertEquals("GET SUGGEST EVENT WITH GOOD ID WITH EVENT", test.getStatusCode(), HttpStatus.OK);
        String strBody = Objects.requireNonNull(test.getBody()).toString();
        logger.debug("Body = : \n" + strBody);
        Assert.assertEquals(events, test.getBody());


    }

    /*    Test lorsque l'user défini Existe
    Supposition :
     - Le MS Predict renvoie 2 ID Events
     - Le MS Event ne connait pas 2 ID Events et renvoie null
    */
    @Test
    public void getSuggestEventTestWithGoodIdWithoutEvent() {
        Integer id = 3;
        List<String> meetupsIdFound = new ArrayList<>();
        meetupsIdFound.add("1");
        meetupsIdFound.add("2");
        logger.info(user.getAge());

        //when
        Mockito.doReturn(user).when(serviceMock).isUserExist(id);
        Mockito.doReturn(events).when(serviceMock).getEventsFromUserId(id);
        Mockito.doReturn(meetupsIdFound).when(serviceMock).sendEventToMSPrediction(events, user.getTags());
        Mockito.lenient().when(restTemplate.postForObject(baseUrlMSPrediction + "predict","json",List.class))
                .thenReturn(null);


        ResponseEntity<Object> test = serviceMock.getSuggestEventsByUserId(id);
        String strBody = Objects.requireNonNull(test.getBody()).toString();
        logger.debug("Body = : \n" + strBody);
        Assert.assertEquals("GET SUGGEST EVENT WITH GOOD ID WITHOUT EVENT", test.getStatusCode(), HttpStatus.BAD_REQUEST);
        Assert.assertTrue(strBody.contains("Les IDs proposées par le MS Predict ne concorde pas avec les ID du MS Event"));
    }

    /*    Test lorsque l'user défini Existe
    Supposition :
    - Le MS Predict renvoie null
    */
    @Test
    public void getSuggestEventTestWithGoodIdWithoutPrediction() {
        //init
        Integer id = 3;
        List<Integer> eventsIdFound = new ArrayList<>();
        eventsIdFound.add(1);
        eventsIdFound.add(2);

        //when
        Mockito.doReturn(user).when(serviceMock).isUserExist(id);
        Mockito.doReturn(events).when(serviceMock).getEventsFromUserId(id);
        Mockito.doReturn(null).when(serviceMock).sendEventToMSPrediction(events, user.getTags());

        //do
        ResponseEntity<Object> test = serviceMock.getSuggestEventsByUserId(id);
        String strBody = Objects.requireNonNull(test.getBody()).toString();
        logger.debug("Body = : \n" + strBody);
        Assert.assertEquals("GET SUGGEST EVENT WITH GOOD ID WITHOUT PREDICTION ", test.getStatusCode(), HttpStatus.BAD_REQUEST);
        Assert.assertTrue(strBody.contains("message"));

    }

    /*    Test lorsque l'user défini n'existe pas
Supposition :
- Le MS User ne trouve pas l'user (renvoie null)
*/
    @Test
    public void getSuggestEventTestWithBadId() {
        Integer id = 48080;
        List<Integer> eventsIdFound = new ArrayList<>();
        eventsIdFound.add(1);
        eventsIdFound.add(2);
        logger.info(user.getAge());

        //when
        Mockito.doReturn(null).when(serviceMock).isUserExist(id);
        //Mockito.doReturn(events).when(serviceMock).getEventsFromUserId(id);
        //Mockito.doReturn(null).when(serviceMock).sendEventToMSPrediction(events, user.getTags().toString());
        //Mockito.doReturn(null).when(serviceMock).convertEventsIdToEvent(eventsIdFound);


        ResponseEntity<Object> test = serviceMock.getSuggestEventsByUserId(id);
        String strBody = Objects.requireNonNull(test.getBody()).toString();
        logger.debug("Body = : \n" + strBody);
        Assert.assertEquals("GET SUGGEST EVENT WITH BAD ID", test.getStatusCode(), HttpStatus.BAD_REQUEST);
        logger.info("test");
        Assert.assertTrue(strBody.contains("message"));
    }

    /*    Test IsUserExist lorsque l'User n'existe pas (renvoie null)
Supposition :
- Le MS User ne trouve pas l'user
*/
    @Test
    public void UserDoesntExistTest() {
        Integer id = 48080;
        Mockito.when(restTemplate.getForObject(baseUrlMSUser + "/" + id, UserModel.class))
        .thenReturn(null);
        UserModel test = serviceMock.isUserExist(id);
        Assert.assertNull("USER DOESNT EXIST", test);
    }


    @Test
    public void UserExistTest() {
        Integer id = 3;
        Mockito.when(restTemplate.getForObject(baseUrlMSUser + "/" + id, UserModel.class))
                .thenReturn(user);
        UserModel testUser = serviceMock.isUserExist(id);
        Assert.assertEquals("USER EXIST", testUser, user);

    }
}
